#include "header.h"

void createList(list *L) {
  (*L).first = NULL;
  (*L).tail = NULL;
}

int countElement(list L) {
  int hasil = 0;

  if (L.first != NULL) {
    /*list tidak kosong*/
    elemen *elmt;

    /*inisialisasi*/
    elmt = L.first;

    while (elmt != NULL) {
      /*proses*/
      hasil = hasil + 1;

      /*iterasi*/
      elmt = elmt->next;
    }
  }

  return hasil;
}

void addFirst(char nim[], char nama[], char nilai[], list *L) {
  elemen *baru;

  baru = (elemen *)malloc(sizeof(elemen));

  strcpy(baru->elmt.nim, nim);
  strcpy(baru->elmt.nama, nama);
  strcpy(baru->elmt.nilai, nilai);

  if ((*L).first == NULL) {
    baru->prev = NULL;
    baru->next = NULL;
    (*L).tail = baru;
  } else {
    baru->next = (*L).first;
    baru->prev = NULL;
    (*L).first->prev = baru;
  }

  (*L).first = baru;
  baru = NULL;
}

void addAfter(elemen *prev, char nim[], char nama[], char nilai[], list *L) {
  elemen *baru;
  baru = (elemen *)malloc(sizeof(elemen));

  strcpy(baru->elmt.nim, nim);
  strcpy(baru->elmt.nama, nama);
  strcpy(baru->elmt.nilai, nilai);

  if (prev->next == NULL) {
    baru->next = NULL;
    (*L).tail = baru;
  } else {
    baru->next = prev->next;
    baru->next->prev = baru;
  }

  baru->prev= prev;
  prev->next = baru;
  baru = NULL;
}

void addLast(char nim[], char nama[], char nilai[], list *L) {
  if ((*L).first == NULL) {
    /*jika list adalah list kosong */
    addFirst(nim, nama, nilai, L);
  } else {
    /*jika list tidak kosong*/
    elemen *baru;
    baru = (elemen *)malloc(sizeof(elemen));

    strcpy(baru->elmt.nim, nim);
    strcpy(baru->elmt.nama, nama);
    strcpy(baru->elmt.nilai, nilai);
    baru->next = NULL;
    (*L).tail->next = baru;
    baru->prev = (*L).tail;
    (*L).tail = baru;
    baru = NULL;
  }
}

void delFirst(list *L) {
  if ((*L).first != NULL) {
    /*jika list bukan list kosong*/
    elemen *hapus = (*L).first;
    if (countElement(*L) == 1) {
      (*L).first = NULL;
      (*L).tail = NULL;
    } else {
      (*L).first = (*L).first->next;
      (*L).first->prev = NULL;
      hapus->next = NULL;
    }
    free(hapus);
  }
}

void delAfter(elemen *prev, list *L) {
  elemen *hapus = prev->next;

  if (hapus != NULL) {
    if (hapus->next == NULL) {
      prev->next = NULL;
    } else {
      prev->next = hapus->next;
      hapus->next->prev = prev;
      hapus->next = NULL;
    }
    hapus->prev = NULL;
    free(hapus);
  }
}

void delLast(list *L) {
  if ((*L).first != NULL) {
    /*jika list tidak kosong*/
    if (countElement(*L) == 1) {
      /*list terdiri dari satu elemen*/
      delFirst(L);
    } else {
      /*mencari elemen terakhir list*/
      elemen *hapus = (*L).tail;
      (*L).tail = hapus->prev;
      (*L).tail->next = NULL;
      hapus->prev = NULL;
      free(hapus);
    }
  }
}

void printElement(list L) {
  if (L.first != NULL) {
    /*jika list tidak kosong*/
    /*inisialisasi*/
    elemen *elmt = L.first;
    int i = 1;
    while (elmt != NULL) {
      /*prosses*/
      printf("elemen ke : %d\n", i);

      printf("nim : %s\n", elmt->elmt.nim);
      printf("nama : %s\n", elmt->elmt.nama);
      printf("nilai : %s\n", elmt->elmt.nilai);

      printf("-------------\n");

      /*iterasi*/
      elmt = elmt->next;
      i = i + 1;
    }
  } else {
    /*proses jika list kosong*/
    printf("liat kosong\n");
  }
}

void delAll(list *L) {
  if (countElement(*L) != 0) {
    int i;

    for (i = countElement(*L); i >= 1; i--) {
      /*proses menghapus elemen list*/
      delLast(L);
    }
  }
}