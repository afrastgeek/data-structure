# struktur data

## pointer
pointer = penunjuk = pengait

## kontainer elemen

elemen = kontainer + isi

## list

kepala -> elements -> null

ketika kepala melepaskan elemennya secara sepihak, elemen akan tetap mengisi memori dengan tanpa pengarah.

penempatan elemen di memori bisa jadi tidak berurut. namun pointernya tetap saling menghubungkan sehingga nampak terurut.

### list kosong

kepala -> null

hanya terdiri dari satu pointer.
tidak punya elemen.
hanya sebuah pointer first yang mengacu ke `NULL`
.hey
## Add First
### Penambahan elemen di awal list (addfirst) list kosong

add ada 3. del ada 3.
first, after, last.

pointer first ngarah ke null
pointer baru ngarah ke element
baru ngasih ke first
first terima
baru lepas
baru jadi ngarah ke null
first ngarah ke element.

### penambahan elemen di awal list (addfirst) banyak elemen

first ngarah ke A (tidak kosong).
baru ngarah ke B.
pointer next di B ngarah ke A.
pointer first ngarah ke B.
baru lepas.
first get.
baru jadi null.
first ngarah ke B ngarah ke A ngarah ke NULL.

MEMASTIKAN DATA YANG LAMA TIDAK HILANG.
all two condition on add first.

### penambahan elemen di awal list (addfirst) banyak elemen jika salah

elemen eksisting ditelantarkan.

## AddAfter
### Penambahan elemen di tengah addafter jika elemen sebelum adalah elemen terakhir

(ADD LAST)

ada pointer yang ngarah ke elemen baru.
last pointer ngarah ke NULL.
last pointer ngarah ke elemen baru. done.
pointer yang awalnya ngarah ke elemen baru jadi ngarah ke null.

### Penambahan elemen di tengah (addafter jika elemen sebelum di tengah

ada baru dan elemen baru.
last dari elemen baru ngarah ke target elemen.
pointer elemen di target elemen ngarah ke elemen baru.
baru ke null.

### Penambahan elemen di tengah (addafter) jika elemen sebelum di tengah dan ada kesalahan.

target elemen lepas karena pointer langsung nunjuk ke baru sebelum last baru nunjuk ke target elemen.

## Add Last
### Penambahan elemen di akhir (add last) jika list kosong maka addfirst untuk list kosong.

jika kosong lakukan add first kosong

### Penambahan elemen di akhir (addlast) jika elemen banyak elemen maka addafter elemen belakang.

addafter yang kosong.

# DELETE
## Delfirst
### Hapus Elemen awal (Delfirst) jika satu elemen.

delete ke target elemen.
firt to null
hapus mengarah ke target elemen.

### Hapus Elemen awal (delfirst) jika banyak elemen elemen

Del ngarah ke target elemen.
first ngarah ke next element.
pointer next element ke null.
delete target element.

### hapus elemen tengah (delafter) yang dihapus paling belakang

before last element akan menjadi last element, arahkan pointer ke last element menjadi null
pointer hapus ke last element.
hapus.

### Hapus elemen tengah (delafter) jika yang dihapus di tengah

pointer hapus ke target element.
pointer dari before target element ke after target element.
pointer target element ke null.
delete target.

### Hapus elemen akhir (dellast) jika hanya satu elemen maka delfirst satu elemen

delfirst kosong.

### Hapus elemen akhir (dellast) jika banyak elemen maka delafter elemen terakhir

delafter kosong.
