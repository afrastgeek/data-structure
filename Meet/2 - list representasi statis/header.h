// deklarasi elemen
#include <stdio.h>
#include <string.h>

typedef struct {
  char nim[10];
  char nama[50];
  char nilai[2];
} nilaiMatKul;

typedef struct {
  nilaiMatKul elmt;
  int next;
} elemen;

/* Another way to create "elemen" type */
// typedef struct {
//   struct elmt {
//     char nim[10];
//     char nama[50];
//     char nilai[2];
//   };
//   int next;
// } elemen;

typedef struct {
  int first;
  elemen
      data[10]; /* we could use global variable or pass the length of this */
} list;

/* next slide */
void createList(list *L);
int countElement(list L);
int emptyElement(list L);
void addFirst(char nim[], char nama[], char nilai[], list *L);
void addAfter(int prev, char nim[], char nama[], char nilai[], list *L);
void addLast(char nim[], char nama[], char nilai[], list *L);
void delFirst(list *L);
void delAfter(int prev, list *L);
void delLast(list *L);
void printElement(list L);
void delAll(list *L);