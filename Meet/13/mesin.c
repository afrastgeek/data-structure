#include "header.h"

/*membuat pohon*/
void makeTree(char c, tree *T){
	//membuat simpul dengan alokasi di memory
	simpul *node;
	node = (simpul *) malloc (sizeof (simpul));
	
	//memasukan data kedalam pohon
	node->info = c;
	/*inisialisasi*/
	node->sibling = NULL; //sibling masih null, karena pohon baru dibuat
	node->child = NULL; //child masih null, karena pohon baru dibuat
	//root memegang data paling atas
	(*T).root = node;
}

void addChild(char c, simpul *root){
	if(root != NULL){
		/*jika root tidak kosong*/
		//membuat simpul dengan alokasi di memory
		simpul *node;
		node = (simpul *) malloc (sizeof (simpul));
		
		//memasukan data
		node->info = c;
		//karena baru dibuat maka child masih NULL
		node->child = NULL;
		/*jika ayah belum memiliki anak*/
		if(root->child == NULL){
			/*simpul baru menjadi anak pertama*/
			node->sibling = NULL;
			root->child = node;
		}
		else{
			/*jika ayah hanya memiliki satu anak*/
			if(root->child->sibling == NULL){
				/*simpul baru menjadi anak kedua*/
				node->sibling = root->child; //sibling anak pertama memegang anak kedua
				root->child->sibling = node; //sibling anak kedua memegang anak pertama
			}
			else{
				/*jika ayah memiliki banyak anak*/
				simpul *last = root->child;
				
				/*mencari simpul anak terakhir*/
				while(last->sibling != root->child){
					last = last->sibling;
				}
				/*simpul baru menjadi anak paling terakhir*/
				node->sibling = root->child; //sibling simpul baru memegang kakak pertama
				last->sibling = node; //sibling kakak kedua terakhir memegang simpul baru
			}
		}
	}
}

void delChild(char c, simpul *root){
	simpul *node =  root->child;
	
	if(node != NULL){
		/*harus memiliki anak karena menghapus anak*/
		if(node->sibling == NULL){
			/*jika hanya memiliki 1 anak*/
			if(root->child->info == c){
				if(node->child != NULL){
					/*jika anak memiliki anak maka tidak bisa dihapus*/
					printf("maaf anak anda memiliki anak\n");
				}
				else{
					/*jika anak tidak memiliki anak maka bisa dihapus*/
					root->child = NULL;
					free(node);
				}
			}
			else{
				/*jika tidak ada nama anak didalam karakter masukan*/
				printf("tidak ada simpul anak dengan info karakter masukan\n");
			}
		}
		else{
			/*jika memiliki banyak anak*/
			simpul *prec = NULL;
			
			/*mencari simpul yang akan dihapus*/
			int ketemu = 0;
			while((node->sibling != root->child) && (ketemu == 0)){
				//mencari simpul dengan mengecek semua simpul kecuali simpul terakhir
				if(node->info == c){
					ketemu = 1; //jika ketemu nilai ketemu menjadi 1
				}
				else{
					//jika masih belum ketemu pencarian berlanjut
					prec =  node; //prec menjadi node
					node = node->sibling; //node menjadi simpul selanjutnya
				}
			}
			
			/*memproses simpul anak terakhir karena belum terproses dalam pengulangan*/
			if((ketemu == 0) && (node->info == c)){
				ketemu = 1;
			}
			
			if(ketemu==1){
				simpul *last = root->child;
				
				/*mencari simpul anak terakhir*/
				while(last->sibling != root->child){
					last = last->sibling;
				}
				
				if(prec==NULL){
					/*jika simpul yang dihapus anak pertama*/
					if((node->sibling == last) && (last->sibling == root->child)){
						/*jika hanya ada 2 anak*/
						root->child = last;
						last->sibling = NULL;
					}
					else{
						/*jika lebih dari 2 anak*/
						root->child = node->sibling;
						last->sibling = root->child;
					}
				}
				else{
					/*jika simpul yang dihapus bukan anak pertama*/
					if((prec == root->child) && (node->sibling == root->child)){
						/*jika hanya ada 2 simpul anak, yang dihapus anak kedua*/
						root->child->sibling = NULL;
					}
					else{
						prec->sibling = node->sibling;
						node->sibling = NULL;
					}
				}
				
				free(node);
			}
			else{
				printf("tidak ada simpul anak dengan info karakter masukan\n");
			}
		}
	}
}

simpul *findSimpul(char c, simpul *root){
	simpul *hasil = NULL;
	if(root != NULL){
		if(root->info == c){
			hasil = root;
		}
		else{
			simpul *node = root->child;
			if(node != NULL){
				if(node->sibling == NULL){
					/*jika memiliki satu anak*/
					if(node->info == c){
						hasil = node;
					}
					else{
						hasil = findSimpul(c,node);
					}
				}
				else{
					/*jika memiliki banyak anak*/
					int ketemu = 0;
					while((node->sibling != root->child) &&(ketemu == 0)){
						if(node->info == c){
							hasil = node;
							ketemu = 1;
						}else{
							if(hasil==NULL){
								hasil = findSimpul(c,node);
								node = node->sibling;
							}
							else{
								ketemu = 1;
							}
						}
					}
				
				/*memproses simpul anak terakhir karena belum terproses dalam pengulangan*/
					if(ketemu == 0){
						if(node->info == c){
							hasil = node;
						}else{
							if(hasil==NULL){
								hasil = findSimpul(c,node);
								node = node->sibling;
							}else{
								ketemu = 1;
							}
						}
					}
				}
			}
		}
	}
	return hasil;
}

void printTreePreOrder(simpul *root){
	if(root != NULL){
		printf(" %c ",root->info);
		simpul *node = root->child;
		
		if(node != NULL){
			if(node->sibling == NULL){
				/*jika memiliki satu anak*/
				printTreePreOrder(node);
			}
			else{
				/*jika memiliki banyak anak*/
				
				/*mencetak simpul anak*/
				while(node->sibling != root->child){
					printTreePreOrder(node);
					node = node->sibling;
				}
				/*memproses simpul anak terakhir karena belum terproses dalam pengurangan*/
				printTreePreOrder(node);
			}
		}
	}
}

void printTreePostOrder(simpul *root){
	if(root != NULL){
		simpul *node = root->child;
		if(node != NULL){
			if(node->sibling == NULL){
				/*jika memiliki satu anak*/
				printTreePostOrder(node);
			}
			else{
				/*jika memiliki banyak anak*/
				
				/*mencetak simpul anak*/
				while(node->sibling != root->child){
					printTreePostOrder(node);
					node = node->sibling;
				}
				/*memproses simpul anak terakhir karena belum terproses dalam pengurangan*/
				printTreePostOrder(node);
			}
		}
		printf(" %c ",root->info);
	}
}

void copyTree(simpul *root1, simpul **root2){
	if(root1 != NULL){ //jika root1 bukan null
		*root2 = (simpul *) malloc (sizeof (simpul));
		
		(*root2)->info = root1->info;
		(*root2)->sibling = NULL;
		(*root2)->child = NULL;
		
		if(root1->child != NULL){
			if(root1->child->sibling == NULL){
				/*jika memiliki 1 anak*/
				copyTree(root1->child, &(*root2)->child);
			}
			else{
				/*jika memiliki banyak anak*/
				simpul *node1 = root1->child;
				simpul *node2 = (*root2)->child;
				
				while(node1->sibling != root1->child){
					copyTree(node1,&(node2));
					node1 = node1->sibling;
					node2 = node2->sibling;
				}
				/*memproses simpul anak terakhir karena belum terproses dalam pengulangan*/
				copyTree(node1,&(node2));
			}
		}
	}
}

int isEqual(simpul *root1, simpul *root2){
	int hasil = 1;
	if((root1 != NULL) && (root2 != NULL)){
		if(root1->info != root2->info){
			hasil = 0;
		}
		else{
			if((root1->child != NULL) && (root2->child != NULL)){
				if(root1->child->sibling == NULL){
					/*jika memiliki satu anak*/
					hasil = isEqual(root1->child,root2->child);
				}
				else{
					/*jika memiliki banyak anak*/
					simpul *node1 = root1->child;
					simpul *node2 = root2->child;
					
					while(node1->sibling != root1->child){
						if((node1 != NULL) && (node2 != NULL)){
							hasil = isEqual(node1,node2);
							node1 = node1->sibling;
							node2 = node2->sibling;
						}
						else{
							hasil = 0;
							break;
						}
					}
					/*memproses simpul anak terakhir karena belum terproses dalam pengulangan*/
					hasil = isEqual(node1, node2);
				}
			}
		}
	}
	else{
		if((root1 != NULL) || (root2 != NULL)){
			hasil = 0;
		}
	}
	return hasil;
}