#include "header.h"

void createEmpty(graph *G) { (*G).first = NULL; }

void addSimpul(char c, graph *G) {
  simpul *node;
  node = (simpul *)malloc(sizeof(simpul));
  node->info = c;
  node->next = NULL;
  node->arc = NULL;

  if ((*G).first == NULL) {
    /*jika graph kosong*/
    (*G).first = node;
  } else {
    /*menambahkan simpul baru pada akhir graph*/
    simpul *last = (*G).first;

    while (last->next != NULL) {
      last = last->next;
    }
    /*iterasi*/
    last->next = node;
  }
}

void addJalur(simpul *tujuan, int beban, simpul *awal) {
  jalur *arc;
  arc = (jalur *)malloc(sizeof(jalur));
  arc->info = beban;
  arc->next = NULL;
  arc->node = tujuan;

  if (awal->arc == NULL) {
    /*jika list jalur kosong*/
    awal->arc = arc;
  } else {
    /*menambahkan jalur baru pada akhir list jalur*/
    jalur *last = awal->arc;

    while (last->next != NULL) {
      last = last->next;
    }
    last->next = arc;
  }
}

void delJalur(char ctujuan, simpul *awal) {
  jalur *arc = awal->arc;
  if (arc != NULL) {
    jalur *prec = NULL;
    /*mencari jalur yang akan dihapus*/
    int ketemu = 0;
    while ((arc != NULL) && (ketemu == 0)) {
      if (arc->node->info == ctujuan) {
        ketemu = 1;
      } else {
        prec = arc;
        arc = arc->next;
      }
    }
    if (ketemu == 1) {
      if (prec == NULL) {
        /*hapus jalur pertama*/
        awal->arc = arc->next;
      } else {
        if (arc->next == NULL) {
          /*hapus jalur terakhir*/
          prec->next = NULL;
        } else {
          /*hapus jalur di tengah*/
          prec->next = arc->next;
          arc->next = NULL;
        }
      }
      free(arc);
    } else {
      printf("tidak ada jalur dengan simpul tujuan\n");
    }
  } else {
    printf("tidak ada jalur dengan simpul tujuan\n");
  }
}

void delSimpul(char c, graph *G) {
  simpul *elmt = (*G).first;
  if (elmt != NULL) {
    simpul *prec = NULL;
    /*mencari simpul yang akan dihapus*/
    int ketemu = 0;
    while ((elmt != NULL) && (ketemu == 0)) {
      if (elmt->info == c) {
        ketemu = 1;
      } else {
        prec = elmt;
        elmt = elmt->next;
      }
    }
    if (ketemu == 1) {
      // 1 semua jalur ke arah koya hapus harus dihapus
      // 2 semua jalur dari/milik kota hapus harus dihapus
      // 3 hapus kota yang ingin dihapus
      if (prec == NULL) {
        /*hapus simpul pertama*/
        (*G).first = elmt->next;
      } else {
        if (elmt->next == NULL) {
          /*hapus simpul terakhir*/
          prec->next = NULL;
        } else {
          /*hapus simpul di tengah*/
          prec->next = elmt->next;
          elmt->next = NULL;
        }
      }
      free(elmt);
    } else {
      printf("tidak ada simpul dengan info karakter masukan\n");
    }
  } else {
    printf("tidak ada simpul dengan dengan info karakter masukan\n");
  }
}

simpul *findSimpul(char c, graph G) {
  simpul *hasil = NULL;
  simpul *node = G.first;
  int ketemu = 0;

  while ((node != NULL) && (ketemu == 0)) {
    if (node->info == c) {
      hasil = node;
      ketemu = 1;
    } else {
      node = node->next;
    }
  }
  return hasil;
}

void printGraph(graph G) {
  simpul *node = G.first;
  if (node != NULL) {
    while (node != NULL) {
      printf("simpul : %c\n", node->info);
      jalur *arc = node->arc;
      while (arc != NULL) {
        printf(" - ada jalur ke simpul : %c dengan beban : %d\n",
               arc->node->info, arc->info);
        arc = arc->next;
      }
      node = node->next;
    }
  } else {
    printf("graph kosong\n");
  }
}
