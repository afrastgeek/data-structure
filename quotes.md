# Quotes
Quotes spoken by lecturer on this course.

---

> syarat ilmu adalah tawadhu (merendahkan diri pada ilmu, menghormati ilmu dan membutuhkan ilmu)
maka ilmu akan meresap di dalam qolbu

----

> kata maha disematkan dalam mahasiswa karena kemampuan untuk menjadi mandiri dan bertanggung jawab terhadap pilihan hidup yang ditempuh dan dipilih.

---

hakikatnya, tiap individu datang ke kampus untuk menuntut ilmu.
ilmu diberikan oleh tuhan melalui dosen.
dosen berusaha menyampaikan ilmu yang dimilikinya kepada setiap individu.
diibaratkan dengan gelas, individu datang dengan gelas kosong atau terisi.
namun, kembali lagi kepada individunya. mau menerima atau tidak.

---

```
tuhan
^ milik 
kemuliaan dan rejeki masa depan   <- nilai ->   tidak taat
```

---

**game of life.**: nabi, di ikan paus.

---

steven covey.

reaktif action.
7 habits of the highly effective people.

kalau kamu merasa semua masalah datang dari eksternal....
intinya problem itu dari dalam diri.

> yesterday i was clever, so i wanted to change the world.
 today i am wise, so i am changing myself.

---
