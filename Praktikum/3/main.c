#include "header.h"

int main(int argc, char const *argv[]) {
  int i;
  dataObat query[5];
  for (i = 0; i < 5; i += 1) {
    scanf(" %s %d %d", query[i].nama, &query[i].stok, &query[i].harga);
  }

  list L;
  createList(&L);

  addLast(query[0].nama, query[0].stok, query[0].harga, &L);
  addFirst(query[1].nama, query[1].stok, query[1].harga, &L);
  addAfter(L.data[L.first].next, query[2].nama, query[2].stok, query[2].harga, &L); 
  printElement(L);
  addFirst(query[3].nama, query[3].stok, query[3].harga, &L);
  delAfter(L.first, &L); 
  printElement(L);
  addLast(query[4].nama, query[4].stok, query[4].harga, &L);
  // printToHead(L);
  printElement(L);
  delFirst(&L);
  delLast(&L);
  printElement(L);

  return 0;
}

/*
Input:
Paracetamol 100 5000
Antihistamin 30 8000
Bioflavonoid 10 13000
Mefenamat 15 10000
Aspirin 40 9000
 */