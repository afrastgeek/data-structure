#include "header.h"

void createList(list *L) { (*L).first = NULL; }

int countElement(list L) {
  int hasil = 0;

  if (L.first != NULL) {
    /* list tidak kosong */

    elemen *elmt;

    /* inisialisasi */
    elmt = L.first;

    while (elmt != NULL) {
      /* proses */
      hasil = hasil + 1;

      /* iterasi */
      elmt = elmt->next;
    }
  }

  return hasil;
}

void addFirst(char nama[], int umur, char jenis_kelamin, list *L) {
  elemen *baru;
  baru = (elemen *)malloc(sizeof(elemen));
  strcpy(baru->elmt.nama, nama);
  baru->elmt.umur = umur;
  baru->elmt.jenis_kelamin = jenis_kelamin;
  if ((*L).first == NULL) {
    baru->next = NULL;
  } else {
    baru->next = (*L).first;
  }
  (*L).first = baru;
  baru = NULL;
}

void addAfter(elemen *prev, char nama[], int umur, char jenis_kelamin,
              list *L) {
  elemen *baru;
  baru = (elemen *)malloc(sizeof(elemen));
  strcpy(baru->elmt.nama, nama);
  baru->elmt.umur = umur;
  baru->elmt.jenis_kelamin = jenis_kelamin;
  if (prev->next == NULL) {
    baru->next = NULL;
  } else {
    baru->next = prev->next;
  }
  prev->next = baru;
  baru = NULL;
}

void addLast(char nama[], int umur, char jenis_kelamin, list *L) {
  if ((*L).first == NULL) {
    /* jika list adalah list kosong */
    addFirst(nama, umur, jenis_kelamin, L);
  } else {
    /* jika list tidak kosong */
    /* mencari elemen terakhir list */
    elemen *prev = (*L).first;
    while (prev->next != NULL) {
      /* iterasi */
      prev = prev->next;
    }
    addAfter(prev, nama, umur, jenis_kelamin, L);
  }
}

void delFirst(list *L) {
  if ((*L).first != NULL) {
    /* jika list bukan list kosong */
    elemen *hapus = (*L).first;
    if (countElement(*L) == 1) {
      (*L).first = NULL;
    } else {
      (*L).first = (*L).first->next;
      hapus->next = NULL;
    }
    free(hapus);
  }
}

void delAfter(elemen *prev, list *L) {
  elemen *hapus = prev->next;
  if (hapus != NULL) {
    if (hapus->next == NULL) {
      prev->next = NULL;
    } else {
      prev->next = hapus->next;
      hapus->next = NULL;
    }
    free(hapus);
  }
}

void delLast(list *L) {
  if ((*L).first != NULL) {
    /* jike list tidak kosong */
    if (countElement(*L) == 1) {
      delFirst(L);
    } else {
      /* mencari elemen terakhir list */
      elemen *last = (*L).first;
      elemen *prev;
      while (last->next != NULL) {
        /* iterasi */
        prev = last;
        last = last->next;
      }
      delAfter(prev, L);
    }
  }
}

void delAll(list *L) {
  int i;
  for (i = countElement(*L); i >= 1; i--) {
    /* proses menghapus elemen list */
    delLast(L);
  }
}

void printElement(list L) {
  if (L.first != NULL) {
    /* jika list tidak kosong */
    /* inisialisasi */
    elemen *elmt = L.first;
    int i = 1;
    while (elmt != NULL) {
      /* proses */
      printf("%s %d %c\n", elmt->elmt.nama, elmt->elmt.umur,
             elmt->elmt.jenis_kelamin);
      /* iterasi */
      elmt = elmt->next;
      i = i + 1;
    }
    printf("=====\n");
  } else {
    /* proses jika list kosong */
    printf("List Kosong\n");
    printf("=====\n");
  }
}