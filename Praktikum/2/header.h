#include <stdio.h>
#include <malloc.h>
#include <string.h>

#ifndef MAXITEM
#define MAXITEM 5
#endif

typedef struct {
  char nama[50];
  int umur;
  char jenis_kelamin;
} karyawan;

typedef struct elmt *alamatelmt;
typedef struct elmt {
  karyawan elmt;
  alamatelmt next;
} elemen;

typedef struct { elemen *first; } list;

void createList(list *L);
int countElement(list L);
void addFirst(char nama[], int umur, char jenis_kelamin, list *L);
void addAfter(elemen *prev, char nama[], int umur, char jenis_kelamin, list *L);
void addLast(char nama[], int umur, char jenis_kelamin, list *L);
void delFirst(list *L);
void delAfter(elemen *prev, list *L);
void delLast(list *L);
void delAll(list *L);
void printElement(list L);
