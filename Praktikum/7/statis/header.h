#include <stdio.h>
#include <string.h>

typedef struct { char nama[50]; } nilaiMatKul;

typedef struct {
  int first;
  int last;
  nilaiMatKul data[5];
} queue;

void createEmpty(queue *Q);
int isEmpty(queue Q);
int isFull(queue Q);
void add(char nama[], queue *Q);
void addprior(int urutan, char nama[], queue *Q);
void del(queue *Q);
void printQueue(queue Q);
