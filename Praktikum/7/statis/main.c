#include "header.h"

int main() {
  queue Q;
  createEmpty(&Q);
  int i;
  nilaiMatKul data[8];
  for (i = 0; i < 8; i++) {
    scanf("%s", data[i].nama);
  }

  printf("======\n");

  add(data[0].nama, &Q);
  add(data[1].nama, &Q);
  addprior(1, data[2].nama, &Q);
  add(data[3].nama, &Q);
  addprior(30, data[4].nama, &Q);
  printQueue(Q);
  printf("======\n");

  add(data[5].nama, &Q);
  addprior(1, data[6].nama, &Q);
  add(data[7].nama, &Q);
  printQueue(Q);
  printf("======\n");

  del(&Q);
  del(&Q);
  del(&Q);
  del(&Q);
  del(&Q);
  printQueue(Q);
  printf("======\n");

  return 0;
}
