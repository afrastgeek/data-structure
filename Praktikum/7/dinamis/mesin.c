#include "header.h"

void createEmpty(queue *Q) {
  (*Q).first = NULL;
  (*Q).last = NULL;
}

int isEmpty(queue Q) {
  int hasil = 0;
  if (Q.first == NULL) {
    hasil = 1;
  }
  return hasil;
}

int countElement(queue Q) {
  int hasil = 0;
  if (Q.first != NULL) {
    elemen *elmt;
    elmt = Q.first;

    while (elmt != NULL) {
      hasil = hasil + 1;
      elmt = elmt->next;
    }
  }
  return hasil;
}

void add(char nama[], queue *Q) {
  elemen *elmt;
  elmt = (elemen *)malloc(sizeof(elemen));
  strcpy(elmt->elmt.nama, nama);
  elmt->next = NULL;
  if ((*Q).first == NULL) {
    (*Q).first = elmt;
  } else {
    (*Q).last->next = elmt;
  }
  (*Q).last = elmt;
  elmt = NULL;
}

void addprior(int urutan, char nama[], queue *Q) {
  elemen *elmt;
  elmt = (elemen *)malloc(sizeof(elemen));
  strcpy(elmt->elmt.nama, nama);
  if (isEmpty(*Q) == 1) {
    // jika queue kosong
    (*Q).first = elmt;
    (*Q).last = elmt;
    elmt->next = NULL;
  } else {
    if (urutan > countElement(*Q)) {
      // jika masukan urutan lebih besar dari isi queue yg ada
      (*Q).last->next = elmt;
      (*Q).last = elmt;
      elmt->next = NULL;
    } else {
      if (urutan == 1) {
        // jika di awal
        elmt->next = (*Q).first;
        (*Q).first = elmt;
      } else {
        // tengah
        int i;
        elemen *prev = (*Q).first;
        for (i = 1; i < urutan - 1; i++) {
          prev = prev->next;
        }
        elmt->next = prev->next;
        prev->next = elmt;
      }
    }
  }
  elmt = NULL;
}

void del(queue *Q) {
  if ((*Q).first != NULL) {
    elemen *elmt = (*Q).first;
    if (countElement(*Q) == 1) {
      (*Q).first = NULL;
    } else {
      (*Q).first = (*Q).first->next;
      elmt->next = NULL;
    }
    free(elmt);
  }
}

void printQueue(queue Q) {
  if (Q.first != NULL) {
    elemen *elmt = Q.first;
    int i = 1;

    while (elmt != NULL) {
      printf("%s\n", elmt->elmt.nama);
      elmt = elmt->next;
      i++;
    }
  } else {
    printf("Queue Kosong!\n");
  }
}
