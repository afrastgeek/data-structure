#include "header.h"

// membuat pohon
void makeTree(char c, tree *T) {
  // membuat elemen
  simpul *node;
  node = (simpul *)malloc(sizeof(simpul));

  // mengisi kontainter
  node->info = c;
  node->sibling = NULL;
  node->child = NULL;
  (*T).root = node;
  num = 1;
}

// tambah anak
void addChild(char c, simpul *root) {
  if (root != NULL) {  // kalo root bukan null
    // membuat elemen
    simpul *node;
    node = (simpul *)malloc(sizeof(simpul));

    // mengisi kontainter
    node->info = c;
    node->child = NULL;

    // kalo belum punya child
    if (root->child == NULL) {
      node->sibling = NULL;                // belum punya sodara
      root->child = node;                  // anak pertama
    } else {                               // kalo sudah punya
      if (root->child->sibling == NULL) {  // kalo sudah punya anak pertama
        node->sibling = root->child;       // sodara ke anak pertama
        root->child->sibling = node;       // sodara ke anak baru
      } else {                             // banyak anak
        simpul *last = root->child;        // pointer ke anak pertama
        while (last->sibling !=
               root->child) {  // sebelum menemukan sibling terakhir
          last = last->sibling;
        }
        node->sibling = root->child;  // sodara anak baru ke anak pertama
        last->sibling = node;         // sodara sibling terakhir ke anak baru
      }
    }
    // node=NULL;
    num += 1;
  }
}

void delChild(char c, simpul *root) {
  // buat pointer untuk anak pertama
  simpul *node = root->child;
  if (node != NULL) {  // kalo punya anak
    //		if(node->child==NULL){
    if (node->sibling == NULL) {  // kalo cuma punya satu anak
      if (root->child->info == c) {
        root->child = NULL;
        free(node);
      } else {
        printf("tidak ada simpul anak dengan info karakter masukan\n");
      }
    } else {                // jika punya banyak anak
      simpul *prec = NULL;  // simpul sebelum dari yang akan di hapus
      // cari simpul yang mau dihapus
      int ketemu = 0;
      while ((node->sibling != root->child) &&
             (ketemu == 0)) {   // sebelum ngecek sampe akhir dan belum ketemu
        if (node->info == c) {  // kalo sama
          ketemu = 1;
        } else {  // kalo ga, maju
          prec = node;
          node = node->sibling;
        }
      }
      if (ketemu == 0 && node->info == c) {  // kalo belum ketemu dan anak
                                             // terakhir merupakan yang ingin
                                             // dihapus
        ketemu = 1;
      }
      if (ketemu == 1) {  // kalo ketemu yang mau dihapus
        // mencari anak terakhir
        simpul *last = root->child;
        while (last->sibling != root->child) {
          last = last->sibling;
        }
        if (prec == NULL) {
          // jika yang dihapus anak pertama
          if (node->sibling == last && last->sibling == root->child) {
            // jika cuma ada 2 anak
            root->child = last;
            last->sibling = NULL;
            node->sibling = NULL;
          } else {  // kalo banyak anak
            root->child = node->sibling;
            last->sibling = root->child;
            node->sibling = NULL;
          }
        } else {
          if (prec == root->child && last->sibling == root->child) {
            // jika hanya ada 2 anak, dan yang dihapus anak kedua
            root->child->sibling = NULL;
            node->sibling = NULL;
          } else {  // kalo banyak anak dan yang di hapus di tengah
            prec->sibling = node->sibling;
            node->sibling = NULL;
          }
        }
        free(node);
      } else {
        printf("tidak ada simpul anak dengan info karakter masukan\n");
      }
      //	}
    }
    num -= 1;
  }
}

// mencari simpul
simpul *findSimpul(char c, simpul *root) {
  simpul *hasil = NULL;     // inisialisasi hasil dengan null
  if (root != NULL) {       // kalo root nya ga null
    if (root->info == c) {  // kalo root adalah simpul yang di cari
      hasil = root;
    } else {                          // kalo root bukan simpul yang dicari
      simpul *node = root->child;     // inisialisasi node dengan child
      if (node != NULL) {             // kalo node ga null
        if (node->sibling == NULL) {  // kalo node adalah anak tunggal
          if (node->info == c) {      // kalo node adalah simpul yang dicari
            hasil = node;
          } else {  // kalo belum nemu
            hasil = findSimpul(c, node);
          }
        } else {  // kalo node bukan anak tunggal
          int ketemu = 0;
          while (
              (node->sibling != root->child) &&
              (ketemu ==
               0)) {  // selama belum nyari sampe sodara terakhir dan belum nemu
            if (node->info == c) {  // kalo ketemu
              hasil = node;
              ketemu = 1;
            } else {  // kalo belum ketemu
              hasil = findSimpul(c, node);
              node = node->sibling;
            }
          }
          if (ketemu == 0) {        // kalo belum ketemu, cek anak bungsu
            if (node->info == c) {  // kalo ketemu
              hasil = node;
            } else {  // kalo belum
              hasil = findSimpul(c, node);
            }
          }
        }
      }
    }
  }
  return hasil;
}
// print secara pre order
void printTreePreOrder(simpul *root) {
  if (root != NULL) {  // kalo root nya ga null
    printf("%c", root->info);
    counter += 1;
    if (counter != num) {
      printf(" ");
    }
    simpul *node = root->child;     // node diisi child dari root
    if (node != NULL) {             // kalo node ga null
      if (node->sibling == NULL) {  // kalo node anak tunggal
        printTreePreOrder(node);
      } else {  // kalo node bukan anak tunggal
        while (node->sibling !=
               root->child) {  // selama belum sampe anak terakhir
          printTreePreOrder(node);
          node = node->sibling;
        }
        printTreePreOrder(node);
      }
    }
  }
}

// print secara postorder
void printTreePostOrder(simpul *root) {
  if (root != NULL) {               // kalo root ga null
    simpul *node = root->child;     // node diisi anak root
    if (node != NULL) {             // kalo node ga null
      if (node->sibling == NULL) {  // kalo node anak tunggal
        printTreePostOrder(node);
      } else {  // kalo node punya sodara
        while (node->sibling !=
               root->child) {  // selama belum sampe anak terakhir
          printTreePostOrder(node);
          node = node->sibling;
        }
        printTreePostOrder(node);
      }
    }
    printf(" %c ", root->info);
  }
}

// menyalin pohon
void copyTree(simpul *root1, simpul **root2) {
  if (root1 != NULL) {  // kalo root1 bukan null
    *root2 = (simpul *)malloc(sizeof(simpul));
    // proses copy
    (*root2)->info = root1->info;
    (*root2)->sibling = NULL;
    (*root2)->child = NULL;
    if (root1->child != NULL) {             // kalo root 1 punya anak
      if (root1->child->sibling == NULL) {  // kalo root cuma punya 1 anak
        copyTree(root1->child, &(*root2)->child);
      } else {  // kalo root punya banyak anak
        simpul *node1 = root1->child;
        simpul *node2 = (*root2)->child;
        while (node1->sibling !=
               root1->child) {  // selama belum cek sampe anak terakhir
          copyTree(node1, &(node2));
          node1 = node1->sibling;
          node2 = node2->sibling;
        }
        // proses anak terakhir
        copyTree(node1, &(node2));
      }
    }
  }
}

// cek kesamaan 2 pohon
int isEqual(simpul *root1, simpul *root2) {
  int hasil = 1;
  if ((root1 != NULL) && (root2 != NULL)) {  // kalo root1 dan root2 ga null
    if (root1->info != root2->info) {        // kalo ga sama
      hasil = 0;
    } else {  // kalo sama
      if ((root1->child != NULL) &&
          (root2->child != NULL)) {  // kalo root1 dan root 2 punya anak
        if (root1->child->sibling == NULL) {  // kalo root1 cuma punya 1 anak
          hasil = isEqual(root1->child, root2->child);
        } else {  // kalo punya banyak anak
          simpul *node1 = root1->child;
          simpul *node2 = root2->child;
          while (node1->sibling !=
                 root1->child) {  // selama belum cek sampe anak terakhir
            if ((node1 != NULL) && (node2 != NULL)) {
              hasil = isEqual(node1, node2);
              node1 = node1->sibling;
              node2 = node2->sibling;
            } else {
              hasil = 0;
              break;
            }
          }
          // cek anak terakhir
          hasil = isEqual(node1, node2);
        }
      }
    }
  } else {
    if ((root1 != NULL) || (root2 != NULL)) {  // kalo salah satu null
      hasil = 0;
    }
  }
  return hasil;
}
