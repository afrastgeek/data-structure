#include "header.h"

int main(int argc, char const *argv[]) {
  tree T;

  makeTree('A', &T);
  addChild('B', T.root);
  delChild('B', T.root);
  addChild('C', T.root);
  addChild('D', T.root);
  addChild('E', T.root->child);
  addChild('F', T.root->child->child);
  addChild('G', T.root->child->child->child);
  addChild('H', T.root);

  counter = 0;
  printTreePreOrder(T.root);
  printf("\n");

  delChild('H', T.root);

  counter = 0;
  printTreePreOrder(T.root);
  printf("\n");

  addChild('I', T.root->child);
  addChild('J', T.root->child->child);

  counter = 0;
  printTreePreOrder(T.root);
  printf("\n");

  findSimpul('C', T.root->child->child);

  simpul *node;
  node = (simpul *)findSimpul('C', T.root->child->child);

  if (node != NULL) {
    printf("ditemukan C\n");
  } else {
    printf("tidak ditemukan\n");
  }

  node = (simpul *)findSimpul('C', T.root);

  if (node != NULL) {
    printf("ditemukan C\n");
  } else {
    printf("tidak ditemukan\n");
  }

  return 0;
}
