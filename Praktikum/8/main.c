#include "header.h"

int main(int argc, char const *argv[]) {
  char query;
  scanf(" %c", &query);

  tree T;
  makeTree(query, &T);

  scanf(" %c", &query);
  addLeft(query, T.root);
  scanf(" %c", &query);
  addRight(query, T.root);
  scanf(" %c", &query);
  addLeft(query, T.root->left);
  scanf(" %c", &query);
  addRight(query, T.root->left);
  scanf(" %c", &query);
  addRight(query, T.root->right);
  scanf(" %c", &query);
  addLeft(query, T.root->left->left);
  scanf(" %c", &query);
  addRight(query, T.root->left->left);
  scanf(" %c", &query);
  addRight(query, T.root->left->right);
  scanf(" %c", &query);
  addLeft(query, T.root->right->right);
  scanf(" %c", &query);
  addRight(query, T.root->right->right);
  scanf(" %c", &query);
  addLeft(query, T.root->left->left->right);
  scanf(" %c", &query);
  addRight(query, T.root->left->left->right);
  scanf(" %c", &query);
  addLeft(query, T.root->right->right->left);

  printf("Pre\n");
  num_item = 0;
  printTreePreOrder(T.root);
  delLeft(T.root->left->left->right);  // L Hilang
  printf("\nIn\n");
  num_item = 0;
  printTreeInOrder(T.root);
  printf("\nPost\n");
  num_item = 0;
  delRight(T.root->left->right);  // I Terhapus
  printTreePostOrder(T.root);

  return 0;
}
