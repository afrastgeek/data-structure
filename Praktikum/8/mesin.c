#include "header.h"

/*membuat pohon*/
void makeTree(char c, tree *T) {
  // membuat simpul dengan alokasi di memory
  simpul *node;
  node = (simpul *)malloc(sizeof(simpul));

  // memasukan data kedalam pohon
  node->info = c;
  /*inisialisasi*/
  node->right = NULL;
  node->left = NULL;
  // root memegang data paling atas
  (*T).root = node;
  tree_item = 1;
}

/*menambah sub pohon bagian kanan*/
void addRight(char c, simpul *root) {
  if (root->right == NULL) {
    /*jika sub pohon kanan kosong*/
    // membuat simpul dengan alokasi di memory
    simpul *node;
    node = (simpul *)malloc(sizeof(simpul));
    // memasukan nama pohon
    node->info = c;
    /*inisialisasi*/
    node->right = NULL;
    node->left = NULL;
    // tangan kanan memegang sub pohon kanan baru
    root->right = node;
  } else {
    /*jika sub pohon kanan telah terisi*/
    printf("sub pohon kanan telah terisi\n");
  }
  tree_item += 1;
}

/*menambah sub pohon bagian kiri*/
void addLeft(char c, simpul *root) {
  if (root->left == NULL) {
    /*jika sub pohon kiri kosong*/
    // membuat simpul dengan alokasi di memory
    simpul *node;
    node = (simpul *)malloc(sizeof(simpul));
    // memasukan nama pohon
    node->info = c;
    /*inisialisasi*/
    node->right = NULL;
    node->left = NULL;
    // tangan kiri memegang sub pohon kiri baru
    root->left = node;
    tree_item += 1;
  } else {
    /*jika sub pohon kiri telah terisi*/
    printf("sub pohon kiri telah terisi\n");
  }
}

/*menghapus sub pohon kanan*/
void delRight(simpul *root) {
  if (root != NULL) {
    /*inisialisasi*/
    simpul *node = root->right;
    if (node != NULL) {
      // jika tangan pohon tidak memegang apapun
      if (node->right == NULL && node->left == NULL) {
        // proses
        root->right = NULL;
        free(node);
        tree_item -= 1;
      } else {
        // jika salah satu tangan memegang sub pohon
        printf("anaknya kemanain pak?\n");
      }
    }
  }
}

/*menghapus sub pohon kiri*/
void delLeft(simpul *root) {
  if (root != NULL) {
    /*inisialisasi*/
    simpul *node = root->left;
    if (node != NULL) {
      // jika tangan pohon tidak memegang apapun
      if ((node->right == NULL) && (node->left == NULL)) {
        // proses
        root->left = NULL;
        free(node);
        tree_item -= 1;
      } else {
        // jika salah satu tangan memegang sub pohon
        printf("anaknya kemanain pak?\n");
      }
    }
  }
}

/*print pohon dari bapak, kiri, kanan*/
void printTreePreOrder(simpul *root) {
  if (root != NULL) {
    /*proses jika pohon tidak kosong*/
    printf("%c", root->info);
    if (num_item < tree_item - 1) {
      printf(" - ");
    }
    num_item += 1;
    printTreePreOrder(root->left);
    printTreePreOrder(root->right);
  }
}

/*print pohon dari kiri, bapak, kanan*/
void printTreeInOrder(simpul *root) {
  if (root != NULL) {
    /*proses jika pohon tidak kosong*/
    printTreeInOrder(root->left);
    printf("%c", root->info);
    if (num_item < tree_item - 1) {
      printf(" - ");
    }
    num_item += 1;
    printTreeInOrder(root->right);
  }
}

/*print pohon dari kiri, kanan, bapak*/
void printTreePostOrder(simpul *root) {
  if (root != NULL) {
    /*proses jika pohon tidak kosong*/
    printTreePostOrder(root->left);
    printTreePostOrder(root->right);
    printf("%c", root->info);
    if (num_item < tree_item - 1) {
      printf(" - ");
    }
    num_item += 1;
  }
}

/*menyalin pohon ke pohon baru*/
void copyTree(simpul *root1, simpul **root2) {
  if (root1 != NULL) {
    // membuat simpul dengan alokasi di memory
    (*root2) = (simpul *)malloc(sizeof(simpul));
    /*inisialisasi*/
    (*root2)->info = root1->info;

    /*proses menyalin pohon ke pohon baru*/
    if (root1->left != NULL) {
      // jika tangan kiri pohon tidak kosong
      /*proses*/
      copyTree(root1->left, &(*root2)->left);
    } else {
      // jika tangan kiri pohon kosong
      /*proses*/
      (*root2)->left = NULL;
    }

    if (root1->right != NULL) {
      // jika tangan kanan pohon tidak kosong
      /*proses*/
      copyTree(root1->right, &(*root2)->right);
    } else {
      // jika tangan kanan pohon kosong
      /*proses*/
      (*root2)->right = NULL;
    }
  }
}

/*memeriksa apakah dua pohon itu sama atau tidak*/
int isEqual(simpul *root1, simpul *root2) {
  /*anggap pohon sama */
  int hasil = 1;
  // jika pohon tidak kosong
  if ((root1 != NULL) && (root2 != NULL)) {
    /*pengecekan jika tidak sama*/
    if (root1->info != root2->info) {
      /*proses*/
      hasil = 0;
    } else {
      /*jika sama cek selanjutnya*/
      isEqual(root1->left, root2->left);
      isEqual(root1->right, root2->right);
    }
  } else {
    // jika pohon kosong
    hasil = 0;
  }
  // melempar hasil
  return hasil;
}
