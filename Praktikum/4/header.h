#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef struct {
  char nama[50];
  char konstelasi[50];
} dataBintang;

typedef struct elmt *alamatelmt;
typedef struct elmt {
  dataBintang elmt;
  alamatelmt prev;
  alamatelmt next;
} elemen;

typedef struct {
  elemen *first;
  elemen *tail;
} list;

void createList(list *L);

int countElement(list L);

void addFirst(char nama[], char konstelasi[], list *L);

void addAfter(elemen *prev, char nama[], char konstelasi[], list *L);

void addLast(char nama[], char konstelasi[], list *L);

void delFirst(list *L);

void delAfter(elemen *prev, list *L);

void delLast(list *L);

void printElement(list L);

void printElementToHead(list L);
