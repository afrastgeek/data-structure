#include "header.h"

int main(int argc, char const *argv[]) {
  int i;
  dataBintang query[6];
  for (i = 0; i < 6; i += 1) {
    scanf(" %s %s", query[i].nama, query[i].konstelasi);
  }

  list L;
  createList(&L);

  addFirst(query[0].nama, query[0].konstelasi, &L);
  addFirst(query[1].nama, query[1].konstelasi, &L);
  addAfter(L.first, query[2].nama, query[2].konstelasi, &L); 	
  printElement(L);

  delAfter(L.first->next->prev, &L); 
  addLast(query[3].nama, query[3].konstelasi, &L);
  printElement(L);

  delFirst(&L);
  addLast(query[4].nama, query[4].konstelasi, &L);
  addAfter(L.first->next, query[5].nama, query[5].konstelasi, &L); 
  delLast(&L);
	printElementToHead(L);

  return 0;
}

/*

(LGD001) List Ganda Dinamis


Pembuat Soal: Asisten Pemrograman 6

Batas Waktu Eksekusi	5 Detik
Batas Memori	1 MB

Buatlah sebuah list ganda dinamis untuk menyimpan daftar namba bintang dan konstelasinya.
Lakukan perintah - perintah list dengan urutan berikut :

addFirst
addFirst
addAfter L.first
printElement

delAfter L.first->next->prev
addLast
printElement

delFirst
addLast
addAfter L.first->next
delLast
printElementToHead

Format Masukan :
Nama bintang, dan konstelasinya sebanyak 6

Format Keluaran :
List nama bintang (di setiap setelah perintah print ditambah pembatas berupa -----

Contoh Masukan

Fornacis Fornax
Bellatrix Orion
Alpheratz Andromeda
Vega Lyra
Aldebaran Taurus
Menkalinan Auriga


Contoh Keluaran

Bellatrix Orion
Alpheratz Andromeda
Fornacis Fornax
-----
Bellatrix Orion
Fornacis Fornax
Vega Lyra
-----
Menkalinan Auriga
Vega Lyra
Fornacis Fornax
-----

 */