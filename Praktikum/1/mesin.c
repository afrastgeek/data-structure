#include "header.h"

void createList(list *L) {
  (*L).first = FIN;
  /* why use parentheses here? */ /* PASS BY VALUE, PASS BY REFERENCE */
  int i;

  for (i = 0; i < MAXITEM; i++) {
    /* proses menginisialisasi isi array */
    (*L).data[i].next = INIT; /* is this the example of inheritance? */
  }
}

int countElement(list L) {
  int hasil = 0;
  if (L.first != FIN) {
    /* list tidak kosong*/
    int elmt;

    /* inisialisasi */
    elmt = L.first;

    while (elmt != FIN) {
      /* proses */
      hasil = hasil + 1;

      /* iterasi */
      elmt = L.data[elmt].next;
    }
  }
  return hasil;
}

int emptyElement(list L) {
  /* tolong di hijaukan. */
  int hasil = FIN;

  if (countElement(L) < MAXITEM) {
    int ketemu = 0;

    int i = 0;
    while ((ketemu == 0) && (i < MAXITEM)) { /* why '&&' instead of '||' ? */
      if (L.data[i].next == INIT) {
        hasil = i;
        ketemu = 1;
      } else {
        i = i + 1;
      }
    }
  }
  return hasil;
}

void addFirst(char nama[], int umur, char jenis_kelamin[], list *L) {
  if (countElement(*L) < MAXITEM) { /* static addFirst */
    int baru = emptyElement(*L);

    /* does PBV and PBR have different pointer number? what does *L and L
     * differ in program? */
    strcpy((*L).data[baru].elmt.nama, nama);
    (*L).data[baru].elmt.umur = umur;
    strcpy((*L).data[baru].elmt.jenis_kelamin, jenis_kelamin);

    if ((*L).first == FIN) {
      /* jika list kosong */
      (*L).data[baru].next = FIN;
    } else {
      /* jika list tidak kosong */
      (*L).data[baru].next = (*L).first;
    }

    (*L).first = baru;
  } else {
    /* proses jika array penuh */
    printf("sudah tidak dapat ditambah\n");
    printf("======\n");
  }
}

void addAfter(int prev, char nama[], int umur, char jenis_kelamin[], list *L) {
  if (countElement(*L) < MAXITEM) { /* static addFirst */
    int baru = emptyElement(*L);

    /* does PBV and PBR have different pointer number? what does *L and L
     * differ in program? */
    strcpy((*L).data[baru].elmt.nama, nama);
    (*L).data[baru].elmt.umur = umur;
    strcpy((*L).data[baru].elmt.jenis_kelamin, jenis_kelamin);

    if ((*L).data[prev].next == FIN) {
      /* jika list kosong */
      (*L).data[baru].next = FIN;
    } else {
      /* jika list tidak kosong */
      (*L).data[baru].next = (*L).data[prev].next;
    }

    (*L).data[prev].next = baru;
  } else {
    /* proses jika array penuh */
    printf("sudah tidak dapat ditambah\n");
    printf("======\n");
  }
}

void addLast(char nama[], int umur, char jenis_kelamin[], list *L) {
  if ((*L).first == FIN) {
    /* proses jika list masih kosong */
    addFirst(nama, umur, jenis_kelamin, L);
  } else {
    /* proses jika list telah berisi element */
    if (countElement(*L) < MAXITEM) {
      /* proses jika array belum penuh */
      /* proses mencari element terakhir */
      /* inisialisasi */
      int prev = (*L).first;

      while ((*L).data[prev].next !=
             FIN) { /* we could create a walker function */
        /* iterasi */
        prev = (*L).data[prev].next;
      }

      addAfter(prev, nama, umur, jenis_kelamin, L);
    } else {
      /* proses jika array penuh */
      printf("sudah tidak dapat ditambah\n");
    }
  }
}

void delFirst(list *L) {
  if ((*L).first != FIN) {
    int hapus = (*L).first;
    if (countElement(*L) == 1) {
      (*L).first = FIN;
    } else {
      (*L).first = (*L).data[(*L).first].next;
    }
    /* elemen awal sebelumnya dikosongkan */
    (*L).data[hapus].next = INIT;
  } else {
    /* proses jika list kosong */
    printf("List Kosong!\n");
    printf("======\n");
  }
}

void delAfter(int prev, list *L) {
  int hapus = (*L).data[prev].next;

  if (hapus != FIN) {
    if ((*L).data[hapus].next == FIN) {
      (*L).data[prev].next = FIN;
    } else {
      (*L).data[prev].next = (*L).data[hapus].next;
    }
    /* pengosongan elemen */
    (*L).data[hapus].next = INIT;
  }
}

void delLast(list *L) {
  if ((*L).first != FIN) {
    if (countElement(*L) == 1) {
      /* proses jika list hanya berisi satu elemen */
      delFirst(L);
    } else {
      int hapus = (*L).first;
      int prev;
      while ((*L).data[hapus].next != FIN) {
        /* iterasi */
        prev = hapus;
        hapus = (*L).data[hapus].next;
      }
      delAfter(prev, L);
    }
  } else {
    /* proses jika list kosong */
    printf("list kosong\n");
    printf("======\n");
  }
}

void printElement(list L) {
  if (L.first != FIN) {
    /* inisialisasi */
    int elmt = L.first;
    int i = 1;
    while (elmt != FIN) {
      /* proses */
      printf("%s %d %s\n", L.data[elmt].elmt.nama, L.data[elmt].elmt.umur,
             L.data[elmt].elmt.jenis_kelamin);

      /* iterasi */
      elmt = L.data[elmt].next;
      i = i + 1;
    }
    printf("======\n");
  } else {
    /* proses jika list kosong */
    printf("List Kosong!\n");
    printf("======\n");
  }
}

void delAll(list *L) {
  int i;
  for (i = countElement(*L); i >= 1; i--) {
    /* proses menghapus elemen list */
    delLast(L);
  }
}