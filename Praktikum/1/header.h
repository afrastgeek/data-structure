// deklarasi elemen
#include <stdio.h>
#include <string.h>

#ifndef MAXITEM
#define MAXITEM 5
#endif

#ifndef FIN
#define FIN -1
#endif

#ifndef INIT
#define INIT -2
#endif

typedef struct {
  char nama[50];
  int umur;
  char jenis_kelamin[2];
} karyawan;

typedef struct {
  int first;
  struct {
    karyawan elmt;
    int next;
  } data[MAXITEM];
} list;

void createList(list *L);
int countElement(list L);
int emptyElement(list L);
void addFirst(char nama[], int umur, char jenis_kelamin[], list *L);
void addAfter(int prev, char nama[], int umur, char jenis_kelamin[], list *L);
void addLast(char nama[], int umur, char jenis_kelamin[], list *L);
void delFirst(list *L);
void delAfter(int prev, list *L);
void delLast(list *L);
void printElement(list L);
void delAll(list *L);