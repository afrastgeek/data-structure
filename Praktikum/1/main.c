#include "header.h"

int main(int argc, char const *argv[]) {
  karyawan data[MAXITEM];
  int i;

  for (i = 0; i < MAXITEM; i += 1) {
    scanf("%s %d %s", data[i].nama, &data[i].umur, data[i].jenis_kelamin);
  }
  
  list L;
  createList(&L);

  addFirst(data[0].nama, data[0].umur, data[0].jenis_kelamin, &L);
  addFirst(data[1].nama, data[1].umur, data[1].jenis_kelamin, &L);
  addLast(data[2].nama, data[2].umur, data[2].jenis_kelamin, &L);
  delLast(&L);
  printElement(L);
  addAfter(L.data[L.data[L.first].next].next, data[3].nama, data[3].umur,
           data[3].jenis_kelamin, &L);
  printElement(L);
  addFirst(data[4].nama, data[4].umur, data[4].jenis_kelamin, &L);
  delAfter(L.first, &L);
  delLast(&L);
  printElement(L);
  delAll(&L);
  printElement(L);

  return 0;
}