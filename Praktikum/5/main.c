#include "header.h"

int main(int argc, char const *argv[])
{
  list L;
  createList(&L);

  int num_char;
  scanf("%d", &num_char);

  int i;
  char alphabet;
  char word[32];
  eBaris *current;
  for (i = 0; i < num_char; i += 1) {
    scanf(" %c", &alphabet);
    addLastB(alphabet, &L);
    if (i == 0) {
      current = L.first;
    } else {
      current = current -> next;
    }
    scanf(" %s", word);
    addLastK(word, current);
    scanf(" %s", word);
    addAfterK(current->col, word);
    scanf(" %s", word);
    addLastK(word, current);
    scanf(" %s", word);
    addFirstK(word, current);
  }

  printElement(L);

  return 0;
}
