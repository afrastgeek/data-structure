#include "header.h"

void createList(list *L) { (*L).first = NULL; }

int countElementB(list L) {
  int hasil = 0;

  if (L.first != NULL) {
    /* list tidak kosong */
    eBaris *elmt;

    /* iniialisasi */
    elmt = L.first;

    while (elmt != NULL) {
      /* proses */
      hasil = hasil + 1;

      /* iterasi */
      elmt = elmt->next;
    }
  }
  return hasil;
}

int countElementK(eBaris L) {
  int hasil = 0;

  if (L.col != NULL) {
    /* list tidak kosong */
    eKolom *elmt;

    /* iniialisasi */
    elmt = L.col;

    while (elmt != NULL) {
      /* proses */
      hasil = hasil + 1;

      /* iterasi */
      elmt = elmt->next;
    }
  }
  return hasil;
}

void addFirstB(char alphabet, list *L) {
  eBaris *elmt;
  elmt = (eBaris *)malloc(sizeof(eBaris));
  elmt->elmt.alphabet = alphabet;
  elmt->col = NULL;
  if ((*L).first == NULL) {
    elmt->next = NULL;
  } else {
    elmt->next = (*L).first;
  }
  (*L).first = elmt;
  elmt = NULL;
}

void addFirstK(char word[], eBaris *L) {
  eKolom *elmt;
  elmt = (eKolom *)malloc(sizeof(eKolom));

  strcpy(elmt->elmt.word, word);

  /*selanjutnya belum dicek*/
  if ((*L).col == NULL) {
    elmt->next = NULL;
  } else {
    elmt->next = (*L).col;
  }
  (*L).col = elmt;
  elmt = NULL;
}

void addAfterB(eBaris *prev, char alphabet) {
  eBaris *elmt;
  elmt = (eBaris *)malloc(sizeof(eBaris));
  elmt->elmt.alphabet = alphabet;
  elmt->col = NULL;
  if (prev->next == NULL) {
    elmt->next = NULL;
  } else {
    elmt->next = prev->next;
  }
  prev->next = elmt;
  elmt = NULL;
}

void addAfterK(eKolom *prev, char word[]) {
  eKolom *elmt;
  elmt = (eKolom *)malloc(sizeof(eKolom));
  strcpy(elmt->elmt.word, word);

  if (prev->next == NULL) {
    elmt->next = NULL;
  } else {
    elmt->next = prev->next;
  }
  prev->next = elmt;
  elmt = NULL;
}

void addLastB(char alphabet, list *L) {
  if ((*L).first == NULL) {
    /* jika list adalah list kosong */
    addFirstB(alphabet, L);
  } else {
    /* jika list tidak kosong */
    eBaris *elmt;
    elmt = (eBaris *)malloc(sizeof(eBaris));
    elmt->elmt.alphabet = alphabet;
    elmt->next = NULL;
    elmt->col = NULL;
    /* mencari elemen terakhir list */
    eBaris *last = (*L).first;
    while (last->next != NULL) {
      /* iterasi */
      last = last->next;
    }
    last->next = elmt;
    elmt = NULL;
  }
}

void addLastK(char word[], eBaris *L) {
  if ((*L).col == NULL) {
    /* jika list adalah list kosong */
    addFirstK(word, L);
  } else {
    /* jika list tidak kosong */
    eKolom *elmt;
    elmt = (eKolom *)malloc(sizeof(eKolom));
    strcpy(elmt->elmt.word, word);
    elmt->next = NULL;
    /* mencari elemen erakhir list */
    eKolom *last = (*L).col;
    while (last->next != NULL) {
      /* iterasi */
      last = last->next;
    }
    last->next = elmt;
    elmt = NULL;
  }
}

/* TAMBAHIN:
 HARUS NGECEK
 anaknya ada atau engga.

  kasih tau masih ada anak, atau delAll anak;
 */

void delFirstB(list *L) {
  if ((*L).first != NULL) {
    /* jika list bukan list kosong */
    eBaris *elmt = (*L).first;
    if (countElementB(*L) == 1) {
      (*L).first = NULL;
    } else {
      (*L).first = (*L).first->next;
      elmt->next = NULL;
    }
    free(elmt);
  }
}

void delFirstK(eBaris *L) {
  if ((*L).col != NULL) {
    /* jika list bukan list kosong */
    eKolom *elmt = (*L).col;
    if (countElementK(*L) == 1) {
      (*L).col = NULL;
    } else {
      (*L).col = (*L).col->next;
      elmt->next = NULL;
    }
    free(elmt);
  }
}

void delAfterB(eBaris *prev) {
  eBaris *elmt = prev->next;
  if (elmt->next == NULL) {
    prev->next = NULL;
  } else {
    prev->next = elmt->next;
    elmt->next = NULL;
  }
  free(elmt);
}

void delAfterK(eKolom *prev) {
  eKolom *elmt = prev->next;
  if (elmt->next == NULL) {
    prev->next = NULL;
  } else {
    prev->next = elmt->next;
    elmt->next = NULL;
  }
  free(elmt);
}

void delLastB(list *L) {
  if ((*L).first != NULL) {
    /* jika list tidak kosong */
    if (countElementB(*L) == 1) {
      /* list terdiri dari satu elemen */
      delFirstB(L);
    } else {
      /* mencari elemen terakhir list */
      eBaris *last = (*L).first;
      eBaris *before_last;
      while (last->next != NULL) {
        /* iterasi */
        before_last = last;
        last = last->next;
      }
      before_last->next = NULL;
      free(last);
    }
  }
}

void delLastK(eBaris *L) {
  if ((*L).col != NULL) {
    /* jika list tidak kosong */
    if (countElementK(*L) == 1) {
      /* list terdiri dari satu elemen */
      delFirstK(L);
    } else {
      /* mencari elemen terakhir list */
      eKolom *last = (*L).col;
      eKolom *before_last;
      while (last->next != NULL) {
        /* iterasi */
        before_last = last;
        last = last->next;
      }
      before_last->next = NULL;
      free(last);
    }
  }
}

void printElement(list L) {
  if (L.first != NULL) {
    /* jika list tidak kosong */
    /* ini sialisasi */
    eBaris *elmt = L.first;

    while (elmt != NULL) {
      /* proses */
      printf("%c\n", elmt->elmt.alphabet);

      eKolom *eCol = elmt->col;
      while (eCol != NULL) {
        printf("%s\n", eCol->elmt.word);
        eCol = eCol->next;
      }

      /* iterasi */
      elmt = elmt->next;
    }
  } else {
    /* proses jika list kosong */
    printf("list kosong\n");
  }
}

void delAllB(list *L) {
  if (countElementB(*L) != 0) {
    int i;

    for (i = countElementB(*L); i >= 1; i--) {
      /* proses menghapus elemen list */
      delLastB(L);
    }
  }
}

void delAllK(eBaris *L) {
  if (countElementK(*L) != 0) {
    int i;

    for (i = countElementK(*L); i >= 1; i--) {
      delLastK(L);
    }
  }
}
