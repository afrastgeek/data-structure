#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef struct {
  char alphabet;
} mahasiswa;

typedef struct {
  char word[32];
} matKul;

typedef struct eklm *alamatekolom;
typedef struct eklm {
  matKul elmt;
  alamatekolom next;
} eKolom;

typedef struct ebr *alamatebaris;
typedef struct ebr {
  mahasiswa elmt;
  eKolom *col;
  alamatebaris next;
} eBaris;

typedef struct { eBaris *first; } list;

void createList(list *L);
int countElementB(list L);
int countElementK(eBaris L);
void addFirstB(char alphabet, list *L);
void addFirstK(char word[], eBaris *L);
void addAfterB(eBaris *prev, char alphabet);
void addAfterK(eKolom *prev, char word[]);
void addLastB(char alphabet, list *L);
void addLastK(char word[], eBaris *L);
void delFirstB(list *L);
void delFirstK(eBaris *L);
void delAfterB(eBaris *prev);
void delAfterK(eKolom *prev);
void delLastB(list *L);
void delLastK(eBaris *L);
void printElement(list L);
void delAllB(list *L);
void delAllK(eBaris *L);
