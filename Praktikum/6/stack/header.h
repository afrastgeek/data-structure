#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef struct { char nama[50]; } masukan;

typedef struct elmt *alamatelmt;
typedef struct elmt {
  masukan elmt;
  alamatelmt next;
} elemen;

typedef struct { elemen *top; } stack;

void printStack_1(stack S);
void printStack_2(stack S);
void pop(stack *S);
void push(char nama[], stack *S);
int countElement(stack S);
int isEmpty(stack S);
void createEmpty(stack *S);
