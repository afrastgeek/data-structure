#include "header.h"

int main() {
  stack S_1;
  stack S_2;

  createEmpty(&S_1);
  createEmpty(&S_2);

  int i;
  masukan temp[10];
  for (i = 0; i < 5; i++) {
    scanf("%s", temp[i].nama);
  }
  printStack_1(S_1);
  printStack_2(S_2);

  push(temp[0].nama, &S_1);
  push(temp[1].nama, &S_2);

  pop(&S_1);
  push(temp[0].nama, &S_2);

  printStack_1(S_1);
  printStack_2(S_2);

  pop(&S_2);
  push(temp[2].nama, &S_1);
  push(temp[3].nama, &S_2);
  push(temp[4].nama, &S_2);

  pop(&S_2);
  push(temp[4].nama, &S_1);

  printStack_2(S_2);
  printStack_1(S_1);

  return 0;
}
