#include "header.h"

void createEmpty(stack *S) { (*S).top = NULL; }

int isEmpty(stack S) {
  int hasil = 0;
  if (S.top == NULL) {
    hasil = 1;
  }
  return hasil;
}

int countElement(stack S) {
  int hasil = 0;
  if (S.top != NULL) {
    /*stack tidak kosong*/
    elemen *elmt;

    /*inisialisai*/
    elmt = S.top;

    while (elmt != NULL) {
      /*proses*/
      hasil = hasil + 1;

      /*iterasi*/
      elmt = elmt->next;
    }
  }

  return hasil;
}

void push(char nama[], stack *S) {
  elemen *elmt;
  elmt = (elemen *)malloc(sizeof(elemen));
  strcpy(elmt->elmt.nama, nama);

  elmt->next = (*S).top;
  (*S).top = elmt;
  elmt = NULL;
}

void pop(stack *S) {
  if ((*S).top != NULL) {
    /*jika stack bukan stack kosong*/

    elemen *elmt = (*S).top;
    (*S).top = (*S).top->next;
    elmt->next = NULL;
    free(elmt);
  }
}

void printStack_1(stack S) {
  if (S.top != NULL) {
    elemen *elmt = S.top;

    printf("S1 -");
    while (elmt != NULL) {
      // printf("==============\n");

      printf(" %s", elmt->elmt.nama);

      /*iterasi*/
      elmt = elmt->next;
    }
    printf("\n");
    // printf("-----------------\n");
  } else {
    /*proses jika stack kosong*/
    printf("S1 - Stack Kosong\n");
  }
}

void printStack_2(stack S) {
  if (S.top != NULL) {
    elemen *elmt = S.top;

    printf("S2 -");
    while (elmt != NULL) {
      // printf("==============\n");

      printf(" %s", elmt->elmt.nama);

      /*iterasi*/
      elmt = elmt->next;
    }
    printf("\n");

    // printf("-----------------\n");
  } else {
    /*proses jika stack kosong*/
    printf("S2 - Stack Kosong\n");
  }
}
