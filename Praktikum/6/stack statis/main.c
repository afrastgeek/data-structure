#include "header.h"

int main() {
  stack S_1;
  stack S_2;

  mahasiswa temp[10];

  int i;
  for (i = 0; i < 5; i++) {
    scanf("%s", temp[i].nama);
  }

  createEmpty(&S_1);
  createEmpty(&S_2);
  printStack1(S_1);
  printStack2(S_2);

  push(temp[0].nama, &S_1);
  push(temp[1].nama, &S_2);
  pop(&S_1);
  push(temp[0].nama, &S_2);
  printStack1(S_1);
  printStack2(S_2);

  pop(&S_2);
  push(temp[2].nama, &S_1);
  push(temp[3].nama, &S_2);
  push(temp[4].nama, &S_2);
  pop(&S_2);
  push(temp[4].nama, &S_1);
  printStack2(S_2);
  printStack1(S_1);

  // push("13507702", "Rudi", 75.11, &S);
  // push("13507703", "Dea", 84.63, &S);
  // printStack(S);
  // printf("=====================\n");

  // pop(&S);
  // pop(&S);
  // printStack(S);
  // printf("=====================\n");

  return 0;
}
