#include <stdio.h>
#include <string.h>

typedef struct { char nama[50]; } mahasiswa;

typedef struct {
  int top;
  mahasiswa data[10];
} stack;

void createEmpty(stack *S);
int isEmpty(stack S);
int isFull(stack S);
void push(char nama[], stack *S);
void pop(stack *S);
void printStack1(stack S);
void printStack2(stack S);

/*


(STC002) Stack Ngepop Statis

Pembuat Soal: Asisten Pemrograman VI

Batas Waktu Eksekusi  5 Detik
Batas Memori  1 MB

Buatlah dua buah stack statis (S1 dan S2) untuk
menampung 5 data nama-nama mahasiswa.
Lakukan perintah stack sesuai urutan berikut.

printStack S1
printStack S2
push S1
push S2
pop S1 ke S2
printStack S1
printStack S2
pop S2
push S1
push S2
push S2
pop S2 ke S1
printStack S2
printStack S1

Contoh Masukan
Fikry
Faisal
Ridwan
Fauzi
Mira


Contoh Keluaran
S1 - Stack Kosong
S2 - Stack Kosong
S1 - Stack Kosong
S2 - Fikry Faisal
S2 - Fauzi Faisal
S1 - Mira Ridwan

*/
