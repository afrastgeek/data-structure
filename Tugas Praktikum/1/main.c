/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada tugas masa depan Alpro 2 pada saat mengerjakan Tugas Praktikum 1
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "header.h"

int main(int argc, char const *argv[]) {
  list_t L;
  createList(&L);

  int index, num_data;
  random_string_t query;
  char random_string_comparator[32];

  scanf("%d", &num_data);
  for (index = 0; index < num_data; index += 1) {
    scanf("%s", query.alphanumeric);
    addLast(query, &L);
  }
  scanf("%s", random_string_comparator);

  for (index = 0; index < num_data; index += 1) {
    L.node[index].content.point = calculatePoint(
        L.node[index].content.alphanumeric, random_string_comparator);
  }

  // sortPointAsc(&L);

  printElement(L);

  return 0;
}

/*

Pada suatu ketika ada 2 orang teman bernama Lua dan Lia sedang bermain games yaitu 
mencocokan sebuah angka-huruf.
Cara mainnya adalah
- Lua mencatat angka-huruf sebanyak n 
- Lia akan mencatat 1 buah angka-huruf miliknya, dan akan di cocokan dengan angka-huruf yang Lua catat
- Setelah itu akan diketahui total angka-huruf yang sama. 

- Jika yang lebih banyak angka maka total angka&huruf yang sama akan di 2 lipat gandakan
- Tetapi, jika yang lebih banyak adalah huruf maka akan di 3 lipat gandakan
- Sedangkan jika angka-huruf yang sama, sama banyak maka tidak akan di lipat gandakan

- Hasil Akhir akan di urutkan secara ascending berdasarkan banyaknya angka-huruf yang sama setelah di lipat gandakan

Bantulah Lua dan Lia untuk menghitung ada berapa angka-huruf yang sama setelah di lipat gandakan

Format Masukan
0 < n < 100 banyaknya angka-huruf yang Lua catat sebanyak n
x angka-huruf yang Lia catat sebanyak 1

Format Keluaran
angka-huruf dan jumlah akhir nya di urutkan secara ascending

Catatan: 
- Gunakan Konsep List Statis dalam menyelesaikan masalah ini
- Angka-huruf = Sebuah kata yang terdiri dari angka/huruf 
- PROSES HITUNG & SORT dilakukan SETELAH input dimasukan ke LIST. 

- Contoh perhitungan; 
y4n999 COMPARE 9a4n3 
Total angka&huruf yang sama = 5. 
Angka = 4, huruf = 1, maka 2x Lipat; Hasil Akhir = 10;  
Contoh Masukan

5
5e8u4h
k3n4n94n
y4n999
akan
d11n94t
9a4n3


Contoh Keluaran

5e8u4h 2
d11n94t 6
akan 9
y4n999 10
k3n4n94n 14
 */