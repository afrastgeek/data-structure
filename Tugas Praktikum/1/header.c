/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada tugas masa depan Alpro 2 pada saat mengerjakan Tugas Praktikum 1
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "header.h"

/**
 * @brief      Creates a list from passed list by initializing it's first
 *             sibling value (first) and each relation value (next).
 *
 * @param      L     List given to initialize.
 */
void createList(list_t *L) {
  (*L).first = NULL_VALUE;
  int node_nth;
  for (node_nth = 0; node_nth < MAX_VALUE; node_nth++) {
    (*L).node[node_nth].next = INIT_VALUE;
  }
}

/**
 * @brief      Counts the number of element from passed list.
 *
 * @param[in]  L     List given to count it's element.
 *
 * @return     Number of element.
 */
int countElement(list_t L) {
  int result = 0;

  if (L.first != NULL_VALUE) {
    int node_nth = L.first;
    while (node_nth != NULL_VALUE) {
      result += 1;
      node_nth = L.node[node_nth].next;
    }
  }

  return result;
}

/**
 * @brief      Find first empty element from a list.
 *
 * @param[in]  L     List given to search for.
 *
 * @return     Element index with empty value.
 */
int emptyElement(list_t L) {
  int result = NULL_VALUE;

  if (countElement(L) < MAX_VALUE) {
    int not_found = 1;
    int i = 0;
    while ((not_found) && (i < MAX_VALUE)) {
      if (L.node[i].next == INIT_VALUE) {
        result = i;
        not_found = 0;
      } else {
        i += 1;
      }
    }
  }

  return result;
}

/**
 * @brief      Adds node with content at the start of a list.
 *
 * @param[in]  content  The content.
 * @param      L        The list to add node to.
 */
void addFirst(random_string_t content, list_t *L) {
  if (countElement(*L) < MAX_VALUE) { /* static addFirst */
    int node_nth = emptyElement(*L);
    (*L).node[node_nth].content = content;

    if ((*L).first == NULL_VALUE) { /* jika list kosong */
      (*L).node[node_nth].next = NULL_VALUE;
    } else { /* jika list tidak kosong */
      (*L).node[node_nth].next = (*L).first;
    }
    (*L).first = node_nth;
  } else { /* proses jika array penuh */
    printf("sudah tidak dapat ditambah\n");
  }
}

/**
 * @brief      Adds node with content to a list after given node.
 *
 * @param[in]  node_before  The node before target added node.
 * @param[in]  content      The content.
 * @param      L            The list to add node to.
 */
void addAfter(int node_before, random_string_t content, list_t *L) {
  if (countElement(*L) < MAX_VALUE) { /* static addFirst */
    int node_nth = emptyElement(*L);

    (*L).node[node_nth].content = content;

    if ((*L).node[node_before].next == NULL_VALUE) { /* jika list kosong */
      (*L).node[node_nth].next = NULL_VALUE;
    } else { /* jika list tidak kosong */
      (*L).node[node_nth].next = (*L).node[node_before].next;
    }
    (*L).node[node_before].next = node_nth;
  } else { /* proses jika array penuh */
    printf("sudah tidak dapat ditambah\n");
  }
}

/**
 * @brief      Adds node content to the end of a list.
 *
 * @param[in]  content  The content
 * @param      L        The list to add node to.
 */
void addLast(random_string_t content, list_t *L) {
  if ((*L).first == NULL_VALUE) { /* proses jika list masih kosong */
    addFirst(content, L);
  } else { /* proses jika list telah berisi element */
    if (countElement(*L) < MAX_VALUE) { /* proses jika array belum penuh */
      int node_nth = (*L).first;        /* proses mencari element terakhir */
      while ((*L).node[node_nth].next != NULL_VALUE) {
        node_nth = (*L).node[node_nth].next;
      }
      addAfter(node_nth, content, L);
    } else { /* proses jika array penuh */
      printf("sudah tidak dapat ditambah\n");
    }
  }
}

/**
 * @brief      Delete node at the start of a list.
 *
 * @param      L     The list to delete node from.
 */
void delFirst(list_t *L) {
  if ((*L).first != NULL_VALUE) {
    int node_nth = (*L).first;
    if (countElement(*L) == 1) {
      (*L).first = NULL_VALUE;
    } else {
      (*L).first = (*L).node[node_nth].next;
    }
    (*L).node[node_nth].next = INIT_VALUE;
  } else { /* proses jika list kosong */
    printf("list kosong\n");
  }
}

/**
 * @brief      Delete node after given node from a list.
 *
 * @param[in]  node_before  The node before target deleted element.
 * @param      L            The list to delete node from.
 */
void delAfter(int node_before, list_t *L) {
  int node_nth = (*L).node[node_before].next;
  if (node_nth != NULL_VALUE) {
    if ((*L).node[node_nth].next == NULL_VALUE) {
      (*L).node[node_before].next = NULL_VALUE;
    } else {
      (*L).node[node_before].next = (*L).node[node_nth].next;
    }
    (*L).node[node_nth].next = INIT_VALUE;
  }
}

/**
 * @brief      Delete node at the end of a list.
 *
 * @param      L     The list to delete node from.
 */
void delLast(list_t *L) {
  if ((*L).first != NULL_VALUE) {
    if (countElement(*L) == 1) { /* proses jika list hanya berisi satu elemen */
      delFirst(L);
    } else {
      int node_nth = (*L).first;
      int previous;
      while ((*L).node[node_nth].next != NULL_VALUE) {
        previous = node_nth;
        node_nth = (*L).node[node_nth].next;
      }
      delAfter(previous, L);
    }
  } else { /* proses jika list kosong */
    printf("list kosong\n");
  }
}

/**
 * @brief      Print all element in the list.
 *
 * @param[in]  L     The list to print node from.
 */
void printElement(list_t L) {
  if (L.first != NULL_VALUE) {
    int node_nth = L.first;
    int i = 0;
    while (node_nth != NULL_VALUE) {
      // printf("elemen ke : %d\n", i);
      // printf("alphanumeric : %s\n", L.node[node_nth].content.alphanumeric);
      // printf("point : %d\n", L.node[node_nth].content.point);
      // printf("next : %d\n", L.node[node_nth].next);
      // printf("-----------\n");
      printf("%s %d\n", L.node[node_nth].content.alphanumeric, L.node[node_nth].content.point);
      node_nth = L.node[node_nth].next;
      i += 1;
    }
  } else { /* proses jika list kosong */
    printf("list kosong\n");
  }
}

/**
 * @brief      Delete all element from a list.
 *
 * @param      L     The list to delete node from.
 */
void delAll(list_t *L) {
  int i;
  for (i = countElement(*L); i >= 1; i--) {
    delLast(L);
  }
}

/**
 * @brief      Counts the number of matching character.
 *
 * @param      first_string   The first string.
 * @param      second_string  The second string.
 *
 * @return     Number of matching character.
 */
int countMatchChar(char first_string[], char second_string[]) {
  int index, index2, count = 0;
  int first_length = strlen(first_string),
      second_length = strlen(second_string);
  for (index = 0; index < first_length; index += 1) {
    for (index2 = 0; index2 < second_length; index2 += 1) {
      if ((first_string[index] == second_string[index2]) &&
          ((first_string[index] >= 'A' && first_string[index] <= 'Z') ||
           (first_string[index] >= 'a' && first_string[index] <= 'z'))) {
        count += 1;
      }
    }
  }
  return count;
}

/**
 * @brief      Counts the number of matching number.
 *
 * @param      first_string   The first string
 * @param      second_string  The second string
 *
 * @return     Number of matching number.
 */
int countMatchNum(char first_string[], char second_string[]) {
  int index, index2, count = 0;
  int first_length = strlen(first_string),
      second_length = strlen(second_string);
  for (index = 0; index < first_length; index += 1) {
    for (index2 = 0; index2 < second_length; index2 += 1) {
      if ((first_string[index] == second_string[index2]) &&
          (first_string[index] >= '0' && first_string[index] <= '9')) {
        count += 1;
      }
    }
  }
  return count;
}

/**
 * @brief      Calculates the point.
 *
 * @param      first_string   The first string
 * @param      second_string  The second string
 *
 * @return     The point.
 */
int calculatePoint(char first_string[], char second_string[]) {
  int match_char = countMatchChar(first_string, second_string),
      match_num = countMatchNum(first_string, second_string);
  int total_match = match_char + match_num;
  if (match_num > match_char) {
    total_match *= 2;
  } else if (match_num < match_char) {
    total_match *= 3;
  }
  return total_match;
}

/**
 * @brief      Sort list by it's node point.
 *
 * @param      L     The list to sort.
 * @param[in]  low   The low index.
 * @param[in]  high  The high index.
 */
void sortPointAsc(list_t *L) {
  int this_node = (*L).first;
  int next_node = (*L).node[this_node].next;
  int prev_node = (*L).first;

  int temp;

  while (next_node != NULL_VALUE) {
    while (next_node != this_node) {
      if ((*L).node[this_node].content.point > (*L).node[next_node].content.point) {
        temp = (*L).node[prev_node].next;
        (*L).node[prev_node].next = (*L).node[this_node].next;
        (*L).node[this_node].next = (*L).node[next_node].next;
        (*L).node[next_node].next = temp;
      }
      prev_node = this_node;
      this_node = (*L).node[this_node].next;
    }
    prev_node = (*L).first;
    this_node = (*L).first;
    next_node = (*L).node[next_node].next;
  }
}