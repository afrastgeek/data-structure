/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Tugas Praktikum 4
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "GGC004.h"

int main(int argc, char const *argv[]) {
  list L;
  createList(&L);

  int num_query;
  scanf("%d", &num_query);

  int i;
  char query[32];
  for (i = 0; i < num_query; i += 1) {
    scanf(" %s", query);
    addLast(query, &L);
  }

  reorder(&L);
  printElement(L);

  return 0;
}

/*

(GGC004) Ganda Ganda Ceria


Pembuat Soal: Asisten Pemrograman 6

Batas Waktu Eksekusi  5 Detik
Batas Memori  1 MB

Gunakan list ganda dinamis untuk menyelesaikan soal ini. Diberikan n buah string masukan. Periksa dari kedua sisi list (depan belakang) dimana semua string yang memiliki huruf konsonan lebih banyak harus dipindahkan ke belakang untuk list bagian depan dan semua string yang memiliki huruf vokal lebih banyak dipindah ke depan untuk list bagian belakang. Sehingga string yang memiliki huruf vokal lebih banyak akan berada di depan dan string yang memiliki huruf konsonan lebih banyak berada di belakang.

Format Masukan:
n, 0 < n < 50, banyaknya string dalam list.
n baris string di dalam list

Format Keluaran:
n Elemen list dari depan

Contoh:
6
antares
lyra
atria
bellatrix
avior
equuleus

lyra
atria
bellatrix
avior
equuleus
antares

atria
bellatrix
avior
equuleus
antares
lyra

equuleus
atria
bellatrix
avior
antares
lyra

avior
equuleus
atria
bellatrix
antares
lyra
Contoh Masukan

6
antares
lyra
atria
bellatrix
avior
equuleus


Contoh Keluaran

avior
equuleus
atria
bellatrix
antares
lyra

Contoh Masukan 2

6
antares
lyra
bellatrix
avior
aldebaran
equuleus


Contoh Keluaran 2

equuleus
avior
aldebaran
antares
lyra
bellatrix

 */
