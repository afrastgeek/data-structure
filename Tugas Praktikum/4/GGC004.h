/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Tugas Praktikum 4
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef struct {
  char string[32];
} random_string_t;

typedef struct content *node_sibling_t;
typedef struct content {
  random_string_t content;
  node_sibling_t prev;
  node_sibling_t next;
} node_t;

typedef struct {
  node_t *first;
  node_t *tail;
} list;

void createList(list *L);
int countElement(list L);
void addFirst(char *string, list *L);
void addAfter(node_t *prev, char *string, list *L);
void addLast(char *string, list *L);
void delFirst(list *L);
void delAfter(node_t *prev, list *L);
void delLast(list *L);
void printElement(list L);
void printElementToHead(list L);
void delAll(list *L);

int countConsonant(char *string);
int countVowel(char *string);
void reorder(list *L);
