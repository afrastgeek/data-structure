/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Tugas Praktikum 4
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "GGC004.h"

/**
 * @brief      Creates a list from passed list by initializing it's first and
 *             last element to NULL.
 *
 * @param      L     List given to initialize.
 */
void createList(list *L) { (*L).first = (*L).tail = NULL; }

/**
 * @brief      Counts the number of element from list passed.
 *
 * @param[in]  L     List given to count it's element.
 *
 * @return     Number of element.
 */
int countElement(list L) {
  int num_node = 0;
  if (L.first != NULL) { /* Only process when the list isn't empty */
    node_t *this_node = L.first;
    while (this_node != NULL) { /* Iterate till found the NULL element */
      num_node = num_node + 1;
      this_node = this_node->next;
    }
  }
  return num_node;
}

/**
 * @brief      Adds node with string as content at the start of a list.
 *
 * @param      str   The string
 * @param      L     The list to add node to.
 */
void addFirst(char *str, list *L) {
  node_t *node_this = (node_t *)malloc(sizeof(node_t));
  strcpy(node_this->content.string, str);
  if ((*L).first == NULL) {
    node_this->prev = node_this->next = NULL;
    (*L).tail = node_this;
  } else {
    node_this->next = (*L).first;
    node_this->prev = NULL;
    (*L).first->prev = node_this;
  }
  (*L).first = node_this;
  node_this = NULL;
}

/**
 * @brief      Adds node with string as content to a list after given node.
 *
 * @param[in]  node_before  The node before target added node.
 * @param      str          The string
 * @param      L            The list to add node to.
 */
void addAfter(node_t *node_before, char *str, list *L) {
  node_t *node_this = (node_t *)malloc(sizeof(node_t));
  strcpy(node_this->content.string, str);
  if (node_before->next == NULL) {
    node_this->next = NULL;
    (*L).tail = node_this;
  } else {
    node_this->next = node_before->next;
    node_this->next->prev = node_this;
  }
  node_this->prev = node_before;
  node_before->next = node_this;
  node_this = NULL;
}

/**
 * @brief      Adds node with string as content to the end of a list.
 *
 * @param      str   The string
 * @param      L     The list to add node to.
 */
void addLast(char *str, list *L) {
  if ((*L).first == NULL) {
    addFirst(str, L);
  } else {
    node_t *node_this = (node_t *)malloc(sizeof(node_t));
    strcpy(node_this->content.string, str);
    node_this->next = NULL;
    (*L).tail->next = node_this;
    node_this->prev = (*L).tail;
    (*L).tail = node_this;
    node_this = NULL;
  }
}

/**
 * @brief      Delete node at the start of a list.
 *
 * @param      L     The list to delete node from.
 */
void delFirst(list *L) {
  if ((*L).first != NULL) {
    node_t *node_this = (*L).first;
    if (countElement(*L) == 1) {
      (*L).first = (*L).tail = NULL;
    } else {
      (*L).first = (*L).first->next;
      (*L).first->prev = NULL;
      node_this->next = NULL;
    }
    free(node_this);
  }
}

/**
 * @brief      Delete node after given node from a list.
 *
 * @param[in]  node_before  The node before target deleted element.
 * @param      L            The list to delete node from.
 */
void delAfter(node_t *node_before, list *L) {
  node_t *node_this = node_before->next;
  if (node_this != NULL) {
    if (node_this->next == NULL) {
      node_before->next = NULL;
    } else {
      node_before->next = node_this->next;
      node_this->next->prev = node_before;
      node_this->next = NULL;
    }
    node_this->prev = NULL;
    free(node_this);
  }
}

/**
 * @brief      Delete node at the end of a list.
 *
 * @param      L     The list to delete node from.
 */
void delLast(list *L) {
  if ((*L).first != NULL) {
    if (countElement(*L) == 1) {
      delFirst(L);
    } else {
      node_t *node_this = (*L).tail;
      (*L).tail = node_this->prev;
      (*L).tail->next = NULL;
      node_this->prev = NULL;
      free(node_this);
    }
  }
}

/**
 * @brief      Print all element in the list.
 *
 * @param[in]  L     The list to print node from.
 */
void printElement(list L) {
  if (L.first != NULL) {
    node_t *this_node = L.first;
    while (this_node != NULL) {
      printf("%s\n", this_node->content.string);
      this_node = this_node->next;
    }
  } else {
    printf("list is empty\n");
  }
}

/**
 * @brief      Print all element in the list, but from the end of list.
 *
 * @param[in]  L     f The list to print node from.
 */
void printElementToHead(list L) {
  if (L.tail != NULL) {
    node_t *this_node = L.tail;
    while (this_node != NULL) {
      printf("%s\n", this_node->content.string);
      this_node = this_node->prev;
    }
  } else {
    printf("list is empty\n");
  }
}

/**
 * @brief      Delete all element from a list.
 *
 * @param      L     The list to delete node from.
 */
void delAll(list *L) {
  int num_node = countElement(*L);
  if (num_node != 0) {
    for (; num_node; num_node -= 1) {
      delLast(L);
    }
  }
}

/**
 * @brief      Counts the number of consonant character from string passed.
 *
 * @param      str   The string
 *
 * @return     Number of consonant.
 */
int countConsonant(char *str) {
  int num_consonant = 0;
  int len = strlen(str) - 1; /* length of string without null character */
  for (; len >= 0; len -= 1) {
    if ((str[len] != 'a') && (str[len] != 'i') && (str[len] != 'u') &&
        (str[len] != 'e') && (str[len] != 'o')) {
      num_consonant += 1;
    }
  }
  return num_consonant;
}

/**
 * @brief      Counts the number of vowel character from string passed.
 *
 * @param      str   The string
 *
 * @return     Number of vowel.
 */
int countVowel(char *str) {
  int num_vowel = 0;
  int len = strlen(str) - 1; /* length of string without null character */
  for (; len >= 0; len -= 1) {
    if ((str[len] == 'a') || (str[len] == 'i') || (str[len] == 'u') ||
        (str[len] == 'e') || (str[len] == 'o')) {
      num_vowel += 1;
    }
  }
  return num_vowel;
}

/**
 * @brief      Reorder the element of given list to fulfill the requirements.
 *             First half element of the list with it's consonant is more than
 *             it's vowel get moved to the end of list. Last half element of
 *             the list with it's vowel is more than it's consonant get moved
 *             to the start of list.
 *
 * @param      L     The list to reorder.
 */
void reorder(list *L) {
  node_t *node_first_half = (*L).first; /* copy node before list got modified */
  node_t *node_last_half = (*L).tail;   /* copy node before list got modified */
  node_t *node_this;
  int num_node_half = countElement(*L) / 2; /* number of node on half list */
  int num_node = 0;
  while (node_first_half != NULL && num_node < num_node_half) {
    node_this = node_first_half;
    node_first_half = node_first_half->next;
    if (countConsonant(node_this->content.string) >
        countVowel(node_this->content.string)) {
      addLast(node_this->content.string, L);
      if (node_this->prev != NULL) {
        delAfter(node_this->prev, L);
      } else {
        delFirst(L);
      }
    }
    num_node += 1; /* increment till before number of node on half list */
  }

  while (node_last_half != NULL && num_node) {
    node_this = node_last_half;
    node_last_half = node_last_half->prev;
    if (countConsonant(node_this->content.string) <
        countVowel(node_this->content.string)) {
      addFirst(node_this->content.string, L);
      if (node_this->prev != NULL) {
        delAfter(node_this->prev, L);
      } else {
        delFirst(L);
      }
    }
    num_node -= 1; /* decrement till zero */
  }
}
