/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Tugas Praktikum 6
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef struct {
  char name[50];
  int time;
  int price;
} content_t;

typedef struct content *element_address;
typedef struct content {
  content_t content;
  element_address next;
} node_t;

typedef struct { node_t *top; } stack_t;

void createEmpty(stack_t *S);
int isEmpty(stack_t S);
int countElement(stack_t S);
void push(content_t content, stack_t *S);
void pop(stack_t *S);
void printStack(stack_t S);
int compareContent(content_t left, content_t right);
void selectProject(stack_t *in, stack_t *out);
