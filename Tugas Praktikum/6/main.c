/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Tugas Praktikum 6
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "header.h"

int main(int argc, char const *argv[]) {
  stack_t project_in;        // input project info stack
  stack_t project_selected;  // choosen project stack

  createEmpty(&project_in);  // initialize the stack
  createEmpty(&project_selected);

  int num_query;            // number of query
  scanf("%d", &num_query);  // prompt for input the number of query

  content_t query;     // create struct for input query to be pushed to stack
  while (num_query) {  // loop while the number of query is present
    scanf(" %s %d %d", query.name, &query.time, &query.price);  // query prompt
    push(query, &project_in);  // push query to project_in stack
    num_query -= 1;            // decrement the number of query variable.
  }

  selectProject(&project_in, &project_selected);  // main procedure

  printStack(project_selected);  // print the result with specified criteria

  return 0;
}

/*
(PPR01) The Winner Projects


Pembuat Soal: Asisten Pemrograman VI

Batas Waktu Eksekusi  5 Detik
Batas Memori  1 MB

Ammar bekerja di sebuah perusahaan IT yang baru 5 tahun berdiri. Setiap awal
tahun, perusahaan tersebut akan menerima n tawaran proyek. Masing-masing proyek
memiliki spesifikasi nama (x), waktu pengerjaan (t) dan profit (p) yang
berbeda-beda. Namun perusahaan tidak selalu bisa mengerjakan semua proyek
tersebut. Jika proyek yang ditawarkan terlalu banyak, mereka terkadang harus
memilih proyek mana yang akan dikerjakan, sehingga total waktu pengerjaan dari
seluruh proyek yang terpilih tidak akan lebih dari 250 hari, namun bisa
menghasilkan profit yang sebesar-besarnya. Bantulah Ammar membuat program untuk
memilih kombinasi proyek yang paling tepat!

Catatan :
Seluruh data masukan diisikan ke dalam stack lebih dulu, setelah itu baru
lakukan proses pemilihan sehingga ditemukan output yang diinginkan.
Gunakan konsep stack statis ATAU dinamis untuk menyelesaikan permasalahan ini
(bisa menggunakan beberapa stack).


Format Masukan :
n (jumlah_data_proyek)
x t p (nama_proyek waktu_pengerjaan profit) untuk setiap data proyek

Format Keluaran :
nama_nama_proyek_yang_terpilih
total_waktu_pengerjaan [spasi] total_profit_yang_bisa_didapat

Contoh Masukan

7
Muka 90 400
DamriCeria 86 140
PapaOke 54 66
Seventh 111 200
First 95 110
Trice 120 100
Knapsack 57 100


Contoh Keluaran

Seventh
Muka
201 600

Contoh Masukan 2

5
PapaOke 54 66
Seventh 111 200
First 95 100
Trice 120 100
Knapsack 57 100


Contoh Keluaran 2

PapaOke
Knapsack
Seventh
222 366

 */
