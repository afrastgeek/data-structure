/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Tugas Praktikum 6
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "header.h"

/**
 * @brief      Creates an empty stack.
 *
 * @param      S     Stack to initialize.
 */
void createEmpty(stack_t *S) { (*S).top = NULL; }

/**
 * @brief      Determines if the stack is empty.
 *
 * @param[in]  S     Stack to check.
 *
 * @return     True if empty, False otherwise.
 */
int isEmpty(stack_t S) {
  int result = 0;
  if (S.top == NULL) {
    result = 1;
  }
  return result;
}

/**
 * @brief      Counts the number of element in stack.
 *
 * @param[in]  S     The stack.
 *
 * @return     Number of element.
 */
int countElement(stack_t S) {
  int result = 0;
  if (S.top != NULL) {  // only proceed when the stack isn't empty
    node_t *this_node;
    this_node = S.top;

    while (this_node != NULL) {  // walk throughout the stack
      result = result + 1;
      this_node = this_node->next;
    }
  }
  return result;
}

/**
 * @brief      Push content to the top of stack.
 *
 * @param[in]  content  The content
 * @param      S        The stack
 */
void push(content_t content, stack_t *S) {
  node_t *this_node;
  this_node = (node_t *)malloc(sizeof(node_t));

  this_node->content = content;
  this_node->next = (*S).top;
  (*S).top = this_node;

  this_node = NULL;
}

/**
 * @brief      Pop the most top content of stack.
 *
 * @param      S     The stack
 */
void pop(stack_t *S) {
  if ((*S).top != NULL) {  // Only proceed when stack isn't empty
    node_t *this_node = (*S).top;

    (*S).top = (*S).top->next;
    this_node->next = NULL;

    free(this_node);
  }
}

/**
 * @brief      Print the element of stack, with modified output specified in
 *             TP6 Problem.
 *
 * @param[in]  S     The stack
 */
void printStack(stack_t S) {
  if (S.top != NULL) {
    node_t *this_node = S.top;

    int total_time = 0;
    int total_price = 0;

    while (this_node != NULL) {
      printf("%s\n", this_node->content.name);
      total_time += this_node->content.time;
      total_price += this_node->content.price;
      this_node = this_node->next;
    }
    printf("%d %d\n", total_time, total_price);
  } else { /*proses jika stack kosong*/
    printf("Stack Kosong\n");
  }
}

/**
 * @brief      Compare two content of content_t struct.
 *
 * @param[in]  left   The left element
 * @param[in]  right  The right element
 *
 * @return     True if match, False otherwise.
 */
int compareContent(content_t left, content_t right) {
  if (!strcmp(left.name, right.name) && left.price == right.price &&
      left.time == right.time) {
    return 1;
  }
  return 0;
}

int isProjectExist(content_t query, stack_t out) {
  node_t *that_node;
  that_node = out.top;
  while (that_node != NULL) {
    if (!compareContent(query, that_node->content)) {
      return 1;
    }
    that_node = that_node->next;
  }
  return 0;
}

/**
 * @brief      Select project with highest payment but shortest time to finish.
 *             Only select project with sum of all selected project timeframe
 *             under 250 days.
 *
 * @param      in    The input stack
 * @param      out   The output stack
 */
void selectProject(stack_t *in, stack_t *out) {
  if (in->top != NULL) {  // only proceed when input stack isn't empty.
    content_t project;
    project.price = 0;
    node_t *this_node;
    int lowest_high_price = 0;
    int total_time = 0;
    int unfinished = 1;

    if (out->top == NULL) {
      this_node = in->top;
      while (this_node != NULL) {
        if (project.price < this_node->content.price) {
          project = this_node->content;
        }
        this_node = this_node->next;
      }
      if (total_time + project.time <= 250) {
        push(project, out);
        lowest_high_price = project.price;
        total_time += project.time;
      }
    }

    while (unfinished) {
      project.price = 0;
      this_node = in->top;
      while (this_node != NULL) {
        if (project.price < this_node->content.price &&
            total_time + this_node->content.time <= 250) {
          if (isProjectExist(this_node->content, *out)) {
            project = this_node->content;
          }
        }
        this_node = this_node->next;
      }
      if (total_time + project.time <= 250) {
        push(project, out);
        lowest_high_price = project.price;
        total_time += project.time;
        unfinished = 1;
      } else {
        unfinished = 0;
      }
    }
  }  // endif input stack isn't empty
}
