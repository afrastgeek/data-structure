#include "header.h"

void createList(list *L) { (*L).first = NULL; }

int countElement(list L) {
  int num_of_node = 0;

  if (L.first != NULL) { /* list tidak kosong */

    element_t *node;

    node = L.first; /* inisialisasi */

    while (node != NULL) { /* proses */
      num_of_node += 1;

      node = node->next; /* iterasi */
    }
  }

  return num_of_node;
}

void addFirst(char message[], list *L) {
  element_t *active;
  active = (element_t *)malloc(sizeof(element_t));
  strcpy(active->content.message, message);
  if ((*L).first == NULL) {
    active->next = NULL;
  } else {
    active->next = (*L).first;
  }
  (*L).first = active;
  active = NULL;
}

void addAfter(element_t *before_target, char message[], list *L) {
  element_t *active;
  active = (element_t *)malloc(sizeof(element_t));
  strcpy(active->content.message, message);
  if (before_target->next == NULL) {
    active->next = NULL;
  } else {
    active->next = before_target->next;
  }
  before_target->next = active;
  active = NULL;
}

void addLast(char message[], list *L) {
  if ((*L).first == NULL) { /* jika list adalah list kosong */
    addFirst(message, L);
  } else { /* jika list tidak kosong */
    /* mencari element_t terakhir list */
    element_t *before_target = (*L).first;
    while (before_target->next != NULL) {
      before_target = before_target->next; /* iterasi */
    }
    addAfter(before_target, message, L);
  }
}

void delFirst(list *L) {
  if ((*L).first != NULL) { /* jika list bukan list kosong */
    element_t *target = (*L).first;
    if (countElement(*L) == 1) {
      (*L).first = NULL;
    } else {
      (*L).first = (*L).first->next;
      target->next = NULL;
    }
    free(target);
  }
}

void delAfter(element_t *before_target, list *L) {
  element_t *target = before_target->next;
  if (target != NULL) {
    if (target->next == NULL) {
      before_target->next = NULL;
    } else {
      before_target->next = target->next;
      target->next = NULL;
    }
    free(target);
  }
}

void delLast(list *L) {
  if ((*L).first != NULL) { /* jike list tidak kosong */
    if (countElement(*L) == 1) {
      delFirst(L);
    } else {
      /* mencari element_t terakhir list */
      element_t *last = (*L).first;
      element_t *before_target;
      while (last->next != NULL) { /* iterasi */
        before_target = last;
        last = last->next;
      }
      delAfter(before_target, L);
    }
  }
}

void printElement(list L) {
  if (L.first != NULL) { /* jika list tidak kosong */

    element_t *node = L.first; /* inisialisasi */
    // int i = 1;
    while (node != NULL) { /* proses */
      // printf("element_t ke : %d\n", i);
      printf("%s\n", node->content.message);
      // printf("------------\n");

      node = node->next; /* iterasi */
      // i = i + 1;         /* iterasi */
    }
  } else { /* proses jika list kosong */
    printf("list kosong\n");
  }
}

int isChar(char character) {
  if ((character >= 'A' && character <= 'Z') ||
      (character >= 'a' && character <= 'z')) {
    return 1;
  }
  return 0;
}

int isNum(char character) {
  if (character >= '0' && character <= '9') return 1;
  return 0;
}

int isValid(char string[]) {
  int length = strlen(string);
  int hasChar = 0;
  int hasSignal = 0;
  for (; length; length -= 1) {
    if (string[length - 1] == '.' || string[length - 1] == '-') {
      hasSignal = 1;
    } else if (isChar(string[length - 1])) {
      hasChar = 1;
    }
    if (hasChar && hasSignal) return 1;
  }
  return 0;
}

int isKuvukiName(char string[]) {
  int length = strlen(string);
  int num_of_char = 0;
  int has_no_num = 1;
  for (; length; length -= 1) {
    if (isChar(string[length - 1])) {
      num_of_char += 1;
    } else if (isNum(string[length - 1])) {
      has_no_num = 0;
    }
    if (num_of_char <= 5 && has_no_num) return 1;
  }
  return 0;
}

char *filterKuvukiName(char string[]) {
  int i, j;                              /* bikin i sama j buat looping */
  for (i = 0; i < strlen(string); ++i) { /* untuk setiap huruf di string */
    if (string[i] == '.' ||
        string[i] == '-') { /* kalo huruf nya titik ato strip */
      for (j = i;
           j < strlen(string) - 1; ++j) { /* untuk setiap huruf di string
                                             dimulai dari posisi huruf yang
                                             titik ato strip */
        string[j] =
            string[j +
                   1]; /* ganti huruf titik ato strip sama huruf setelahnya */
      }
      /* isi j bakal jadi panjang string yang udah dikurangi 1 */
      string[j] =
          '\0'; /* ganti karakter terakhir sama simbol terminator string */
      i = 0;    /* reset i biar mulai lagi dari awal loop karakternya */
    }
  }
  return string;
}

void messageFilter(list *L) {
  element_t *node = (*L).first; /* inisialisasi */
  element_t *before_target = (*L).first;
  int filtered = 0;

  while (node != NULL) {
    if (isValid(node->content.message) && isKuvukiName(node->content.message)) {
      strcpy(node->content.message, filterKuvukiName(node->content.message));
    } else {
      while (before_target != NULL &&
             strcmp(before_target->content.message, node->content.message)) {
        before_target = before_target->next;
      }
      delAfter(before_target, L);
      filtered += 1;
    }
    node = node->next;
  };
  printf("%d\n", filtered);
}