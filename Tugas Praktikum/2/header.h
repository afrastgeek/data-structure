/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada tugas masa depan Alpro 2 pada saat mengerjakan Tugas Praktikum 2
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef struct { char message[32]; } content_t;

typedef struct content *element_address_t;
typedef struct content {
  content_t content;
  element_address_t next;
} element_t;

typedef struct { element_t *first; } list;

void createList(list *L);
int countElement(list L);
void addFirst(char message[], list *L);
void addAfter(element_t *before_target, char message[], list *L);
void addLast(char message[], list *L);
void delFirst(list *L);
void delAfter(element_t *before_target, list *L);
void delLast(list *L);
void printElement(list L);

int isChar(char character);
int isNum(char character);
int isValid(char string[]);
int isKuvukiName(char string[]);

void messageFilter(list *L);