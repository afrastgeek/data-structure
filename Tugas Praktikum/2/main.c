#include "header.h"

int main(int argc, char const *argv[]) {
  list L;
  createList(&L);
  // printElement(L);

  char query[32];
  while (strcmp(query, "DONE")) {
    scanf(" %s", query);
    if (strcmp(query, "DONE")) {
      addLast(query, &L);
    }
  };

  printf("==== Received Messages ====\n");
  printElement(L);

  printf("==== Filtered Messages ====\n");
  messageFilter(&L);

  printf("=== Valid Messages List ===\n");
  printElement(L);

  return 0;
}

// mampu menerima sinyal selama 1 hari penuh
//   untuk menerima daftar nama yang harus ditemukan

// sinyal  yang berisi nama atau inisial target
//   ada - atau .
//   ada 1 karakter huruf
// isvalid -> masuk list

// kriteria nama atau alias:
//   maks 5 huruf
//   tidak ada angka

// get name sort asc

// use 1 list
// del invalid

/*
Tobi adalah seorang agen intelijen dari negeri Kuvukiland. Ia ditugaskan oleh pemerintahnya untuk pergi mengembara dan menyelamatkan para penduduk negeri Kuvukiland yang diculik oleh negeri lain. Ia mendapat perintah untuk membuat program yang mampu menerima sinyal selama 1 hari penuh untuk menerima daftar nama yang harus ditemukannya. Menurut pemerintah, sinyal yang berisi nama atau inisial target memiliki ciri sebagai berikut :

Memiliki sinyal khusus panjang (Strip / - ) dan/atau pendek (Titik / . )
Memiliki paling tidak 1 karakter huruf
Tobi juga harus membuat program tersebut menyaring sinyal yang sudah masuk agar sesuai dengan kriteria nama atau alias penduduk Kuvukiland, yaitu :

Memiliki maksimal 5 karakter huruf
Tidak memiliki karakter angka
Bantulah Tobi mendapatkan daftar nama targetnya yang sudah diurutkan dari atas!

Catatan : Usahakan menggunakan 1 list saja dan usahakan data yang tidak valid dihapus (gunakan fungsi mesin del*)
Format Masukan

Kode sinyal sebanyak-banyaknya hingga ditemukan kata DONE
Format Keluaran

Isi Daftar/List semua sinyal yang diterima, jika tidak ada, beri pesan khusus untuk Tobi
Jumlah sinyal yang disaring/dihapus/tidak valid
Isi Daftar/List nama yang valid saja dan sudah diurutkan, jika kosong beri pesan pada Tobi untuk pulang
Contoh Masukan

FS-A.
M.F---.-R
LPM4ST3R-666
-.-.-.-.-.-.-
M-i-r-a.N
D4D4NG
SADLIFE
HELPME
M.N.
M-Rn
F-AF
DONE


Contoh Keluaran

==== Received Messages ====
FS-A.
M.F---.-R
LPM4ST3R-666
-.-.-.-.-.-.-
M-i-r-a.N
D4D4NG
SADLIFE
HELPME
M.N.
M-Rn
F-AF
==== Filtered Messages ====
5
=== Valid Messages List ===
FAF
FSA
MFR
MN
MRn
MiraN

Contoh Masukan 2

DONE


Contoh Keluaran 2

==== Received Messages ====
No signal received.
==== Filtered Messages ====
0
=== Valid Messages List ===
Return to base immediately.

 */