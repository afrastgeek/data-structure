/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Tugas Praktikum 8
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef struct node_s *node;
typedef struct node_s {
  char name[50];
  int point;
  node right;
  node left;
} node_t;

typedef struct { node_t *root; } tree;

void makeTree(char *query_name, int query_point, tree *T);
void addRight(char *query_name, int query_point, node_t *root);
void addLeft(char *query_name, int query_point, node_t *root);
void delRight(node_t *root);
void delLeft(node_t *root);
void printTreePreOrder(node_t *root);
void printTreeInOrder(node_t *root);
void printTreePostOrder(node_t *root);
void copyTree(node_t *root1, node_t **root2);
int isEqual(node_t *root1, node_t *root2);
void traverseTreePreOrder(node_t *root, int selector, char *to_find,
                          char name[64][32], int point);
