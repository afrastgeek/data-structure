/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Tugas Praktikum 8
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "header.h"

int main() {
  tree T;
  makeTree("Hadi", 10, &T);

  addLeft("Ando", 20, T.root);
  addLeft("Euis", 22, T.root->left);
  addRight("Otun", 23, T.root->left);
  addLeft("Dodi", 30, T.root->left->right);
  addRight("Ebta", 12, T.root->left->right);

  addRight("Nesa", 15, T.root);
  addLeft("Yeni", 25, T.root->right);
  addRight("Atik", 20, T.root->right);
  addLeft("Kiki", 25, T.root->right->right);
  addRight("Akbar", 21, T.root->right->right);

  int num_query;
  scanf("%d", &num_query);
  char query[num_query][50];

  int i;
  for (i = 0; i < num_query; i++) {
    scanf("%s", query[i]);
  }

  for (i = 0; i < num_query; i++) {  // loop for each of inputted name
    char name[64][32];               // array of name
    int point = 0;                   // for storing calculated point
    int selector = 0;
    traverseTreePreOrder(T.root, selector, query[i], name, point);
  }

  return 0;
}
