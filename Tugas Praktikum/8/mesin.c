/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Tugas Praktikum 8
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "header.h"

/*membuat pohon*/
void makeTree(char *query_name, int query_point, tree *T) {
  // membuat simpul dengan alokasi di memory
  node_t *node;
  node = (node_t *)malloc(sizeof(node_t));

  // memasukan data kedalam kedalam pohon
  strcpy(node->name, query_name);
  node->point = query_point;
  /*inisialisasi*/
  node->right = NULL;
  node->left = NULL;
  // root memegang data paling atas
  (*T).root = node;
}

/*menambah sub pohon bagian kanan*/
void addRight(char *query_name, int query_point, node_t *root) {
  if (root->right == NULL) {
    /*jika sub pohon kanan kosong*/
    // membuat simpul dengan alokasi di memory
    node_t *node;
    node = (node_t *)malloc(sizeof(node_t));
    // memasukan data kedalam pohon
    strcpy(node->name, query_name);
    node->point = query_point;
    /*inisialisasi*/
    node->right = NULL;
    node->left = NULL;
    // tangan kanan memegang sub pohon kanan baru
    root->right = node;
  } else {
    /*jika sub pohon kanan telah terisi*/
    printf("sub pohon kanan telah terisi\n");
  }
}

/*menambah sub pohon bagian kiri*/
void addLeft(char *query_name, int query_point, node_t *root) {
  if (root->left == NULL) {
    /*jika sub pohon kiri kosong*/
    // membuat simpul dengan alokasi di memory
    node_t *node;
    node = (node_t *)malloc(sizeof(node_t));
    // memasukan data kedalam pohon
    strcpy(node->name, query_name);
    node->point = query_point;
    /*inisialisasi*/
    node->right = NULL;
    node->left = NULL;
    // tangan kiri memegang sub pohon kiri baru
    root->left = node;
  } else {
    /*jika sub pohon kiri telah terisi*/
    printf("sub pohon kiri telah terisi\n");
  }
}

/*menghapus sub pohon kanan*/
void delRight(node_t *root) {
  if (root != NULL) {
    node_t *node = root->right;
    if (node != NULL) {
      // jika tangan pohon tidak memegang apapun
      if (node->right == NULL && node->left == NULL) {
        // proses
        root->right = NULL;
        free(node);
      } else {
        // jika salah satu tangan memegang sub pohon
        printf("anaknya kemanain pak?\n");
      }
    }
  }
}

/*menghapus sub pohon kiri*/
void delLeft(node_t *root) {
  if (root != NULL) {
    node_t *node = root->left;
    if (node != NULL) {
      // jika tangan pohon tidak memegang apapun
      if ((node->right == NULL) && (node->left == NULL)) {
        // proses
        root->left = NULL;
        free(node);
      } else {
        // jika salah satu tangan memegang sub pohon
        printf("anaknya kemanain pak?\n");
      }
    }
  }
}

/*print pohon dari bapak, kiri, kanan*/
void printTreePreOrder(node_t *root) {
  if (root != NULL) {
    printf("%s", root->name);
    printf(" - ");
    printTreePreOrder(root->left);
    printTreePreOrder(root->right);
  }
}

/*print pohon dari kiri, bapak, kanan*/
void printTreeInOrder(node_t *root) {
  if (root != NULL) {
    printf("%s", root->name);
    printTreeInOrder(root->left);
    printf(" - ");
    printTreeInOrder(root->right);
  }
}

/*print pohon dari kiri, kanan, bapak*/
void printTreePostOrder(node_t *root) {
  if (root != NULL) {
    printTreePostOrder(root->left);
    printf("%s", root->name);
    printTreePostOrder(root->right);
    printf(" - ");
  }
}

void printArray(int selector, char name[64][32]) {
  int i = 0;
  while (i < selector) {
    printf("%s", name[i]);
    i += 1;
    if (i < selector) printf("-");
  }
}

/**
 * @brief      Traverse the tree and print it's path to the selected
 *element.
 *
 * @param      root      The root element on the tree.
 * @param      to_find   The person name to find on tree.
 * @param      name      The array of name to the selected person.
 * @param[in]  point     The sum of point
 * @param[in]  selector  The selector
 */
void traverseTreePreOrder(node_t *root, int selector, char *to_find,
                          char name[64][32], int point) {
  if (root != NULL) {
    // append current element to the passed array value
    strcpy(name[selector], root->name);
    point += root->point;
    selector += 1;

    if (strcmp(root->name, to_find) == 0) {  // if current element match
      selector -= 1;
      if (selector) {  // loop the array and print the name
        printArray(selector, name);
      } else {  // otherwise print Null.
        printf("Null");
      }
      printf(" %d %d\n", selector + 1, point);  // print level and point.
    } else {
      traverseTreePreOrder(root->left, selector, to_find, name, point);
      traverseTreePreOrder(root->right, selector, to_find, name, point);
    }
  }
}
