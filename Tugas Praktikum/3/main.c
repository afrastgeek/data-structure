/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada tugas masa depan Alpro 2 pada saat mengerjakan Tugas Praktikum 3
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "header.h"

int main(int argc, char const *argv[]) {
  list L;
  createList(&L);

  int num_drugs = 0;
  scanf("%d", &num_drugs);
  inventory_t query[num_drugs];
  for (; num_drugs; num_drugs -= 1) {
    scanf("%d %s %d %d", &query[num_drugs].code, query[num_drugs].name,
          &query[num_drugs].stock, &query[num_drugs].price);
    addLast(query[num_drugs], &L);
  }
  char diagnose[32];
  scanf("%s", diagnose);

  // sortRelationByPriceAsc(&L);
  sortContentByPriceAsc(&L);

  if (isRecommendation(L, diagnose)) {
    recommendationFromHead(L, diagnose);
    printf("======\n");
    recommendationToHead(L, diagnose);
  } else {
    printf("Tidak ada rekomendasi/obat tidak tersedia.\n");
  }

  return 0;
}

/*
Input:
Paracetamol 100 5000
Antihistamin 30 8000
Bioflavonoid 10 13000
Mefenamat 15 10000
Aspirin 40 9000
 */