/*
* Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
* pada tugas masa depan Alpro 2 pada saat mengerjakan Tugas Praktikum 3
* Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
* saya, dan saya bersedia menerima hukumanNya. Aamiin.
*/
#include "header.h"

/**
* @brief      Creates a list from passed list by initializing it's first
*             sibling value (first) and each relation value (next).
*
* @param      L     List given to initialize.
*/
void createList(list *L) {
  (*L).first = (*L).tail = NULL_VALUE;
  int i;
  for (i = 0; i < MAX_VALUE; i += 1) {
    (*L).node[i].prev = (*L).node[i].next = INIT_VALUE;
  }
}

/**
* @brief      Counts the number of element from passed list.
*
* @param[in]  L     List given to count it's element.
*
* @return     Number of element.
*/
int countElement(list L) {
  int result = 0;
  if (L.first != NULL_VALUE) { /* list tidak kosong*/
    int node_nth = L.first;
    while (node_nth != NULL_VALUE) { /* proses */
      result += 1;
      node_nth = L.node[node_nth].next;
    }
  }
  return result;
}

/**
* @brief      Find first empty element from a list.
*
* @param[in]  L     List given to search for.
*
* @return     Element index with empty value.
*/
int emptyElement(list L) {
  int result = NULL_VALUE;
  if (countElement(L) < MAX_VALUE) {
    int not_found = 1;
    int node_nth = 0;
    while (not_found && node_nth < MAX_VALUE) {
      if (L.node[node_nth].next == INIT_VALUE) {
        result = node_nth;
        not_found = 0;
      } else {
        node_nth += 1;
      }
    }
  }
  return result;
}

/**
* @brief      Adds node with content at the start of a list.
*
* @param[in]  content  The content.
* @param      L        The list to add node to.
*/
void addFirst(inventory_t content, list *L) {
  if (countElement(*L) < MAX_VALUE) {
    int node_nth = emptyElement(*L);

    (*L).node[node_nth].content = content;

    if ((*L).first == NULL_VALUE) { /* jika list kosong */
      (*L).node[node_nth].prev = (*L).node[node_nth].next = NULL_VALUE;
      (*L).tail = node_nth;
    } else { /* jika list tidak kosong */
      (*L).node[node_nth].prev = NULL_VALUE;
      (*L).node[node_nth].next = (*L).first;
      (*L).node[(*L).first].prev = node_nth;
    }

    (*L).first = node_nth;
  } else { /* proses jika array penuh */
    printf("sudah tidak dapat ditambah\n");
  }
}

/**
* @brief      Adds node with content to a list after given node.
*
* @param[in]  node_before  The node before target added node.
* @param[in]  content      The content.
* @param      L            The list to add node to.
*/
void addAfter(int node_before, inventory_t content, list *L) {
  if (countElement(*L) < MAX_VALUE) {
    int node_nth = emptyElement(*L);

    (*L).node[node_nth].content = content;

    if ((*L).node[node_before].next != NULL_VALUE) {
      (*L).node[node_nth].prev = node_before;
      (*L).node[node_nth].next = (*L).node[node_before].next;
      (*L).node[node_before].next = node_nth;
      (*L).node[(*L).node[node_nth].next].prev = node_nth;
    } else {  // jika node_nth menjadi elemen terakhir
      (*L).node[node_nth].prev = node_before;
      (*L).node[node_before].next = node_nth;
      (*L).node[node_nth].next = NULL_VALUE;
      (*L).tail = node_nth;
    }
  } else { /* proses jika array penuh */
    printf("sudah tidak dapat ditambah\n");
  }
}

/**
* @brief      Adds node content to the end of a list.
*
* @param[in]  content  The content
* @param      L        The list to add node to.
*/
void addLast(inventory_t content, list *L) {
  if ((*L).first == NULL_VALUE) { /* proses jika list masih kosong */
    int node_nth = 0;

    (*L).node[node_nth].content = content;

    (*L).node[node_nth].prev = (*L).node[node_nth].next = NULL_VALUE;
    (*L).first = (*L).tail = node_nth;
  } else { /* proses jika list telah berisi element */
    if (countElement(*L) < MAX_VALUE) { /* proses jika array belum penuh */
      int node_nth = emptyElement(*L);

      (*L).node[node_nth].content = content;

      (*L).node[node_nth].next = NULL_VALUE;
      (*L).node[(*L).tail].next = node_nth;
      (*L).node[node_nth].prev = (*L).tail;
      (*L).tail = node_nth;
    } else { /* proses jika array penuh */
      printf("sudah tidak dapat ditambah\n");
    }
  }
}

/**
* @brief      Delete node at the start of a list.
*
* @param      L     The list to delete node from.
*/
void delFirst(list *L) {
  if ((*L).first != NULL_VALUE) {
    int node_nth = (*L).first;
    if (countElement(*L) == 1) {
      (*L).first = (*L).tail = NULL_VALUE;
    } else {
      (*L).first = (*L).node[(*L).first].next;
      (*L).node[(*L).first].prev = NULL_VALUE;
    }
    (*L).node[node_nth].prev = (*L).node[node_nth].next = INIT_VALUE;
  } else { /* proses jika list kosong */
    printf("list kosong\n");
  }
}

/**
* @brief      Delete node after given node from a list.
*
* @param[in]  node_before  The node before target deleted element.
* @param      L            The list to delete node from.
*/
void delAfter(int node_before, list *L) {
  int node_nth = (*L).node[node_before].next;
  if (node_nth != NULL_VALUE) {
    if ((*L).node[node_nth].next == NULL_VALUE) {
      (*L).tail = node_before;
      (*L).node[node_before].next = NULL_VALUE;
    } else {
      (*L).node[node_before].next = (*L).node[node_nth].next;
      (*L).node[(*L).node[node_nth].next].prev = node_before;
    }
    (*L).node[node_nth].prev = (*L).node[node_nth].next = INIT_VALUE;
  }
}

/**
* @brief      Delete node at the end of a list.
*
* @param      L     The list to delete node from.
*/
void delLast(list *L) {
  if ((*L).first != NULL_VALUE) {
    if (countElement(*L) == 1) { /* proses jika list hanya berisi satu elemen */
      delFirst(L);
    } else {
      int node_nth = (*L).tail;
      (*L).tail = (*L).node[node_nth].prev;
      (*L).node[(*L).tail].next = NULL_VALUE;
      (*L).node[node_nth].prev = (*L).node[node_nth].next = INIT_VALUE;
    }
  } else { /* proses jika list kosong */
    printf("list kosong\n");
  }
}

/**
* @brief      Print all element in the list.
*
* @param[in]  L     The list to print node from.
*/
void printElement(list L) {
  if (L.first != NULL_VALUE) {
    int node_nth = L.first;
    while (node_nth != NULL_VALUE) {
      printf("%d %s %d %d\n", L.node[node_nth].content.code,
             L.node[node_nth].content.name, L.node[node_nth].content.stock,
             L.node[node_nth].content.price);

      node_nth = L.node[node_nth].next;
    }
  } else { /* proses jika list kosong */
    printf("list kosong\n");
  }
}

/**
* @brief      Print all element in the list but from the end.
*
* @param[in]  L     The list to print node from.
*/
void printToHead(list L) {
  if (L.tail != NULL_VALUE) {
    int node_nth = L.tail;
    while (node_nth != NULL_VALUE) {
      printf("%d %s %d %d\n", L.node[node_nth].content.code,
             L.node[node_nth].content.name, L.node[node_nth].content.stock,
             L.node[node_nth].content.price);

      node_nth = L.node[node_nth].prev;
    }
  } else { /* proses jika list kosong */
    printf("list kosong\n");
  }
}

/**
* @brief      Delete all element from a list.
*
* @param      L     The list to delete node from.
*/
void delAll(list *L) {
  int i = countElement(*L);
  for (; i; i -= 1) delLast(L);
}

/**
 * @brief      [UNFINISHED] sort the node of given list by it's price.
 *
 * @param      L     The list to sort.
 */
// void sortRelationByPriceAsc(list *L) {
//   int node_nth = (*L).first;
//   int node_child = (*L).first;
//   int next_child;
//   while ((*L).node[node_nth].next != NULL_VALUE) {
//     node_child = node_nth;
//     while ((*L).node[node_child].next != NULL_VALUE) {
//       next_child = (*L).node[node_child].next;
//       if ((*L).node[node_child].content.price >
//           (*L).node[next_child].content.price) {
//         (*L).node[node_child].next = (*L).node[next_child].next;
//         (*L).node[next_child].next = (*L).node[next_child].prev;
//         if ((*L).node[node_child].prev != NULL_VALUE) {
//           (*L).node[next_child].prev = (*L).node[node_child].prev;
//         } else {
//           (*L).node[next_child].prev = NULL_VALUE;
//         }
//         (*L).node[node_child].prev = next_child;
//       }
//     }
//   }
// }

/**
 * @brief      sort the content of given list by it's price.
 *
 * @param      L     The list to sort.
 */
void sortContentByPriceAsc(list *L) {
  inventory_t temp;
  int this_node, child_node;

  this_node = (*L).first;
  while (this_node != NULL_VALUE) {
    child_node = (*L).node[this_node].next;
    while (child_node != NULL_VALUE) {
      if ((*L).node[this_node].content.price <
          (*L).node[child_node].content.price) {
        temp = (*L).node[this_node].content;
        (*L).node[this_node].content = (*L).node[child_node].content;
        (*L).node[child_node].content = temp;
      }
      child_node = (*L).node[child_node].next;
    }
    this_node = (*L).node[this_node].next;
  }
}

int isRecommendation(list L, char *diagnose) {
  int recommendation = 0;
  if (L.first != NULL_VALUE) {
    int node_nth = L.first;
    while (node_nth != NULL_VALUE) {
      int drugs_code = L.node[node_nth].content.code;
      if (L.node[node_nth].content.stock) {
        if (!strcmp(diagnose, "nyeri")) {
          if (drugs_code == 1 || drugs_code == 4) {
            recommendation = 1;
          }
        } else if (!strcmp(diagnose, "demam")) {
          if (drugs_code == 1 || drugs_code == 3) {
            recommendation = 1;
          }
        } else if (!strcmp(diagnose, "alergi")) {
          if (drugs_code == 2 || drugs_code == 5) {
            recommendation = 1;
          }
        }
      }
      node_nth = L.node[node_nth].next;
    }
  }

  return recommendation;
}

/**
 * @brief      print list of recommended drugs.
 *
 * @param[in]  L         the list to get drugs from.
 * @param      diagnose  The diagnose as base of the recommendation.
 *
 * @return     the number of recommendation
 */
void recommendationFromHead(list L, char *diagnose) {
  int price_rec[2] = {0};
  if (L.first != NULL_VALUE) {
    int node_nth = L.first;
    while (node_nth != NULL_VALUE) {
      int drugs_code = L.node[node_nth].content.code;
      int drugs_price = L.node[node_nth].content.price;
      if (L.node[node_nth].content.stock) {
        if (!strcmp(diagnose, "nyeri")) {
          if (drugs_code == 1 || drugs_code == 4) {
            printf("%d %s %d\n", drugs_code, L.node[node_nth].content.name,
                   drugs_price);
            if (drugs_code == 1) {
              if (price_rec[0] < drugs_price) {
                price_rec[0] = drugs_price;
              }
            } else if (drugs_code == 4) {
              if (price_rec[1] < drugs_price) {
                price_rec[1] = drugs_price;
              }
            }
          }
        } else if (!strcmp(diagnose, "demam")) {
          if (drugs_code == 1 || drugs_code == 3) {
            printf("%d %s %d\n", drugs_code, L.node[node_nth].content.name,
                   drugs_price);
            if (drugs_code == 1) {
              if (price_rec[0] < drugs_price) {
                price_rec[0] = drugs_price;
              }
            } else if (drugs_code == 3) {
              if (price_rec[1] < drugs_price) {
                price_rec[1] = drugs_price;
              }
            }
          }
        } else if (!strcmp(diagnose, "alergi")) {
          if (drugs_code == 2 || drugs_code == 5) {
            printf("%d %s %d\n", drugs_code, L.node[node_nth].content.name,
                   drugs_price);
            if (drugs_code == 2) {
              if (price_rec[0] < drugs_price) {
                price_rec[0] = drugs_price;
              }
            } else if (drugs_code == 5) {
              if (price_rec[1] < drugs_price) {
                price_rec[1] = drugs_price;
              }
            }
          }
        }
      }
      node_nth = L.node[node_nth].next;
    }
  } else { /* proses jika list kosong */
    printf("list kosong\n");
  }

  printf("%d\n", price_rec[0] + price_rec[1]);
}

/**
 * @brief      print list of recommended drugs
 *
 * @param[in]  L         the list to get drugs from.
 * @param      diagnose  The diagnose as base of the recommendation.
 */
void recommendationToHead(list L, char *diagnose) {
  int price_rec[2];
  price_rec[0] = 65534;
  price_rec[1] = 65534;
  if (L.tail != NULL_VALUE) {
    int node_nth = L.tail;
    while (node_nth != NULL_VALUE) {
      int drugs_code = L.node[node_nth].content.code;
      int drugs_price = L.node[node_nth].content.price;
      if (L.node[node_nth].content.stock) {
        if (!strcmp(diagnose, "nyeri")) {
          if (drugs_code == 1 || drugs_code == 4) {
            printf("%d %s %d\n", drugs_code, L.node[node_nth].content.name,
                   drugs_price);
            if (drugs_code == 1) {
              if (price_rec[0] > drugs_price) {
                price_rec[0] = drugs_price;
              }
            } else if (drugs_code == 4) {
              if (price_rec[1] > drugs_price) {
                price_rec[1] = drugs_price;
              }
            }
          }
        } else if (!strcmp(diagnose, "demam")) {
          if (drugs_code == 1 || drugs_code == 3) {
            printf("%d %s %d\n", drugs_code, L.node[node_nth].content.name,
                   drugs_price);
            if (drugs_code == 1) {
              if (price_rec[0] > drugs_price) {
                price_rec[0] = drugs_price;
              }
            } else if (drugs_code == 3) {
              if (price_rec[1] > drugs_price) {
                price_rec[1] = drugs_price;
              }
            }
          }
        } else if (!strcmp(diagnose, "alergi")) {
          if (drugs_code == 2 || drugs_code == 5) {
            printf("%d %s %d\n", drugs_code, L.node[node_nth].content.name,
                   drugs_price);
            if (drugs_code == 2) {
              if (price_rec[0] > drugs_price) {
                price_rec[0] = drugs_price;
              }
            } else if (drugs_code == 5) {
              if (price_rec[1] > drugs_price) {
                price_rec[1] = drugs_price;
              }
            }
          }
        }
      }
      node_nth = L.node[node_nth].prev;
    }
  } else { /* proses jika list kosong */
    printf("list kosong\n");
  }

  printf("%d\n", price_rec[0] + price_rec[1]);
}
