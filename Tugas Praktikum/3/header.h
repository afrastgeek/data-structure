/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada tugas masa depan Alpro 2 pada saat mengerjakan Tugas Praktikum 3
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include <stdio.h>
#include <string.h>

#ifndef MAX_VALUE
#define MAX_VALUE 16
#endif

#ifndef NULL_VALUE
#define NULL_VALUE -1
#endif

#ifndef INIT_VALUE
#define INIT_VALUE -2
#endif

typedef struct {
  int code;
  char name[50];
  int stock;
  int price;
} inventory_t;

typedef struct {
  inventory_t content;
  int prev;
  int next;
} element_t;

typedef struct {
  int first;
  int tail;
  element_t node[MAX_VALUE];
} list;

void createList(list *L);
int countElement(list L);
int emptyElement(list L);
void addFirst(inventory_t content, list *L);
void addAfter(int node_before, inventory_t content, list *L);
void addLast(inventory_t content, list *L);
void delFirst(list *L);
void delAfter(int node_before, list *L);
void delLast(list *L);
void printElement(list L);
void printToHead(list L);
void delAll(list *L);

// void sortRelationByPriceAsc(list *L);
void sortContentByPriceAsc(list *L);

int isRecommendation(list L, char *diagnose);
void recommendationFromHead(list L, char *diagnose);
void recommendationToHead(list L, char *diagnose);
void printRecommendation(list L, char *diagnose);