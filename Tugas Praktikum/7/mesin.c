/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Tugas Praktikum 7
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "header.h"

/**
 * @brief      Creates an empty Queue.
 *
 * @param      Q     The Queue
 */
void createEmpty(queue_t *Q) {
  (*Q).first = NULL;
  (*Q).last = NULL;
}

/**
 * @brief      Determines if given queue is empty.
 *
 * @param[in]  Q     The queue
 *
 * @return     True if empty, False otherwise.
 */
int isEmpty(queue_t Q) {
  int hasil = 0;
  if (Q.first == NULL) {
    hasil = 1;
  }
  return hasil;
}

/**
 * @brief      Counts the number of element in queue.
 *
 * @param[in]  Q     The queue
 *
 * @return     Number of element.
 */
int countElement(queue_t Q) {
  int hasil = 0;
  if (Q.first != NULL) {
    node_t *elmt;
    elmt = Q.first;

    while (elmt != NULL) {
      hasil = hasil + 1;
      elmt = elmt->next;
    }
  }
  return hasil;
}

/**
 * @brief      Add a query item to the queue
 *
 * @param[in]  query  The query
 * @param      Q      The queue
 */
void add(content_t query, queue_t *Q) {
  node_t *elmt;
  elmt = (node_t *)malloc(sizeof(node_t));
  elmt->content = query;
  elmt->next = NULL;
  if ((*Q).first == NULL) {
    (*Q).first = elmt;
  } else {
    (*Q).last->next = elmt;
  }
  (*Q).last = elmt;
  elmt = NULL;
}

/**
 * @brief      Adds a query item to queue with given priority.
 *
 * @param[in]  priority  The priority
 * @param[in]  query     The query
 * @param      Q         The queue
 */
void addPriority(int priority, content_t query, queue_t *Q) {
  node_t *elmt;
  elmt = (node_t *)malloc(sizeof(node_t));
  elmt->content = query;

  if (isEmpty(*Q) == 1) {  // if the queue is empty
    (*Q).first = elmt;
    (*Q).last = elmt;
    elmt->next = NULL;
  } else {
    if (priority == 0 || priority > countElement(*Q)) {  // normal add
      (*Q).last->next = elmt;
      (*Q).last = elmt;
      elmt->next = NULL;
    } else if (priority == 1) {  // highest priority
      elmt->next = (*Q).first;
      (*Q).first = elmt;
    } else {  // same as priority number
      int i = 1;
      node_t *prev = (*Q).first;

      while (elmt != NULL && i < priority - 1) {
        prev = prev->next;
        i++;
      }
      elmt->next = prev->next;
      prev->next = elmt;
    }
  }
  elmt = NULL;
}

/**
 * @brief      Delete a query item from queue.
 *
 * @param      Q     The queue
 */
void del(queue_t *Q) {
  if ((*Q).first != NULL) {
    node_t *elmt = (*Q).first;
    if (countElement(*Q) == 1) {
      (*Q).first = NULL;
    } else {
      (*Q).first = (*Q).first->next;
      elmt->next = NULL;
    }
    free(elmt);
  }
}

/**
 * @brief      Print the Queue with given limit. Only print item from the start
 *             of queue till the limit number.
 *
 * @param[in]  Q     The queue
 * @param[in]  limit  The limit
 */
void printQueue(queue_t Q) {
  if (Q.first != NULL) {
    node_t *elmt = Q.first;
    int i = 1;

    while (elmt != NULL) {
      printf("%s %d\n", elmt->content.name, elmt->content.score);
      elmt = elmt->next;
      i++;
    }
  } else {
    printf("Kosong!\n");
  }
}

/**
 * @brief      Gets applicant with matching criteria and print result to the
 *             stdout.
 *
 * @param[in]  Q     The Queue
 */
void getSelected(queue_t Q) {
  queue_t Q_1st, Q_2nd, Q_left;
  createEmpty(&Q_1st);
  createEmpty(&Q_2nd);
  createEmpty(&Q_left);

  int counter_1st = 0;
  int counter_2nd = 0;

  node_t *this_applicant = Q.first;
  this_applicant = Q.first;
  while (this_applicant != NULL) {
    if (this_applicant->content.score >= 650 && counter_1st < 5) {
      add(this_applicant->content, &Q_1st);
      counter_1st += 1;
    } else if (this_applicant->content.score >= 500 && counter_2nd < 5) {
      add(this_applicant->content, &Q_2nd);
      counter_2nd += 1;
    } else if (this_applicant->content.score >= 500) {
      add(this_applicant->content, &Q_left);
    }
    this_applicant = this_applicant->next;
  }

  printf("===Antrian Wawancara Golongan 1===\n");
  printQueue(Q_1st);
  printf("===Antrian Wawancara Golongan 2===\n");
  printQueue(Q_2nd);
  printf("===Sisa Pendaftar===\n");
  printQueue(Q_left);
}
