/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Tugas Praktikum 7
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "header.h"

int main(int argc, char const *argv[]) {
  int num_applicant;
  scanf("%d", &num_applicant);

  queue_t Q;
  createEmpty(&Q);

  int i;
  content_t applicant;
  int priority;
  for (i = 0; i < num_applicant; i += 1) {
    scanf("%s %d %d", applicant.name, &applicant.score, &priority);
    addPriority(priority, applicant, &Q);
  }

  getSelected(Q);

  return 0;
}
