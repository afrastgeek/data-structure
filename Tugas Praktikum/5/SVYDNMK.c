/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur pada saat mengerjakan UTS Struktur Data. Jika saya
 * melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 * menerima hukumanNya. Aamiin.
 */
#include "SVYDNMK.h"

void createList(list *L) { (*L).first = NULL; }

int countElementRow(list L) {
  int hasil = 0;

  if (L.first != NULL) {
    /* list tidak kosong */
    row_element_t *elmt;

    /* iniialisasi */
    elmt = L.first;

    while (elmt != NULL) {
      /* proses */
      hasil = hasil + 1;

      /* iterasi */
      elmt = elmt->next;
    }
  }
  return hasil;
}

int countElementColumn(row_element_t L) {
  int hasil = 0;

  if (L.col != NULL) {
    /* list tidak kosong */
    column_element_t *elmt;

    /* iniialisasi */
    elmt = L.col;

    while (elmt != NULL) {
      /* proses */
      hasil = hasil + 1;

      /* iterasi */
      elmt = elmt->next;
    }
  }
  return hasil;
}

void addFirstRow(char *name, list *L) {
  row_element_t *elmt;
  elmt = (row_element_t *)malloc(sizeof(row_element_t));
  strcpy(elmt->head_content.name, name);
  elmt->col = NULL;
  if ((*L).first == NULL) {
    elmt->next = NULL;
  } else {
    elmt->next = (*L).first;
  }
  (*L).first = elmt;
  elmt = NULL;
}

void addFirstColumn(char *name, int year, int ucu, row_element_t *L) {
  column_element_t *elmt;
  elmt = (column_element_t *)malloc(sizeof(column_element_t));

  strcpy(elmt->body_content.name, name);
  elmt->body_content.year = year;
  elmt->body_content.ucu = ucu;

  /*selanjutnya belum dicek*/
  if ((*L).col == NULL) {
    elmt->next = NULL;
  } else {
    elmt->next = (*L).col;
  }
  (*L).col = elmt;
  elmt = NULL;
}

void addAfterRow(row_element_t *prev, char *name) {
  row_element_t *elmt;
  elmt = (row_element_t *)malloc(sizeof(row_element_t));
  strcpy(elmt->head_content.name, name);
  elmt->col = NULL;
  if (prev->next == NULL) {
    elmt->next = NULL;
  } else {
    elmt->next = prev->next;
  }
  prev->next = elmt;
  elmt = NULL;
}

void addAfterColumn(column_element_t *prev, char *name, int year, int ucu) {
  column_element_t *elmt;
  elmt = (column_element_t *)malloc(sizeof(column_element_t));
  strcpy(elmt->body_content.name, name);
  elmt->body_content.year = year;
  elmt->body_content.ucu = ucu;

  if (prev->next == NULL) {
    elmt->next = NULL;
  } else {
    elmt->next = prev->next;
  }
  prev->next = elmt;
  elmt = NULL;
}

void addLastRow(char *name, list *L) {
  if ((*L).first == NULL) {
    /* jika list adalah list kosong */
    addFirstRow(name, L);
  } else {
    /* jika list tidak kosong */
    row_element_t *elmt;
    elmt = (row_element_t *)malloc(sizeof(row_element_t));
    strcpy(elmt->head_content.name, name);
    elmt->next = NULL;
    elmt->col = NULL;
    /* mencari elemen terakhir list */
    row_element_t *last = (*L).first;
    while (last->next != NULL) {
      /* iterasi */
      last = last->next;
    }
    last->next = elmt;
    elmt = NULL;
  }
}

void addLastColumn(char *name, int year, int ucu, row_element_t *L) {
  if ((*L).col == NULL) {
    /* jika list adalah list kosong */
    addFirstColumn(name, year, ucu, L);
  } else {
    /* jika list tidak kosong */
    column_element_t *elmt;
    elmt = (column_element_t *)malloc(sizeof(column_element_t));
    strcpy(elmt->body_content.name, name);
    elmt->body_content.year = year;
    elmt->body_content.ucu = ucu;
    elmt->next = NULL;
    /* mencari elemen erakhir list */
    column_element_t *last = (*L).col;
    while (last->next != NULL) {
      /* iterasi */
      last = last->next;
    }
    last->next = elmt;
    elmt = NULL;
  }
}

/* TAMBAHIN:
 HARUS NGECEK
 anaknya ada atau engga.

  kasih tau masih ada anak, atau delAll anak;
 */

void delFirstRow(list *L) {
  if ((*L).first != NULL) {
    /* jika list bukan list kosong */
    row_element_t *elmt = (*L).first;
    if (countElementRow(*L) == 1) {
      (*L).first = NULL;
    } else {
      (*L).first = (*L).first->next;
      elmt->next = NULL;
    }
    free(elmt);
  }
}

void delFirstColumn(row_element_t *L) {
  if ((*L).col != NULL) {
    /* jika list bukan list kosong */
    column_element_t *elmt = (*L).col;
    if (countElementColumn(*L) == 1) {
      (*L).col = NULL;
    } else {
      (*L).col = (*L).col->next;
      elmt->next = NULL;
    }
    free(elmt);
  }
}

void delAfterRow(row_element_t *prev) {
  row_element_t *elmt = prev->next;
  if (elmt->next == NULL) {
    prev->next = NULL;
  } else {
    prev->next = elmt->next;
    elmt->next = NULL;
  }
  free(elmt);
}

void delAfterColumn(column_element_t *prev) {
  column_element_t *elmt = prev->next;
  if (elmt->next == NULL) {
    prev->next = NULL;
  } else {
    prev->next = elmt->next;
    elmt->next = NULL;
  }
  free(elmt);
}

void delLastRow(list *L) {
  if ((*L).first != NULL) {
    /* jika list tidak kosong */
    if (countElementRow(*L) == 1) {
      /* list terdiri dari satu elemen */
      delFirstRow(L);
    } else {
      /* mencari elemen terakhir list */
      row_element_t *last = (*L).first;
      row_element_t *before_last;
      while (last->next != NULL) {
        /* iterasi */
        before_last = last;
        last = last->next;
      }
      before_last->next = NULL;
      free(last);
    }
  }
}

void delLastColumn(row_element_t *L) {
  if ((*L).col != NULL) {
    /* jika list tidak kosong */
    if (countElementColumn(*L) == 1) {
      /* list terdiri dari satu elemen */
      delFirstColumn(L);
    } else {
      /* mencari elemen terakhir list */
      column_element_t *last = (*L).col;
      column_element_t *before_last;
      while (last->next != NULL) {
        /* iterasi */
        before_last = last;
        last = last->next;
      }
      before_last->next = NULL;
      free(last);
    }
  }
}

void printElement(list L) {
  if (L.first != NULL) {
    /* jika list tidak kosong */
    /* ini sialisasi */
    row_element_t *elmt = L.first;

    while (elmt != NULL) {
      /* proses */
      printf("%s\n", elmt->head_content.name);

      column_element_t *eCol = elmt->col;
      while (eCol != NULL) {
        printf("- %s %d\n", eCol->body_content.name, eCol->body_content.ucu);
        eCol = eCol->next;
      }

      /* iterasi */
      elmt = elmt->next;
    }
  } else {
    /* proses jika list kosong */
    printf("list kosong\n");
  }
}

void delAllRow(list *L) {
  if (countElementRow(*L) != 0) {
    int i;

    for (i = countElementRow(*L); i >= 1; i--) {
      /* proses menghapus elemen list */
      delLastRow(L);
    }
  }
}

void delAllColumn(row_element_t *L) {
  if (countElementColumn(*L) != 0) {
    int i;

    for (i = countElementColumn(*L); i >= 1; i--) {
      delLastColumn(L);
    }
  }
}

int checkRow(char *name, list L) {
  int num_next = 0;
  if (L.first != NULL) { /* if list has only one row element */
    row_element_t *elmt = L.first;
    while (elmt != NULL && strcmp(elmt->head_content.name, name) == -1) {
      elmt = elmt->next;
      num_next += 1;
    }
  }
  return num_next;
}

void addColleger(column_content_t query, list *L) {
  char year[5];
  sprintf(year, "%d", query.year);

  if ((*L).first == NULL) {
    addFirstRow(year, L);
    addFirstColumn(query.name, query.year, query.ucu, (*L).first);
  } else {
    row_element_t *elmt_row = (*L).first;
    int num_next = checkRow(year, *L);
    for (; num_next > 1; num_next -= 1) {
      elmt_row = elmt_row->next;
    }
    if (strcmp(elmt_row->head_content.name, year) == 1) {
      addFirstRow(year, L);
      addLastColumn(query.name, query.year, query.ucu, (*L).first);
    } else {
      if (!checkRow) addAfterRow(elmt_row, year);
      if (strcmp(elmt_row->head_content.name, year) == -1) {
        addLastColumn(query.name, query.year, query.ucu, elmt_row->next);
      } else {
        addLastColumn(query.name, query.year, query.ucu, elmt_row);
      }
    }
  }
}

void dropColleger(char *name, list *L) {
  if ((*L).first != NULL) { /* jika list tidak kosong */
    row_element_t *elmt = (*L).first;
    // row_element_t *lastRow = (*L).first;

    // while (lastRow != NULL) {
    //   lastRow = lastRow->next;
    // }

    while (elmt != NULL) {
      column_element_t *eCol = elmt->col;
      while (eCol != NULL) {
        if (!strcmp(eCol->body_content.name, name)) {
          // addLastColumn(eCol->body_content.name, eCol->body_content.year, eCol->body_content.ucu, lastRow);
          if (eCol == elmt->col) {
            // delFirstColumn(elmt);
          } else {
            // column_element_t *delCol = elmt->col;
            // while (delCol != eCol) {
            //   delCol = delCol->next;
            // }
            // delAfterColumn(delCol);
          }
        }
        eCol = eCol->next;
      }

      /* iterasi */
      elmt = elmt->next;
    }
  } else {
    /* proses jika list kosong */
    printf("list kosong\n");
  }
}
