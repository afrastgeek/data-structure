/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur pada saat mengerjakan UTS Struktur Data. Jika saya
 * melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 * menerima hukumanNya. Aamiin.
 */
#include "SVYDNMK.h"

int main(int argc, char const *argv[]) {
  list L;
  createList(&L);

  int num_colleger;
  scanf("%d", &num_colleger);
  column_content_t query;
  for (; num_colleger; num_colleger -= 1) {
    scanf(" %s %d %d", query.name, &query.year, &query.ucu);
    addColleger(query, &L);
  }

  addLastRow("undur diri", &L);
  scanf("%d", &num_colleger);
  char name[32];
  for (; num_colleger; num_colleger -= 1) {
    scanf(" %s", name);
    // dropColleger(name, &L);
  }

  printElement(L);

  return 0;
}

/*
(lolank16) Mahasiswa Angkatan


Pembuat Soal: Rosa A. S.

Batas Waktu Eksekusi  5 Detik
Batas Memori  1 MB



Kompetensi yang dievaluasi: Kemampuan mengakses dan mengoperasikan list of list.
- jangan lupa ketik janji setia pada kejujuran
- setiap suasana tidak kondusif akan mendapat teguran, dan setiap teguran ketiga
akan dikenakan pemotongan waktu 10 menit bagi semua peserta evaluasi.

Diberikan n buah data mahasiswa yang berisi nama, angkatan, jumlah sks.
Kelompokkan setiap mahasiswa berdasarkan angkatannya. Kemudian akan diberikan m
buah mahasiswa yang mengundurkan diri, kelompokkan pula mahasiswa yang
mengundurkan diri ini pada kelompok undur diri. Memasukkan angkatan ke dalam
list baris/besar urut angkatan dari yang kecil ke besar. Begitu pula saat
memasukkan ke dalam list kolom/kecil pada undur diri urut angkatan dari terkecil
ke besar.

Format Masukan:

n, 0 < n < 50, banyaknya mahasiswa.
n baris nama(string) angkatan(string) jumlah sks(int).
m, 0 < n < 40, banyaknya mahasiswa yang mengundurkan diri.
m baris nama mahasiswa yang mengundurkan diri.

Format Keluaran:

isi list of list setelah proses.

Contoh Masukan

5
ani 2016 0
ana 2015 11
ati 2014 32
tina 2015 44
yena 2014 2
3
yena
ani
ana


Contoh Keluaran

2014
- ati 32
2015
- tina 44
undur diri
- yena 2014 2
- ana 2015 11
- ani 2016 0

 */
