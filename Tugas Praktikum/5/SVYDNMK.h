/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur pada saat mengerjakan UTS Struktur Data. Jika saya
 * melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya bersedia
 * menerima hukumanNya. Aamiin.
 */
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

typedef struct {
  char name[32];
} row_content_t;

typedef struct {
  char name[32];
  int year;
  int ucu; /* University Credit Unit */
} column_content_t;

typedef struct column_element_s *column_address_t;
typedef struct column_element_s {
  column_content_t body_content;
  column_address_t next;
} column_element_t;

typedef struct row_element_s *row_address_t;
typedef struct row_element_s {
  row_content_t head_content;
  column_element_t *col;
  row_address_t next;
} row_element_t;

typedef struct { row_element_t *first; } list;

void createList(list *L);
int countElementRow(list L);
int countElementK(row_element_t L);
void addFirstRow(char *name, list *L);
void addFirstK(char *name, int year, int ucu, row_element_t *L);
void addAfterRow(row_element_t *prev, char *name);
void addAfterK(column_element_t *prev, char *name, int year, int ucu);
void addLastRow(char *name, list *L);
void addLastK(char *name, int year, int ucu, row_element_t *L);
void delFirstRow(list *L);
void delFirstK(row_element_t *L);
void delAfterRow(row_element_t *prev);
void delAfterK(column_element_t *prev);
void delLastRow(list *L);
void delLastK(row_element_t *L);
void printElement(list L);
void delAllRow(list *L);
void delAllK(row_element_t *L);

void addColleger(column_content_t query, list *L);
void dropColleger(char *name, list *L);
