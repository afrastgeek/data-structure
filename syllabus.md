# <a id="syllabus" href="#syllabus">Syllabus</a>
Data structure class (IK410) syllabus.


## On this page

1. [Overview](#overview)
1. [Scoring](#scoring)
1. [Materials](#materials)
1. [FLOW](#flow)
1. [What We Do](#what-we-do)
1. [See also](#see-also)

## <a id="overview" href="#overview">Overview</a>

3 sks
Syarat: alpro 1 & 2

Metode:
- ceramah
- diskusi
- tanya jawab
- praktikum

Tugas individu

## <a id="scoring" href="#scoring">Scoring</a>

20% TMD
20% Kuis
20% UTS
20% UAS
20% Praktikum

## <a id="materials" href="#materials">Materials</a>

1. Pendahuluan Dan Kosep List () Tunggal
2. Implementasi List Tunggal Secara Statis
3. Implementasi List Tunggal Secara Dinamis
4. Implementasi List Ganda Secara Statis
5. Implementasi List Ganda Secara Dinamis
6. Kuis 1
7. Konsep List Of List
8. UTS
9. Konsep Stack
10. Queue
11. Kuis 2
12. Tree Biner
13. Tree
14. Tree N...
15. Graf
16. UAS

Bisa menggunakan apapun buku (untuk referensi)

## <a id="flow" href="#flow">FLOW</a>

bentuk menghargai proses pada strukdat

penilaian diberikan melalui :
- 5 evaluasi di kelas (rata-rata 1-2 soal)
- 1 tugas masa depan
- 10 tugas praktikum (masing-masing satu soal)
- (nilai implisit)

maka total jendral diberikan 16 kali kesempatan sepanjang 1 semester untuk mengumpulkan nilai. 16 kali kesempatan adalah sebuah proses

> "you gotta learn to talk, jeffrey. it's part of the aging process".

## <a id="what-we-do" href="#what-we-do">What We Do</a>

di kelas:
- di kelas membahas konsep dan mesin

di rumah:
- memastikan mesin jalan
- latihan soal (cspc)
- mengerjakan tp

praktikum:
- mengerjakan latihan soal dengan mesin.

> strukdat fokus pada **aturan main** dari konsep konsep yang termasuk dalam struktur data.
