/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur pada saat mengerjakan Ujian Akhir Semester
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef struct node_s *node;
typedef struct node_s {
  char name[8];
  int number[2];
  node right;
  node left;
} node_t;

typedef struct { node_t *root; } tree;

void makeTree(char *query_name, int *query_number, tree *T);
void addRight(char *query_name, int *query_number, node_t *root);
void addLeft(char *query_name, int *query_number, node_t *root);
void delRight(node_t *root);
void delLeft(node_t *root);
void printTreePreOrder(node_t *root);
void printTreeInOrder(node_t *root);
void printTreePostOrder(node_t *root);
void copyTree(node_t *root1, node_t **root2);
node_t *findNode(char *query_name, node_t *root);
int checkLeafNumber(node_t *node);
int getLeafCount(node_t *node);
