/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur pada saat mengerjakan Ujian Akhir Semester
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "pbto16.h"

/*membuat pohon*/
void makeTree(char *query_name, int *query_number, tree *T) {
  // membuat simpul dengan alokasi di memory
  node_t *node;
  node = (node_t *)malloc(sizeof(node_t));

  // memasukan data kedalam kedalam pohon
  strcpy(node->name, query_name);
  node->number[0] = query_number[0];
  node->number[1] = query_number[1];
  /*inisialisasi*/
  node->right = NULL;
  node->left = NULL;
  // root memegang data paling atas
  (*T).root = node;
}

/*menambah sub pohon bagian kanan*/
void addRight(char *query_name, int *query_number, node_t *root) {
  if (root->right == NULL) {
    /*jika sub pohon kanan kosong*/
    // membuat simpul dengan alokasi di memory
    node_t *node;
    node = (node_t *)malloc(sizeof(node_t));
    // memasukan data kedalam pohon
    strcpy(node->name, query_name);
    node->number[0] = query_number[0];
    node->number[1] = query_number[1];
    /*inisialisasi*/
    node->right = NULL;
    node->left = NULL;
    // tangan kanan memegang sub pohon kanan baru
    root->right = node;
  } else {
    /*jika sub pohon kanan telah terisi*/
    printf("sub pohon kanan telah terisi\n");
  }
}

/*menambah sub pohon bagian kiri*/
void addLeft(char *query_name, int *query_number, node_t *root) {
  if (root->left == NULL) {
    /*jika sub pohon kiri kosong*/
    // membuat simpul dengan alokasi di memory
    node_t *node;
    node = (node_t *)malloc(sizeof(node_t));
    // memasukan data kedalam pohon
    strcpy(node->name, query_name);
    node->number[0] = query_number[0];
    node->number[1] = query_number[1];
    /*inisialisasi*/
    node->right = NULL;
    node->left = NULL;
    // tangan kiri memegang sub pohon kiri baru
    root->left = node;
  } else {
    /*jika sub pohon kiri telah terisi*/
    printf("sub pohon kiri telah terisi\n");
  }
}

/*menghapus sub pohon kanan*/
void delRight(node_t *root) {
  if (root != NULL) {
    node_t *node = root->right;
    if (node != NULL) {
      // jika tangan pohon tidak memegang apapun
      if (node->right == NULL && node->left == NULL) {
        // proses
        root->right = NULL;
        free(node);
      } else {
        // jika salah satu tangan memegang sub pohon
        printf("anaknya kemanain pak?\n");
      }
    }
  }
}

/*menghapus sub pohon kiri*/
void delLeft(node_t *root) {
  if (root != NULL) {
    node_t *node = root->left;
    if (node != NULL) {
      // jika tangan pohon tidak memegang apapun
      if ((node->right == NULL) && (node->left == NULL)) {
        // proses
        root->left = NULL;
        free(node);
      } else {
        // jika salah satu tangan memegang sub pohon
        printf("anaknya kemanain pak?\n");
      }
    }
  }
}

/*print pohon dari bapak, kiri, kanan*/
void printTreePreOrder(node_t *root) {
  if (root != NULL) {
    printf("%s", root->name);
    printf(" - ");
    printTreePreOrder(root->left);
    printTreePreOrder(root->right);
  }
}

/*print pohon dari kiri, bapak, kanan*/
void printTreeInOrder(node_t *root) {
  if (root != NULL) {
    printf("%s", root->name);
    printTreeInOrder(root->left);
    printf(" - ");
    printTreeInOrder(root->right);
  }
}

/*print pohon dari kiri, kanan, bapak*/
void printTreePostOrder(node_t *root) {
  if (root != NULL) {
    printTreePostOrder(root->left);
    printf("%s", root->name);
    printTreePostOrder(root->right);
    printf(" - ");
  }
}

void printArray(int selector, char name[64][32]) {
  int i = 0;
  while (i < selector) {
    printf("%s", name[i]);
    i += 1;
    if (i < selector) printf("-");
  }
}

/**
 * @brief      Find node in the Tree
 *
 * @param      query_name  The query name in node to find.
 * @param      root        The root element from tree.
 *
 * @return     return pointer to node with matched name.
 */
node_t *findNode(char *query_name, node_t *root) {
  if (root != NULL) {  // only run if node in tree is exist
    if (!strcmp(root->name, query_name)) return root;

    node_t *next_child;
    next_child = findNode(query_name, root->left);
    if (next_child) return next_child;

    next_child = findNode(query_name, root->right);
    if (next_child) return next_child;
  }
  // return NULL;
}

/**
 * @brief      Counts the number of leaf.
 *
 * @param      node  The node
 *
 * @return     Number of leaf.
 */
int getLeafCount(node_t *node) {
  if (node == NULL) return 0;
  if (node->left == NULL && node->right == NULL) {
    return 1;
  } else {
    return getLeafCount(node->left) + getLeafCount(node->right);
  }
}

/**
 * @brief      Traverse the tree and check if number in leaf is multiplication
 *             of the other number.
 *
 * @param      node  The node.
 *
 * @return     return TRUE (1) if all number in leaf node is multiplication of
 *             the other number.
 */
int checkLeafNumber(node_t *node) {
  if (node == NULL) return 0;
  if (node->left == NULL && node->right == NULL) {
    if (node->number[0] % node->number[1] == 0 ||
        node->number[1] % node->number[0] == 0) {
      return 1;
    } else {
      return 0;
    }
  } else {
    return checkLeafNumber(node->left) + checkLeafNumber(node->right);
  }
}
