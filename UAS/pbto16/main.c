/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur pada saat mengerjakan Ujian Akhir Semester
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "pbto16.h"

int main(int argc, char const *argv[]) {
  tree T;            // declare an empty Tree
  node_t *n_parent;  // an empty struct to store parent element

  // query variables
  int num_query;
  char q_parent[16];
  char q_name[16];
  int q_number[2];
  char q_position[2];

  scanf("%d", &num_query);
  while (num_query) {  // loop in number of num_query
    scanf("%s %s %d %d %s", q_parent, q_name, &q_number[0], &q_number[1],
          q_position);                // input sequence
    if (!strcmp(q_parent, "null")) {  // if first element, make the tree
      makeTree(q_name, q_number, &T);
    } else {  // add child based on position query.
      n_parent = findNode(q_parent, T.root);  // get the parent node.
      if (!strcmp(q_position, "kanan")) {
        addRight(q_name, q_number, n_parent);
      } else if (!strcmp(q_position, "kiri")) {
        addLeft(q_name, q_number, n_parent);
      }
    }
    num_query -= 1;
  }

  // if all leaf num is multiplication of the other
  if (getLeafCount(T.root) == checkLeafNumber(T.root)) {
    printf("|om telolet om|\n");
  } else {
    printf("|bukan telolet om|\n");
  }

  return 0;
}
