/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur pada saat mengerjakan Ujian Akhir Semester
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include "grfjlr14.h"

int main(int argc, char const *argv[]) {
  graph G;
  createEmpty(&G); // create an empty graph

  int num_query;
  scanf("%d", &num_query); // query the number of node

  struct {
    char name;
    int num;
    char node[16];
  } path[num_query]; // create struct to store node's neighbor

  int i, j;
  for (i = 0; i < num_query; i += 1) {
    scanf(" %c %d", &path[i].name, &path[i].num);
    addSimpul(path[i].name, &G); // create node based on input
    for (j = 0; j < path[i].num; j += 1) {
      scanf(" %c", &path[i].node[j]); // store node's neighbor to array
    }
  }

  char q_start, q_end;
  scanf(" %c %c", &q_start, &q_end); // wanted start of path and end of path.

  simpul *start, *neighbor;
  for (i = 0; i < num_query; i += 1) {
    start = findSimpul(path[i].name, G); // get the start of path node
    for (j = 0; j < path[i].num; j += 1) {
      neighbor = findSimpul(path[i].node[j], G); // get the neighbor node
      addJalur(start, neighbor); // add path between
    }
  }

  printGraph(G);

  return 0;
}
