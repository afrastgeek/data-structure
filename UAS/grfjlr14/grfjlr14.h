/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur pada saat mengerjakan Ujian Akhir Semester
 * Struktur Data. Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi
 * saya, dan saya bersedia menerima hukumanNya. Aamiin.
 */
#include <stdio.h>
#include <malloc.h>

typedef struct smp *alamatsimpul;
typedef struct jlr *alamatjalur;

typedef struct smp {
  char info;
  alamatsimpul next;
  alamatjalur arc;
} simpul;

typedef struct jlr {
  alamatjalur next;
  simpul *tujuan;
} jalur;

typedef struct { simpul *first; } graph;

void createEmpty(graph *G);
void addSimpul(char c, graph *G);
void addJalur(simpul *akhir, simpul *awal);
void delJalur(char ctujuan, simpul *awal);
void delSimpul(char c, graph *G);
simpul *findSimpul(char c, graph G);
void printGraph(graph G);
