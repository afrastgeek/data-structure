/* Saya M Ammar Fadhlur Rahman mengerjakan evaluasi Tugas Masa Depan dalam mata
 * kuliah Struktur Data untuk keberkahanNya maka saya tidak melakukan
 * kecurangan seperti yang telah dispesifikasikan. Aamiin.
 */

#include "tmds16.h"

int main(int argc, char const *arv[]) {
  SBBSTree T;

  int num_person;
  scanf("%d", &num_person);

  char query[64];
  char parent[16];
  char numeric[4];
  int i, j;

  data_t data;

  for (i = 0; i < num_person; ++i) {
    scanf("%s", query);
    // sscanf(query[i], "%15[^#]#%15[^#]#%3[^#]#%s", parent, child, num_good,
    // num_bad);
    strcpy(parent, strtok(query, "#"));
    strcpy(data.name, strtok(NULL, "#"));
    strcpy(numeric, strtok(NULL, "#"));
    data.n_bad = atoi(numeric);
    strcpy(numeric, strtok(NULL, "#"));
    data.n_good = atoi(numeric);

    for (j = 0; j < data.n_bad; ++j) {
      scanf("%s", data.bad[j]);
    }
    for (j = 0; j < data.n_good; ++j) {
      scanf("%s", data.good[j]);
    }

    if (i == 0) {
      makeTree(&T, data);
    } else {
      SBBSTreeNode *node = findSimpul(T.root, parent);
      addChild(node, data);
    }
  }

  int num_tobat;
  scanf("%d", &num_tobat);
  char tobat[num_tobat][32];
  for (i = 0; i < num_tobat; ++i) {
    scanf("%s", tobat[i]);
  }
  int num_luck;
  scanf("%d", &num_luck);
  char luck[num_luck][32];
  for (i = 0; i < num_luck; ++i) {
    scanf("%s", luck[i]);
  }

  printTreePreOrder(T.root);

  printTreePreOrder(T.root);

  return 0;
}
