/* Saya M Ammar Fadhlur Rahman mengerjakan evaluasi Tugas Masa Depan dalam mata
 * kuliah Struktur Data untuk keberkahanNya maka saya tidak melakukan
 * kecurangan seperti yang telah dispesifikasikan. Aamiin.
 */

#include "stdio.h"
#include "string.h"
#include "stdlib.h"

#ifndef _SBBSTree_h
#define _SBBSTree_h

typedef struct {
  char name[32];
  char good[32][32];
  char bad[32][32];
  int n_good;
  int n_bad;
} data_t;

typedef struct SBBSTreeNode {
  data_t data;
  struct SBBSTreeNode *sibling;
  struct SBBSTreeNode *child;
} SBBSTreeNode;

typedef struct { SBBSTreeNode *root; } SBBSTree;

int compareData(data_t data1, data_t data2);

void makeTree(SBBSTree *T, data_t data);
void addChild(SBBSTreeNode *parent, data_t data);
void delChild(SBBSTreeNode *parent, data_t data);
SBBSTreeNode *findSimpul(SBBSTreeNode *parent, char *name);

void calcSpacePreOrder(SBBSTreeNode *parent, int level);
void printTreePreOrderIndented(SBBSTreeNode *parent, int spaces);
void printTreePreOrder(SBBSTreeNode *parent);
void printTreePostOrder(SBBSTreeNode *parent);
int spaces[32];  // storing spaces for 32 level max.

void copyTree(SBBSTreeNode *parent1, SBBSTreeNode **parent2);
int isEqual(SBBSTreeNode *parent1, SBBSTreeNode *parent2);

void replaceData(SBBSTreeNode *parent, int num_tobat, char tobat[num_tobat][32],
                 int num_luck, char luck[num_luck][32]);

#endif
