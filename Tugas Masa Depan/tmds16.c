/* Saya M Ammar Fadhlur Rahman mengerjakan evaluasi Tugas Masa Depan dalam mata
 * kuliah Struktur Data untuk keberkahanNya maka saya tidak melakukan
 * kecurangan seperti yang telah dispesifikasikan. Aamiin.
 */

#include "tmds16.h"

void makeTree(SBBSTree *T, data_t data) {
  SBBSTreeNode *node = (SBBSTreeNode *)malloc(sizeof(SBBSTreeNode));
  node->data = data;
  node->sibling = node->child = NULL;
  (*T).root = node;
}

void addChild(SBBSTreeNode *parent, data_t data) {
  if (parent != NULL) {
    SBBSTreeNode *node = (SBBSTreeNode *)malloc(sizeof(SBBSTreeNode));
    node->data = data;
    node->child = NULL;
    if (parent->child == NULL) {
      node->sibling = NULL;
      parent->child = node;
    } else {
      if (parent->child->sibling == NULL) {
        node->sibling = parent->child;
        parent->child->sibling = node;
      } else {
        SBBSTreeNode *last = parent->child;
        while (last->sibling != parent->child) {
          last = last->sibling;
        }
        node->sibling = parent->child;
        last->sibling = node;
      }
    }
  }
}

void delChild(SBBSTreeNode *parent, data_t data) {
  SBBSTreeNode *node = parent->child;
  if (node != NULL) {
    if (node->sibling == NULL) {
      if (compareData(parent->child->data, data)) {
        if (node->child != NULL) {
          printf("maaf anak anda memiliki anak\n");
        } else {
          parent->child = NULL;
          free(node);
        }
      } else {
        printf("tidak ada SBBSTreeNode anak dengan info karakter masukan\n");
      }
    } else {
      SBBSTreeNode *prec = NULL;
      int ketemu = 0;
      while ((node->sibling != parent->child) && (ketemu == 0)) {
        if (compareData(node->data, data)) {
          ketemu = 1;
        } else {
          prec = node;
          node = node->sibling;
        }
      }
      if ((ketemu == 0) && (compareData(node->data, data))) {
        ketemu = 1;
      }
      if (ketemu == 1) {
        SBBSTreeNode *last = parent->child;
        while (last->sibling != parent->child) {
          last = last->sibling;
        }
        if (prec == NULL) {
          if ((node->sibling == last) && (last->sibling == parent->child)) {
            parent->child = last;
            last->sibling = NULL;
          } else {
            parent->child = node->sibling;
            last->sibling = parent->child;
          }
        } else {
          if ((prec == parent->child) && (node->sibling == parent->child)) {
            parent->child->sibling = NULL;
          } else {
            prec->sibling = node->sibling;
            node->sibling = NULL;
          }
        }
        free(node);
      } else {
        printf("tidak ada SBBSTreeNode anak dengan info karakter masukan\n");
      }
    }
  }
}

SBBSTreeNode *findSimpul(SBBSTreeNode *parent, char *name) {
  SBBSTreeNode *hasil = NULL;
  if (parent != NULL) {
    if (!strcmp(parent->data.name, name)) {
      hasil = parent;
    } else {
      SBBSTreeNode *node = parent->child;
      if (node != NULL) {
        if (node->sibling == NULL) {
          if (!strcmp(parent->data.name, name)) {
            hasil = node;
          } else {
            hasil = findSimpul(node, name);
          }
        } else {
          int ketemu = 0;
          while ((node->sibling != parent->child) && (ketemu == 0)) {
            if (!strcmp(parent->data.name, name)) {
              hasil = node;
              ketemu = 1;
            } else {
              if (hasil == NULL) {
                hasil = findSimpul(node, name);
                node = node->sibling;
              } else {
                ketemu = 1;
              }
            }
          }
          if (ketemu == 0) {
            if (!strcmp(parent->data.name, name)) {
              hasil = node;
            } else {
              if (hasil == NULL) {
                hasil = findSimpul(node, name);
                node = node->sibling;
              } else {
                ketemu = 1;
              }
            }
          }
        }
      }
    }
  }
  return hasil;
}

void printTreePreOrder(SBBSTreeNode *parent) {
  if (parent != NULL) {
    int spaces = strlen(parent->data.name);
    printf("%s\n", parent->data.name);
    int i, j;
    for (i = 0; i < parent->data.n_bad; ++i) {
      for (j = 0; j < spaces; ++j) {
        printf(" ");
      }
      printf(" %s\n", parent->data.bad[i]);
      if (spaces < strlen(parent->data.bad[i])) {
        spaces = strlen(parent->data.bad[i]);
      }
    }
    for (i = 0; i < parent->data.n_good; ++i) {
      printf(" %s\n", parent->data.good[i]);
      if (spaces < strlen(parent->data.good[i])) {
        spaces = strlen(parent->data.good[i]);
      }
    }
    SBBSTreeNode *node = parent->child;
    if (node != NULL) {
      if (node->sibling == NULL) {
        printf("\n");
        printTreePreOrderIndented(node, spaces);
      } else {
        while (node->sibling != parent->child) {
          printf("\n");
          printTreePreOrderIndented(node, spaces);
          node = node->sibling;
        }
        printf("\n");
        printTreePreOrderIndented(node, spaces);
      }
    }
  }
}

void printTreePreOrderIndented(SBBSTreeNode *parent, int spaces) {
  if (parent != NULL) {
    int longest = strlen(parent->data.name);
    int i, j;
    for (j = 0; j < spaces; ++j) {
      printf(" ");
    }
    printf("|%s\n", parent->data.name);
    for (i = 0; i < parent->data.n_bad; ++i) {
      for (j = 0; j < spaces; ++j) {
        printf(" ");
      }
      printf(" %s\n", parent->data.bad[i]);
      if (longest < strlen(parent->data.bad[i])) {
        longest = strlen(parent->data.bad[i]);
      }
    }
    for (i = 0; i < parent->data.n_good; ++i) {
      for (j = 0; j < spaces; ++j) {
        printf(" ");
      }
      printf(" %s\n", parent->data.good[i]);
      if (longest < strlen(parent->data.good[i])) {
        longest = strlen(parent->data.good[i]);
      }
    }
    spaces += longest + 1;
    SBBSTreeNode *node = parent->child;
    if (node != NULL) {
      if (node->sibling == NULL) {
        printf("\n");
        printTreePreOrderIndented(node, spaces);
      } else {
        while (node->sibling != parent->child) {
          printf("\n");
          printTreePreOrderIndented(node, spaces);
          node = node->sibling;
        }
        printf("\n");
        printTreePreOrderIndented(node, spaces);
      }
    }
  }
}

void printTreePostOrder(SBBSTreeNode *parent) {
  if (parent != NULL) {
    SBBSTreeNode *node = parent->child;
    if (node != NULL) {
      if (node->sibling == NULL) {
        printTreePostOrder(node);
      } else {
        while (node->sibling != parent->child) {
          printTreePostOrder(node);
          node = node->sibling;
        }
        printTreePostOrder(node);
      }
    }
    printf(" %s ", parent->data.name);
  }
}

void copyTree(SBBSTreeNode *parent1, SBBSTreeNode **parent2) {
  if (parent1 != NULL) {
    *parent2 = (SBBSTreeNode *)malloc(sizeof(SBBSTreeNode));
    (*parent2)->data = parent1->data;
    (*parent2)->sibling = NULL;
    (*parent2)->child = NULL;
    if (parent1->child != NULL) {
      if (parent1->child->sibling == NULL) {
        copyTree(parent1->child, &(*parent2)->child);
      } else {
        SBBSTreeNode *node1 = parent1->child;
        SBBSTreeNode *node2 = (*parent2)->child;
        while (node1->sibling != parent1->child) {
          copyTree(node1, &(node2));
          node1 = node1->sibling;
          node2 = node2->sibling;
        }
        copyTree(node1, &(node2));
      }
    }
  }
}

int isEqual(SBBSTreeNode *parent1, SBBSTreeNode *parent2) {
  int hasil = 1;
  if ((parent1 != NULL) && (parent2 != NULL)) {
    if (!compareData(parent1->data, parent2->data)) {
      hasil = 0;
    } else {
      if ((parent1->child != NULL) && (parent2->child != NULL)) {
        if (parent1->child->sibling == NULL) {
          hasil = isEqual(parent1->child, parent2->child);
        } else {
          SBBSTreeNode *node1 = parent1->child;
          SBBSTreeNode *node2 = parent2->child;
          while (node1->sibling != parent1->child) {
            if ((node1 != NULL) && (node2 != NULL)) {
              hasil = isEqual(node1, node2);
              node1 = node1->sibling;
              node2 = node2->sibling;
            } else {
              hasil = 0;
              break;
            }
          }
          hasil = isEqual(node1, node2);
        }
      }
    }
  } else {
    if ((parent1 != NULL) || (parent2 != NULL)) {
      hasil = 0;
    }
  }
  return hasil;
}

int compareData(data_t data1, data_t data2) {
  if (!strcmp(data1.name, data2.name)) {
    int identical = 1;
    int i = 0;
    if (data1.n_good == data2.n_good) {
      while (identical) {
        if (strcmp(data1.good[i], data2.good[i])) return 0;
        i += 1;
      }
    }
    if (data1.n_bad == data2.n_bad) {
      i = 0;
      while (identical) {
        if (strcmp(data1.bad[i], data2.bad[i])) return 0;
        i += 1;
      }
    }
  }
  return 1;
}
