/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur pada saat mengerjakan Remedial Struktur Data. Jika
 * saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include "lolR14.h"

int main(int argc, char const *argv[]) {
  int i;
  int num_query;
  int num_vowel;
  int exist_in_row = 0;
  char string[32];
  list L;
  createList(&L);
  eBaris *row_element = L.first;
  eBaris *row_element2 = L.first;

  scanf("%d", &num_query);

  for (i = 0; i < num_query; i += 1) {  // iterate in the number of query.
    scanf("%s", string);
    num_vowel = countVowel(string);
    exist_in_row = findRowElement(num_vowel, L);

    if (!exist_in_row) {  // if number of vowel doesn't exist in row of list.
      addLastB(num_vowel, &L);

      if (i != 0 && row_element->next != NULL) {  // row element walker
        row_element = row_element->next;
      } else {
        row_element = L.first;
      }

      addLastK(string, row_element);
    } else {  // when number of vowel exist in row of list.
      row_element2 = L.first;

      while (row_element2->next != NULL &&
             row_element2->elmt.number != num_vowel) {  // row element walker
        row_element2 = row_element2->next;
      }

      addLastK(string, row_element2);
    }
  }

  printChildVowel(L);

  return 0;
}
