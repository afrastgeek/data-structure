/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur pada saat mengerjakan Remedial Struktur Data. Jika
 * saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef struct { char number; } row_element_t;

typedef struct { char word[32]; } column_element_t;

typedef struct eklm *alamatekolom;

typedef struct eklm {
  column_element_t elmt;
  alamatekolom next;
} eKolom;

typedef struct ebr *alamatebaris;

typedef struct ebr {
  row_element_t elmt;
  eKolom *col;
  alamatebaris next;
} eBaris;

typedef struct { eBaris *first; } list;

void createList(list *L);
int countElementB(list L);
int countElementK(eBaris L);
void addFirstB(int number, list *L);
void addFirstK(char word[], eBaris *L);
void addAfterB(eBaris *prev, int number);
void addAfterK(eKolom *prev, char word[]);
void addLastB(int number, list *L);
void addLastK(char word[], eBaris *L);
void delFirstB(list *L);
void delFirstK(eBaris *L);
void delAfterB(eBaris *prev);
void delAfterK(eKolom *prev);
void delLastB(list *L);
void delLastK(eBaris *L);
void delAllB(list *L);
void delAllK(eBaris *L);
void printElement(list L);

int countVowel(char word[32]);
int findRowElement(int angka, list L);
void printVowel(char word[]);
void printChildVowel(list L);
