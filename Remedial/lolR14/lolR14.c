/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur pada saat mengerjakan Remedial Struktur Data. Jika
 * saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include "lolR14.h"

void createList(list *L) { (*L).first = NULL; }

int countElementB(list L) {
  int hasil = 0;
  if (L.first != NULL) {    /* list tidak kosong */
    eBaris *elmt = L.first; /* iniialisasi */
    while (elmt != NULL) {  /* proses */
      hasil = hasil + 1;
      elmt = elmt->next; /* iterasi */
    }
  }
  return hasil;
}

int countElementK(eBaris L) {
  int hasil = 0;
  if (L.col != NULL) {     /* list tidak kosong */
    eKolom *elmt = L.col;  /* iniialisasi */
    while (elmt != NULL) { /* proses */
      hasil = hasil + 1;
      elmt = elmt->next; /* iterasi */
    }
  }
  return hasil;
}

void addFirstB(int number, list *L) {
  eBaris *elmt;
  elmt = (eBaris *)malloc(sizeof(eBaris));
  elmt->elmt.number = number;
  elmt->col = NULL;
  if ((*L).first == NULL) {
    elmt->next = NULL;
  } else {
    elmt->next = (*L).first;
  }
  (*L).first = elmt;
  elmt = NULL;
}

void addFirstK(char word[], eBaris *L) {
  eKolom *elmt;
  elmt = (eKolom *)malloc(sizeof(eKolom));
  strcpy(elmt->elmt.word, word);
  if ((*L).col == NULL) {
    elmt->next = NULL;
  } else {
    elmt->next = (*L).col;
  }
  (*L).col = elmt;
  elmt = NULL;
}

void addAfterB(eBaris *prev, int number) {
  eBaris *elmt;
  elmt = (eBaris *)malloc(sizeof(eBaris));
  elmt->elmt.number = number;
  elmt->col = NULL;
  if (prev->next == NULL) {
    elmt->next = NULL;
  } else {
    elmt->next = prev->next;
  }
  prev->next = elmt;
  elmt = NULL;
}

void addAfterK(eKolom *prev, char word[]) {
  eKolom *elmt;
  elmt = (eKolom *)malloc(sizeof(eKolom));
  strcpy(elmt->elmt.word, word);
  if (prev->next == NULL) {
    elmt->next = NULL;
  } else {
    elmt->next = prev->next;
  }
  prev->next = elmt;
  elmt = NULL;
}

void addLastB(int number, list *L) {
  if ((*L).first == NULL) { /* jika list adalah list kosong */
    addFirstB(number, L);
  } else { /* jika list tidak kosong */
    eBaris *elmt;
    elmt = (eBaris *)malloc(sizeof(eBaris));
    elmt->elmt.number = number;
    elmt->next = NULL;
    elmt->col = NULL;
    eBaris *last = (*L).first;
    while (last->next != NULL) { /* mencari elemen terakhir list */
      last = last->next;         /* iterasi */
    }
    last->next = elmt;
    elmt = NULL;
  }
}

void addLastK(char word[], eBaris *L) {
  if ((*L).col == NULL) { /* jika list adalah list kosong */
    addFirstK(word, L);
  } else { /* jika list tidak kosong */
    eKolom *elmt;
    elmt = (eKolom *)malloc(sizeof(eKolom));
    strcpy(elmt->elmt.word, word);
    elmt->next = NULL; /* mencari elemen erakhir list */
    eKolom *last = (*L).col;
    while (last->next != NULL) {
      last = last->next; /* iterasi */
    }
    last->next = elmt;
    elmt = NULL;
  }
}

void delFirstB(list *L) {
  if ((*L).first != NULL) { /* jika list bukan list kosong */
    eBaris *elmt = (*L).first;
    if (countElementB(*L) == 1) {
      (*L).first = NULL;
    } else {
      (*L).first = (*L).first->next;
      elmt->next = NULL;
    }
    free(elmt);
  }
}

void delFirstK(eBaris *L) {
  if ((*L).col != NULL) { /* jika list bukan list kosong */
    eKolom *elmt = (*L).col;
    if (countElementK(*L) == 1) {
      (*L).col = NULL;
    } else {
      (*L).col = (*L).col->next;
      elmt->next = NULL;
    }
    free(elmt);
  }
}

void delAfterB(eBaris *prev) {
  eBaris *elmt = prev->next;
  if (elmt->next == NULL) {
    prev->next = NULL;
  } else {
    prev->next = elmt->next;
    elmt->next = NULL;
  }
  free(elmt);
}

void delAfterK(eKolom *prev) {
  eKolom *elmt = prev->next;
  if (elmt->next == NULL) {
    prev->next = NULL;
  } else {
    prev->next = elmt->next;
    elmt->next = NULL;
  }
  free(elmt);
}

void delLastB(list *L) {
  if ((*L).first != NULL) {       /* jika list tidak kosong */
    if (countElementB(*L) == 1) { /* list terdiri dari satu elemen */
      delFirstB(L);
    } else { /* mencari elemen terakhir list */
      eBaris *last = (*L).first;
      eBaris *before_last;
      while (last->next != NULL) { /* iterasi */
        before_last = last;
        last = last->next;
      }
      before_last->next = NULL;
      free(last);
    }
  }
}

void delLastK(eBaris *L) {
  if ((*L).col != NULL) {         /* jika list tidak kosong */
    if (countElementK(*L) == 1) { /* list terdiri dari satu elemen */
      delFirstK(L);
    } else { /* mencari elemen terakhir list */
      eKolom *last = (*L).col;
      eKolom *before_last;
      while (last->next != NULL) {
        before_last = last; /* iterasi */
        last = last->next;
      }
      before_last->next = NULL;
      free(last);
    }
  }
}

void printElement(list L) {
  if (L.first != NULL) {    /* jika list tidak kosong */
    eBaris *elmt = L.first; /* inisialisasi */
    int i = 1;
    while (elmt != NULL) { /* proses */
      printf("elemen ke : %d\n", i);
      printf("number : %d\n", elmt->elmt.number);
      eKolom *eCol = elmt->col;
      while (eCol != NULL) {
        printf("kode kuliah : %s\n", eCol->elmt.word);
        eCol = eCol->next;
      }
      printf("------------\n");
      elmt = elmt->next; /* iterasi */
      i = i + 1;
    }
  } else { /* proses jika list kosong */
    printf("list kosong\n");
  }
}

void delAllB(list *L) {
  if (countElementB(*L) != 0) {
    int i;
    for (i = countElementB(*L); i >= 1; i--) {
      delLastB(L); /* proses menghapus elemen list */
    }
  }
}

void delAllK(eBaris *L) {
  if (countElementK(*L) != 0) {
    int i;
    for (i = countElementK(*L); i >= 1; i--) {
      delLastK(L);
    }
  }
}

/**
 * @brief      Counts the number of vowel in a word.
 *
 * @param      word  The word to count vowel from.
 *
 * @return     Number of vowel.
 */
int countVowel(char word[32]) {
  int num_character = strlen(word);
  int num_vowel = 0;
  while (num_character) {
    num_character -= 1;
    if (word[num_character] == 'a' || word[num_character] == 'i' ||
        word[num_character] == 'u' || word[num_character] == 'e' ||
        word[num_character] == 'o') {
      num_vowel += 1;
    }
  }
  return num_vowel;
}

/**
 * @brief      Find an element in row of a list.
 *
 * @param[in]  number  The number to search.
 * @param[in]  L       List to search from.
 *
 * @return     1 if found, 0 otherwise.
 */
int findRowElement(int number, list L) {
  if (L.first != NULL) {  // only proceed when list isn't empty
    eBaris *this_node = L.first;
    while (this_node != NULL) {
      if (this_node->elmt.number == number) return 1;
      this_node = this_node->next;
    }
  }
  return 0;
}

/**
 * @brief      Print the vowel character from word passed.
 *
 * @param      word  The word to print.
 */
void printVowel(char word[]) {
  int i;
  for (i = 0; i < strlen(word); ++i) {
    if (word[i] == 'a' || word[i] == 'i' || word[i] == 'u' || word[i] == 'e' ||
        word[i] == 'o') {
      printf("%c", word[i]);
    }
  }
  printf("\n");
}

/**
 * @brief      Print only vowel in child element of List.
 *
 * @param[in]  L     List to print from.
 */
void printChildVowel(list L) {
  if (L.first != NULL) {
    eBaris *this_node = L.first;
    while (this_node != NULL) {
      printf("%d\n", this_node->elmt.number);
      eKolom *this_child = this_node->col;
      while (this_child != NULL) {
        printVowel(this_child->elmt.word);
        this_child = this_child->next;
      }
      this_node = this_node->next;
    }
  } else {
    printf("list kosong\n");
  }
}
