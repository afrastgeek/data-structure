/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur pada saat mengerjakan Remedial Struktur Data. Jika
 * saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include "grrem15.h"

void createEmpty(graph *G) { (*G).first = NULL; }

void addSimpul(char c, graph *G) {
  simpul *baru;
  baru = (simpul *)malloc(sizeof(simpul));
  baru->info = c;
  baru->next = NULL;
  baru->arc = NULL;

  if ((*G).first == NULL) {
    /* jika graf nya graf kosong */
    (*G).first = baru;  // langsung diisi
  } else {
    /* menambahkan simpul baru pada akhir graf */
    simpul *last = (*G).first;  // untuk mencari elemen terakhir

    while (last->next != NULL) {
      last = last->next;
    }
    last->next = baru;
  }
}

void addJalur(simpul *akhir, simpul *awal) {
  jalur *baru;
  baru = (jalur *)malloc(sizeof(jalur));

  baru->next = NULL;
  baru->tujuan = akhir;

  if (awal->arc == NULL) {
    /* jika list jalur kosong */
    awal->arc = baru;
  } else {
    /* menambahkan jalur baru pada akhir list jalur */
    jalur *last = awal->arc;
    while (last->next != NULL) {
      last = last->next;
    }
    last->next = baru;
  }
}

void delJalur(char ctujuan, simpul *awal) {
  jalur *arc = awal->arc;
  if (arc != NULL) {
    jalur *prec = NULL;
    /* mencari jalur yg ajan dihapus */
    int ketemu = 0;
    while ((arc != NULL) && (ketemu == 0)) {
      if (arc->tujuan->info == ctujuan) {
        ketemu = 1;
      } else {
        prec = arc;
        arc = arc->next;
      }
    }

    if (ketemu == 1) {
      if (prec == NULL) {  // menghapus jalur pertama
        awal->arc = arc->next;
      } else {
        if (arc->next == NULL) {  // menghapus jalur akhir
          prec->next = NULL;
        } else {
          /* hapus jalur di tengah */
          prec->next = arc->next;
          arc->next = NULL;
        }
      }
      free(arc);
    } else {
      printf("tidak ada jalur dengan simpul tujuan\n");
    }
  } else {
    printf("tidak ada jalur dengan simpul tujuan\n");
  }
}

void delSimpul(char c, graph *G) {
  simpul *elmt = (*G).first;

  // siapkan kakaknya
  if (elmt != NULL) {  // ada graf
    simpul *prec = NULL;
    /* mencari simpul yang akan dihapus */
    int ketemu = 0;
    while ((elmt != NULL) && (ketemu == 0)) {
      if (elmt->info == c) {
        ketemu = 1;
      } else {
        prec = elmt;
        elmt = elmt->next;
      }
    }
    if (ketemu == 1) {
      simpul *hapus = (*G).first;
      while (hapus != NULL) {
        if (hapus->info != elmt->info) {
          // skip jika simpul nya sama
          jalur *hapus2 = hapus->arc;
          int status = 0;
          while (hapus2 != NULL && status == 0) {
            // cari yang mengarah ke simpul hapus
            if (hapus2->tujuan->info ==
                elmt->info) {  // ketentuan kalo infonya sama
              status = 1;
              delJalur(elmt->info, hapus);
            } else {
              hapus2 = hapus2->next;
            }
          }
        }
        hapus = hapus->next;
      }
      // hapus semua jalur yang di miliki simpul
      while (elmt->arc != NULL) {
        delJalur(elmt->arc->tujuan->info, elmt);
      }

      // printf("-%c\n", prec->info);
      if (prec == NULL) {
        /* hapus simpul pertama */
        (*G).first = elmt->next;
      } else {
        if (elmt->next == NULL) {
          /* hapus simpul terakhir */
          prec->next = NULL;
        } else {
          /* hapus simpul di tengah */
          prec->next = elmt->next;
          elmt->next = NULL;
        }
      }
      free(elmt);
    } else {
      printf("gaada simpul dengan info karakter maskuna\n");
    }

  } else {
    printf("tidak ada graf\n");
  }
}

simpul *findSimpul(char c, graph G) {
  simpul *hasil = NULL;
  simpul *node = G.first;  // ditunjuk ke graf pertama

  int ketemu = 0;
  while ((node != NULL) &&
         (ketemu == 0)) {  // selama ada isinya dan belum ketemu
    if (node->info == c) {
      hasil = node;
      ketemu = 1;
    } else {
      node = node->next;
    }
  }
  return hasil;
}

void printGraph(graph G) {
  simpul *node = G.first;
  if (node != NULL) {
    while (node != NULL) {
      printf("simpul :%c\n", node->info);
      jalur *arc = node->arc;
      while (arc != NULL) {
        printf("%c ", arc->tujuan->info);
        arc = arc->next;
      }
      printf("\n");
      node = node->next;
    }
  } else {
    printf("graf kosong\n");
  }
}

/**
 * @brief      find Path in a graph. Print "ada" if match, "tidak ada"
 *             otherwise.
 *
 * @param[in]  G     Graph to find from.
 */
void findPath(graph G) {
  int counter = 0;
  int path;
  simpul *node = G.first;
  int i;
  if (node != NULL) {
    counter = 0;
    
    while (node != NULL) {
      path = 0;
      jalur *arc = node->arc;
      while (arc != NULL) {
        path++;
        arc = arc->next;
      }

      if (path == 1) {
        cari[counter] = node;
        counter++;
      }
      node = node->next;
    }
  }

  printf("%d\n", counter);
  for (i = 0; i < counter; i++) {
    if (i == counter - 1) {
      printf("tidak ada\n");
    } else {
      printf("ada\n");
    }
  }
}
