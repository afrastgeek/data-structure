/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur pada saat mengerjakan Remedial Struktur Data. Jika
 * saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include "grrem15.h"

int main(int argc, char const *argv[]) {
  graph G;
  int i;
  int num_query;
  createEmpty(&G);
  char graf_path[50];
  char to_check[50];

  scanf("%d", &num_query);
  for (i = 0; i < num_query; i++) {
    scanf(" %c", &graf_path[i]);
    scanf(" %c", &to_check[i]);
  }

  for (i = 0; i < num_query; i++) {
    if (findSimpul(graf_path[i], G) == NULL) addSimpul(graf_path[i], &G);
    if (findSimpul(to_check[i], G) == NULL) addSimpul(to_check[i], &G);

    simpul *begin = findSimpul(graf_path[i], G);
    simpul *end = findSimpul(to_check[i], G);
    addJalur(end, begin);
  }

  findPath(G);

  return 0;
}
