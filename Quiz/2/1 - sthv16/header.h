/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Kuis 2 Struktur Data.
 * Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include <stdio.h>
#include <stdlib.h>
#include <malloc.h>
#include <string.h>

typedef struct {
  char operation[16][16];
  int element;
} content_t;

typedef struct content *element_address;
typedef struct content {
  content_t content;
  element_address next;
} node_t;

typedef struct { node_t *top; } stack_t;

void createEmpty(stack_t *S);
int isEmpty(stack_t S);
int countElement(stack_t S);
void push(content_t content, stack_t *S);
void pop(stack_t *S);
void printStack(stack_t S);
int getEqualSign(char equation[16][16], int num_element);
char *getVariable(char equation[16][16], int middle, int num_element);
int sumElement(char equation[16][16], int start, int stop);
void solve(char equation[16][16], int num_element);
