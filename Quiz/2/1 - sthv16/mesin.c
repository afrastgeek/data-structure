/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Kuis 2 Struktur Data.
 * Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include "header.h"

/**
 * @brief      Creates an empty stack.
 *
 * @param      S     Stack to initialize.
 */
void createEmpty(stack_t *S) { (*S).top = NULL; }

/**
 * @brief      Determines if the stack is empty.
 *
 * @param[in]  S     Stack to check.
 *
 * @return     True if empty, False otherwise.
 */
int isEmpty(stack_t S) {
  int result = 0;
  if (S.top == NULL) {
    result = 1;
  }
  return result;
}

/**
 * @brief      Counts the number of element in stack.
 *
 * @param[in]  S     The stack.
 *
 * @return     Number of element.
 */
int countElement(stack_t S) {
  int result = 0;
  if (S.top != NULL) {  // only proceed when the stack isn't empty
    node_t *this_node;
    this_node = S.top;

    while (this_node != NULL) {  // walk throughout the stack
      result = result + 1;
      this_node = this_node->next;
    }
  }
  return result;
}

/**
 * @brief      Push content to the top of stack.
 *
 * @param[in]  content  The content
 * @param      S        The stack
 */
void push(content_t content, stack_t *S) {
  node_t *this_node;
  this_node = (node_t *)malloc(sizeof(node_t));

  this_node->content = content;
  this_node->next = (*S).top;
  (*S).top = this_node;

  this_node = NULL;
}

/**
 * @brief      Pop the most top content of stack.
 *
 * @param      S     The stack
 */
void pop(stack_t *S) {
  if ((*S).top != NULL) {  // Only proceed when stack isn't empty
    node_t *this_node = (*S).top;

    (*S).top = (*S).top->next;
    this_node->next = NULL;

    free(this_node);
  }
}

/**
 * @brief      Print the element of stack.
 *
 * @param[in]  S     The stack
 */
void printStack(stack_t S) {
  if (S.top != NULL) {
    stack_t N;
    createEmpty(&N);

    // reorder the stack, move to the N stack.
    node_t *this_node = S.top;
    while (this_node != NULL) {
      push(this_node->content, &N);
      this_node = this_node->next;
    }

    // print equation result
    this_node = N.top;
    while (this_node != NULL) {
      solve(this_node->content.operation, this_node->content.element);
      this_node = this_node->next;
    }
  } else { /*proses jika stack kosong*/
    printf("Stack Kosong\n");
  }
}

/**
 * @brief      Gets the equal sign index.
 *
 * @param      equation     The equation
 * @param[in]  num_element  The number of element
 *
 * @return     The equal sign index.
 */
int getEqualSign(char equation[16][16], int num_element) {
  int result = -1;
  int i;
  for (i = 0; i < num_element; ++i) {
    if (!strcmp(equation[i], "=")) {
      result = i;
    }
  }
  return result;
}

/**
 * @brief      Gets the variable from equation.
 *
 * @param      equation     The equation
 * @param[in]  middle       The middle (location of equal sign)
 * @param[in]  num_element  The number of element
 *
 * @return     The variable.
 */
char *getVariable(char equation[16][16], int middle, int num_element) {
  int i;
  for (i = 0; i < num_element; ++i) {
    if (strcmp(equation[i], "=") && !atoi(equation[i])) {
      return equation[i];
    }
  }
  return "";
}

/**
 * @brief      Calculate element in one side of equation.
 *
 * @param      equation  The equation
 * @param[in]  start     The start location.
 * @param[in]  stop      The stop location.
 *
 * @return     Sum of element.
 */
int sumElement(char equation[16][16], int start, int stop) {
  int result = 0;
  int i;
  for (i = start; i < stop; ++i) {
    if (atoi(equation[i])) {
      result += atoi(equation[i]);
    }
  }
  return result;
}

/**
 * @brief      Solve the sum equation problem and print the result.
 *
 * @param      equation     The equation
 * @param[in]  num_element  The number of element
 */
void solve(char equation[16][16], int num_element) {
  int equal_sign_location = getEqualSign(equation, num_element);

  int left = sumElement(equation, 0, equal_sign_location);
  int right = sumElement(equation, equal_sign_location, num_element);

  char variable[16];
  strcpy(variable, getVariable(equation, equal_sign_location, num_element));

  if (left) {
    printf("%s = %d\n", variable, left);
  } else if (right) {
    printf("%s = %d\n", variable, right);
  }
}
