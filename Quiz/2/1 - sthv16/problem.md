(sthv16) Stack Hitung Variabel


Pembuat Soal: Rosa A. S.

Batas Waktu Eksekusi  5 Detik
Batas Memori  1 MB



- jangan lupa ketik janji setia pada kejujuran
- setiap suasana tidak kondusif akan mendapat teguran, dan setiap teguran ketiga akan dikenakan pemotongan waktu 10 menit bagi semua peserta evaluasi.

Kompetensi yang dievaluasi: Kemampuan memahami konsep stack
Gunakan stack dengan representasi dinamis untuk soal ini. Diberikan n buah urutan skrip pertambahan, hitunglah dengan menggunakan stack berapa hasil dari pertambahan. Boleh menggunakan atoi untuk mengubah string menjadi integer. Operator yang digunakan hanya pertambahan. Masukkan urutan skrip ke dalam stack lalu hitung hasilnya.
Format Masukan:

n, 0 < n < 50, banyaknya urutan skrip pertambahan
n baris urutan skrip pertambahan yang terdiri dari:
   m, 0 < m < 20, banyaknya string skrip.
   m baris string skrip.
Format Keluaran:

n baris hasil isi variabel
Contoh Masukan

2
5
a
=
23
+
2
3
b
=
5


Contoh Keluaran

a = 25
b = 5

Contoh Masukan 2

1
7
v
=
1
+
2
+
3


Contoh Keluaran 2

v = 6
