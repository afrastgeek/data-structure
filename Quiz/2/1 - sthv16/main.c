/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Kuis 2 Struktur Data.
 * Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include "header.h"

int main(int argc, char const *argv[]) {
  stack_t operation;
  createEmpty(&operation);

  int num_operation;
  scanf("%d", &num_operation);

  int i, j;
  content_t query[num_operation];
  for (i = 0; i < num_operation; i += 1) {
    scanf("%d", &query[i].element); // number of element
    for (j = 0; j < query[i].element; j += 1) {
      scanf(" %s", query[i].operation[j]); // get equation element
    }
    push(query[i], &operation); // insert complete equation
  }

  printStack(operation); // print the solved equation

  return 0;
}
