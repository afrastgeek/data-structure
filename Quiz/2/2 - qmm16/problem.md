(qmm16) Mari Mengantri dengan Berkah


Pembuat Soal: Rosa A. S.

Batas Waktu Eksekusi  5 Detik
Batas Memori  1 MB



- jangan lupa ketik janji setia pada kejujuran
- setiap suasana tidak kondusif akan mendapat teguran, dan setiap teguran ketiga akan dikenakan pemotongan waktu 10 menit bagi semua peserta evaluasi.

Kompetensi yang dievaluasi: Kemampuan memahami konsep queue
Gunakan queue dengan representasi dinamis untuk soal ini. Diberikan n buah data orang yang akan masuk ke dalam antrian, beserta prioritasnya. Kemudian diberikan sebuah angka masukan jumlah orang yang keluar dari antrian. Tampilkan data orang yang keluar dari antrian.
Format Masukan:

n, 0 < n < 50, banyaknya data orang.
n baris data orang yang terdiri dari nama dan nomor antrian.
m, 0 < m < 50, banyaknya orang yang keluar antrian.

Format Keluaran:

m baris orang yang keluar antrian dan posisinya di antrian.

Contoh Masukan

3
dia 3
dina 14
sitata 1
1


Contoh Keluaran

sitata 1

Contoh Masukan 2

2
dita 17
tina 2
2


Contoh Keluaran 2

dita 1
tina 2
