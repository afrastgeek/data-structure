/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Kuis 2 Struktur Data.
 * Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef struct { char name[16]; } content_t;

typedef struct node *node_address;
typedef struct node {
  content_t content;
  node_address next;
} node_t;

typedef struct {
  node_t *first;
  node_t *last;
} queue_t;

void createEmpty(queue_t *Q);
int isEmpty(queue_t Q);
int countElement(queue_t Q);
void add(content_t query, queue_t *Q);
void addPriority(int priority, content_t query, queue_t *Q);
void del(queue_t *Q);
void printQueue(queue_t Q, int limit);
