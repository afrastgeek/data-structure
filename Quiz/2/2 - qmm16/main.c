/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Kuis 2 Struktur Data.
 * Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include "header.h"

int main() {
  queue_t Q;        // create a queue
  createEmpty(&Q);  // initialize the queue

  int num_people;
  scanf("%d", &num_people);

  int i;
  content_t query;
  int priority = 0;
  for (i = 0; i < num_people; i += 1) {
    scanf(" %s %d", query.name, &priority);  // get name and queue priority
    addPriority(priority, query, &Q);
  }

  int num_out;
  scanf("%d", &num_out);
  printQueue(Q, num_out);  // print the person out from queue till the num_out.

  return 0;
}
