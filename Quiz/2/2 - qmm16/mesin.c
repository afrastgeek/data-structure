/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Kuis 2 Struktur Data.
 * Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include "header.h"

/**
 * @brief      Creates an empty Queue.
 *
 * @param      Q     The Queue
 */
void createEmpty(queue_t *Q) {
  (*Q).first = NULL;
  (*Q).last = NULL;
}

/**
 * @brief      Determines if given queue is empty.
 *
 * @param[in]  Q     The queue
 *
 * @return     True if empty, False otherwise.
 */
int isEmpty(queue_t Q) {
  int hasil = 0;
  if (Q.first == NULL) {
    hasil = 1;
  }
  return hasil;
}

/**
 * @brief      Counts the number of element in queue.
 *
 * @param[in]  Q     The queue
 *
 * @return     Number of element.
 */
int countElement(queue_t Q) {
  int hasil = 0;
  if (Q.first != NULL) {
    node_t *elmt;
    elmt = Q.first;

    while (elmt != NULL) {
      hasil = hasil + 1;
      elmt = elmt->next;
    }
  }
  return hasil;
}

/**
 * @brief      Add a query item to the queue
 *
 * @param[in]  query  The query
 * @param      Q      The queue
 */
void add(content_t query, queue_t *Q) {
  node_t *elmt;
  elmt = (node_t *)malloc(sizeof(node_t));
  elmt->content = query;
  elmt->next = NULL;
  if ((*Q).first == NULL) {
    (*Q).first = elmt;
  } else {
    (*Q).last->next = elmt;
  }
  (*Q).last = elmt;
  elmt = NULL;
}

/**
 * @brief      Adds a query item to queue with given priority.
 *
 * @param[in]  priority  The priority
 * @param[in]  query     The query
 * @param      Q         The queue
 */
void addPriority(int priority, content_t query, queue_t *Q) {
  node_t *elmt;
  elmt = (node_t *)malloc(sizeof(node_t));
  elmt->content = query;
  if (isEmpty(*Q) == 1) {  // if thequeue is empty
    (*Q).first = elmt;
    (*Q).last = elmt;
    elmt->next = NULL;
  } else {
    if (priority > countElement(*Q)) {  // if priority number bigger than queue
      (*Q).last->next = elmt;
      (*Q).last = elmt;
      elmt->next = NULL;
    } else {
      if (priority == 1) {  // if highest priority, add at start of queue
        elmt->next = (*Q).first;
        (*Q).first = elmt;
      } else {  // otherwise, add at the priority number on the middle of queue
        int i;
        node_t *prev = (*Q).first;
        for (i = 1; i < priority - 1; i++) {
          prev = prev->next;
        }
        elmt->next = prev->next;
        prev->next = elmt;
      }
    }
  }
  elmt = NULL;
}

/**
 * @brief      Delete a query item from queue.
 *
 * @param      Q     The queue
 */
void del(queue_t *Q) {
  if ((*Q).first != NULL) {
    node_t *elmt = (*Q).first;
    if (countElement(*Q) == 1) {
      (*Q).first = NULL;
    } else {
      (*Q).first = (*Q).first->next;
      elmt->next = NULL;
    }
    free(elmt);
  }
}

/**
 * @brief      Print the Queue with given limit. Only print item from the start
 *             of queue till the limit number.
 *
 * @param[in]  Q      The queue
 * @param[in]  limit  The limit
 */
void printQueue(queue_t Q, int limit) {
  if (Q.first != NULL) {
    node_t *elmt = Q.first;
    int i = 0;

    while (elmt != NULL) {
      if (i < limit) {
        printf("%s %d\n", elmt->content.name, i + 1);
      }
      elmt = elmt->next;
      i++;
    }
  } else {
    printf("Queue Kosong!\n");
  }
}
