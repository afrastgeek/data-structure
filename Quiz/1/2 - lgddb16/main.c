/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Kuis 1 Struktur Data.
 * Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include "lgddb16.h"

int main(int argc, char const *argv[]) {
  int i;
  int num_query;
  int numeric;
  list L;
  createList(&L);

  scanf("%d", &num_query);
  for (i = 0; i < num_query; i += 1) {
    scanf(" %d", &numeric);
    addTail(numeric, &L);
  }

  express999(&L);

  printElement(L);
  printElementToHead(L);

  return 0;
}

/*

(lgddb16) List Ganda Depan Belakang


Pembuat Soal: Rosa A. S.

Batas Waktu Eksekusi  5 Detik
Batas Memori  1 MB



- jangan lupa ketik janji setia pada kejujuran
- setiap suasana tidak kondusif akan mendapat teguran, dan setiap teguran ketiga akan dikenakan pemotongan waktu 10 menit bagi semua peserta evaluasi.

Kompetensi yang dievaluasi: Pemahaman konsep dan implementasi list ganda beserta operasinya.
Gunakan list ganda dinamis untuk menyelesaikan soal ini dan kerjakan sesuai spesifikasi.
Diberikan n masukan berupa integer, masukkan semua elemen ke dalam sebuah list ganda dari belakang (dari tail dengan addfirst belakang). Lalu ubah elemen tengah dengan angka 999. Dan tampilkan isi list dari depan dan belakang kecuali angka 999 yang berada di tengah.
Masukan:

n, 0 < n < 50, banyaknya integer, selalu ganjil
n baris integer.
Keluaran:

isi list akhir
Contoh Masukan

3
1
2
3


Contoh Keluaran

depan 1
belakang 3

Contoh Masukan 2

5
1
2
3
4
5


Contoh Keluaran 2

depan 1 2
belakang 5 4

 */
