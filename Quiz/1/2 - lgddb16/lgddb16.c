/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Kuis 1 Struktur Data.
 * Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include "lgddb16.h"

/**
 * @brief      Creates a list from passed list by initializing it's first
 *             and last element to NULL.
 *
 * @param      L     List given to initialize.
 */
void createList(list *L) { (*L).first = (*L).tail = NULL; }

/**
 * @brief      Counts the number of element from list passed.
 *
 * @param[in]  L     List given to count it's element.
 *
 * @return     Number of element.
 */
int countElement(list L) {
  int num_node = 0;
  if (L.first != NULL) { /* Only process when the list isn't empty */
    node_t *this_node = L.first;
    while (this_node != NULL) { /* Iterate till found the NULL element */
      num_node = num_node + 1;
      this_node = this_node->next;
    }
  }
  return num_node;
}

/**
 * @brief      Adds node with content at the start of a list.
 *
 * @param[in]  content  The content.
 * @param      L        The list to add node to.
 */
void addFirst(int numeric, list *L) {
  node_t *node_this = (node_t *)malloc(sizeof(node_t));
  node_this->content.numeric = numeric;
  if ((*L).first == NULL) {
    node_this->prev = node_this->next = NULL;
    (*L).tail = node_this;
  } else {
    node_this->next = (*L).first;
    node_this->prev = NULL;
    (*L).first->prev = node_this;
  }
  (*L).first = node_this;
  node_this = NULL;
}

/**
 * @brief      Adds node with content at the end of a list. (Modified AddFirst)
 *
 * @param[in]  content  The content.
 * @param      L        The list to add node to.
 */
void addTail(int numeric, list *L) {
  node_t *node_this = (node_t *)malloc(sizeof(node_t));
  node_this->content.numeric = numeric;
  if ((*L).tail == NULL) {
    node_this->prev = node_this->next = NULL;
    (*L).first = node_this;
  } else {
    node_this->next = NULL;
    node_this->prev = (*L).tail;
    (*L).tail->next = node_this;
  }
  (*L).tail = node_this;
  node_this = NULL;
}

void addAfter(node_t *prev, int numeric, list *L) {
  node_t *node_this;
  node_this = (node_t *)malloc(sizeof(node_t));

  node_this->content.numeric = numeric;

  if (prev->next == NULL) {
    node_this->next = NULL;
    (*L).tail = node_this;
  } else {
    node_this->next = prev->next;
    node_this->next->prev = node_this;
  }

  node_this->prev = prev;
  prev->next = node_this;
  node_this = NULL;
}

void addLast(int numeric, list *L) {
  if ((*L).first == NULL) { /*jika list adalah list kosong */
    addFirst(numeric, L);
  } else { /*jika list tidak kosong*/
    node_t *node_this;
    node_this = (node_t *)malloc(sizeof(node_t));

    node_this->content.numeric = numeric;
    node_this->next = NULL;
    (*L).tail->next = node_this;
    node_this->prev = (*L).tail;
    (*L).tail = node_this;
    node_this = NULL;
  }
}

void delFirst(list *L) {
  if ((*L).first != NULL) { /*jika list bukan list kosong*/
    node_t *hapus = (*L).first;
    if (countElement(*L) == 1) {
      (*L).first = NULL;
      (*L).tail = NULL;
    } else {
      (*L).first = (*L).first->next;
      (*L).first->prev = NULL;
      hapus->next = NULL;
    }
    free(hapus);
  }
}

void delAfter(node_t *prev, list *L) {
  node_t *hapus = prev->next;

  if (hapus != NULL) {
    if (hapus->next == NULL) {
      prev->next = NULL;
    } else {
      prev->next = hapus->next;
      hapus->next->prev = prev;
      hapus->next = NULL;
    }
    hapus->prev = NULL;
    free(hapus);
  }
}

void delLast(list *L) {
  if ((*L).first != NULL) {      /*jika list tidak kosong*/
    if (countElement(*L) == 1) { /*list terdiri dari satu node_t*/
      delFirst(L);
    } else { /*mencari node_t terakhir list*/
      node_t *hapus = (*L).tail;
      (*L).tail = hapus->prev;
      (*L).tail->next = NULL;
      hapus->prev = NULL;
      free(hapus);
    }
  }
}

void printElement(list L) {
  if (L.first != NULL) {         /*jika list tidak kosong*/
    node_t *this_node = L.first; /*inisialisasi*/
    int i = 0;
    printf("depan");
    while (this_node != NULL && i < countElement(L) / 2) { /*prosses*/
      printf(" %d", this_node->content.numeric);

      /*iterasi*/
      this_node = this_node->next;
      i = i + 1;
    }
    printf("\n");
  } else { /*proses jika list kosong*/
    printf("liat kosong\n");
  }
}

void printElementToHead(list L) {
  if (L.tail != NULL) { /*jika list tidak kosong*/
    /*inisialisasi*/
    node_t *this_node = L.tail;
    int num_node_half_list = countElement(L) / 2; /* Half length of list */
    int i = 0;
    printf("belakang");
    while (this_node != NULL && i < num_node_half_list) { /* Only print till the middle of the list */
      printf(" %d", this_node->content.numeric);

      /*iterasi*/
      this_node = this_node->prev;
      i = i + 1;
    }
    printf("\n");
  } else {
    /*proses jika list kosong*/
    printf("liat kosong\n");
  }
}

void delAll(list *L) {
  if (countElement(*L) != 0) {
    int i;

    for (i = countElement(*L); i >= 1; i--) { /*proses menghapus node_t list*/
      delLast(L);
    }
  }
}

/**
 * @brief      Express the middle element of given list as 999.
 *
 * @param      L     List to express.
 */
void express999(list *L) {
  if ((*L).first != NULL) { /* Only proceed when the list has element */
    int num_node_half_list = countElement(*L) / 2; /* Half length of list */
    int node_nth = 0;
    node_t *this_node = (*L).first;
    while (node_nth < num_node_half_list) { /* Iterate till on the middle */
      this_node = this_node->next;
      node_nth += 1;
    }
    this_node->content.numeric = 999; /* Set the middle element value as 999 */
  }
}
