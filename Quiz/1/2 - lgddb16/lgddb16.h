/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Kuis 1 Struktur Data.
 * Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include <stdio.h>
#include <malloc.h>
#include <string.h>

typedef struct {
  int numeric;
} random_string_t;

typedef struct content *node_sibling_t;
typedef struct content {
  random_string_t content;
  node_sibling_t prev;
  node_sibling_t next;
} node_t;

typedef struct {
  node_t *first;
  node_t *tail;
} list;

void createList(list *L);
int countElement(list L);
void addFirst(int numeric, list *L);
void addTail(int numeric, list *L);
void addAfter(node_t *prev, int numeric, list *L);
void addLast(int numeric, list *L);
void delFirst(list *L);
void delAfter(node_t *prev, list *L);
void delLast(list *L);
void printElement(list L);
void printElementToHead(list L);
void delAll(list *L);

void express999(list *L);
