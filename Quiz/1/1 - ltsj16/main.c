/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Kuis 1 Struktur Data.
 * Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include "ltsj16.h"

int main(int argc, char const *argv[]) {
  list_t L;
  createList(&L);

  int num_data;
  scanf("%d", &num_data);

  int index;
  random_num_t query;
  for (index = 0; index < num_data; index += 1) {
    scanf("%d", &query.numeric); /* Request for a number */
    addLast(query, &L); /* Insert the number to the list */
  }

  int lo, hi;
  scanf("%d %d", &lo, &hi);

  filterInRange(&L, lo, hi); /* Filter in range element */
  printElement(L); /* Print final form of the list */

  return 0;
}

/*

(ltsj16) List Tunggal Jangkauan


Pembuat Soal: Rosa A. S.

Batas Waktu Eksekusi  5 Detik
Batas Memori  1 MB



- jangan lupa ketik janji setia pada kejujuran
- setiap suasana tidak kondusif akan mendapat teguran, dan setiap teguran ketiga akan dikenakan pemotongan waktu 10 menit bagi semua peserta evaluasi.

Kompetensi yang dievaluasi: Pemahaman konsep dan implementasi list tunggal beserta operasinya.
Gunakan list tunggal statis untuk menyelesaikan soal ini dan kerjakan sesuai spesifikasi.
Diberikan n masukan berupa integer, masukkan semua elemen ke dalam sebuah list tunggal. Masukan berikutnya adalah jangkauan angka awal dan angka akhir. Periksa list dan hapus isi list yang masuk dalam jangkauan.
Masukan:

n, 0 < n < 50, banyaknya integer.
n baris integer.
integer jangkauan awal.
integer jangkauan akhir.

Keluaran:

isi list akhir, jika kosong tampilkan empty list
Contoh Masukan

5
2
3
4
5
6
3
4


Contoh Keluaran

2 5 6

Contoh Masukan 2

2
45
66
2
3


Contoh Keluaran 2

45 66


Contoh Masukan 3

3
22
33
44
1
100


Contoh Keluaran 3

empty list

 */
