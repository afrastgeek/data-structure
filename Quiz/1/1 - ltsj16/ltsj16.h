/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Kuis 1 Struktur Data.
 * Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include <stdio.h>
#include <string.h>

#ifndef MAX_VALUE
#define MAX_VALUE 100
#endif

#ifndef NULL_VALUE
#define NULL_VALUE -1
#endif

#ifndef INIT_VALUE
#define INIT_VALUE -2
#endif

typedef struct {
  int numeric;
} random_num_t;

typedef struct {
  random_num_t content;
  int next;
} element_t;

typedef struct {
  int first;
  element_t node[MAX_VALUE];
} list_t;

void createList(list_t *L);
int countElement(list_t L);
int emptyElement(list_t L);
void addFirst(random_num_t content, list_t *L);
void addAfter(int node_after_this, random_num_t content, list_t *L);
void addLast(random_num_t content, list_t *L);
void delFirst(list_t *L);
void delAfter(int prev, list_t *L);
void delLast(list_t *L);
void printElement(list_t L);
void delAll(list_t *L);

int isInRange(int num, int lo, int hi);
int walker(int node_after, list_t L);
void filterInRange(list_t *L, int lo, int hi);
