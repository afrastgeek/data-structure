/*
 * Saya M Ammar Fadhlur Rahman tidak melakukan kecurangan yang dispesifikasikan
 * pada mata kuliah Struktur Data pada saat mengerjakan Kuis 1 Struktur Data.
 * Jika saya melakukan kecurangan maka Allah/Tuhan adalah saksi saya, dan saya
 * bersedia menerima hukumanNya. Aamiin.
 */
#include "ltsj16.h"

/**
 * @brief      Creates a list from passed list by initializing it's first
 *             sibling value (first) and each relation value (next).
 *
 * @param      L     List given to initialize.
 */
void createList(list_t *L) {
  (*L).first = NULL_VALUE;
  int node_nth;
  for (node_nth = 0; node_nth < MAX_VALUE; node_nth++) {
    (*L).node[node_nth].next = INIT_VALUE;
  }
}

/**
 * @brief      Counts the number of element from list passed.
 *
 * @param[in]  L     List given to count it's element.
 *
 * @return     Number of element.
 */
int countElement(list_t L) {
  int result = 0;
  if (L.first != NULL_VALUE) {
    int node_nth = L.first;
    while (node_nth != NULL_VALUE) {
      result += 1;
      node_nth = L.node[node_nth].next;
    }
  }
  return result;
}

/**
 * @brief      Find first empty element from a list.
 *
 * @param[in]  L     List given to search for.
 *
 * @return     Element index with empty value.
 */
int emptyElement(list_t L) {
  int result = NULL_VALUE;
  if (countElement(L) < MAX_VALUE) {
    int not_found = 1;
    int i = 0;
    while ((not_found) && (i < MAX_VALUE)) {
      if (L.node[i].next == INIT_VALUE) {
        result = i;
        not_found = 0;
      } else {
        i += 1;
      }
    }
  }
  return result;
}

/**
 * @brief      Adds node with content at the start of a list.
 *
 * @param[in]  content  The content.
 * @param      L        The list to add node to.
 */
void addFirst(random_num_t content, list_t *L) {
  if (countElement(*L) < MAX_VALUE) { /* static addFirst */
    int node_nth = emptyElement(*L);
    (*L).node[node_nth].content = content;
    if ((*L).first == NULL_VALUE) { /* jika list kosong */
      (*L).node[node_nth].next = NULL_VALUE;
    } else { /* jika list tidak kosong */
      (*L).node[node_nth].next = (*L).first;
    }
    (*L).first = node_nth;
  } else { /* proses jika array penuh */
    printf("sudah tidak dapat ditambah\n");
  }
}

/**
 * @brief      Adds node with content to a list after given node.
 *
 * @param[in]  node_before  The node before target added node.
 * @param[in]  content      The content.
 * @param      L            The list to add node to.
 */
void addAfter(int node_before, random_num_t content, list_t *L) {
  if (countElement(*L) < MAX_VALUE) { /* static addFirst */
    int node_nth = emptyElement(*L);
    (*L).node[node_nth].content = content;
    if ((*L).node[node_before].next == NULL_VALUE) { /* jika list kosong */
      (*L).node[node_nth].next = NULL_VALUE;
    } else { /* jika list tidak kosong */
      (*L).node[node_nth].next = (*L).node[node_before].next;
    }
    (*L).node[node_before].next = node_nth;
  } else { /* proses jika array penuh */
    printf("sudah tidak dapat ditambah\n");
  }
}

/**
 * @brief      Adds node content to the end of a list.
 *
 * @param[in]  content  The content
 * @param      L        The list to add node to.
 */
void addLast(random_num_t content, list_t *L) {
  if ((*L).first == NULL_VALUE) { /* proses jika list masih kosong */
    addFirst(content, L);
  } else { /* proses jika list telah berisi element */
    if (countElement(*L) < MAX_VALUE) { /* proses jika array belum penuh */
      int node_nth = (*L).first;        /* proses mencari element terakhir */
      while ((*L).node[node_nth].next != NULL_VALUE) {
        node_nth = (*L).node[node_nth].next;
      }
      addAfter(node_nth, content, L);
    } else { /* proses jika array penuh */
      printf("sudah tidak dapat ditambah\n");
    }
  }
}

/**
 * @brief      Delete node at the start of a list.
 *
 * @param      L     The list to delete node from.
 */
void delFirst(list_t *L) {
  if ((*L).first != NULL_VALUE) {
    int node_nth = (*L).first;
    if (countElement(*L) == 1) {
      (*L).first = NULL_VALUE;
    } else {
      (*L).first = (*L).node[node_nth].next;
    }
    (*L).node[node_nth].next = INIT_VALUE;
  } else { /* proses jika list kosong */
    printf("empty list\n");
  }
}

/**
 * @brief      Delete node after given node from a list.
 *
 * @param[in]  node_before  The node before target deleted element.
 * @param      L            The list to delete node from.
 */
void delAfter(int node_before, list_t *L) {
  int node_nth = (*L).node[node_before].next;
  if (node_nth != NULL_VALUE) {
    if ((*L).node[node_nth].next == NULL_VALUE) {
      (*L).node[node_before].next = NULL_VALUE;
    } else {
      (*L).node[node_before].next = (*L).node[node_nth].next;
    }
    (*L).node[node_nth].next = INIT_VALUE;
  }
}

/**
 * @brief      Delete node at the end of a list.
 *
 * @param      L     The list to delete node from.
 */
void delLast(list_t *L) {
  if ((*L).first != NULL_VALUE) {
    if (countElement(*L) == 1) { /* proses jika list hanya berisi satu elemen */
      delFirst(L);
    } else {
      int node_nth = (*L).first;
      int previous;
      while ((*L).node[node_nth].next != NULL_VALUE) {
        previous = node_nth;
        node_nth = (*L).node[node_nth].next;
      }
      delAfter(previous, L);
    }
  } else { /* proses jika list kosong */
    printf("empty list\n");
  }
}

/**
 * @brief      Print all element in the list.
 *
 * @param[in]  L    f The list to print node from.
 */
void printElement(list_t L) {
  if (L.first != NULL_VALUE) {
    int node_nth = L.first;
    while (node_nth != NULL_VALUE) {
      printf("%d", L.node[node_nth].content.numeric);
      node_nth = L.node[node_nth].next;
      if (node_nth != NULL_VALUE) printf(" ");
    }
    printf("\n");
  } else { /* proses jika list kosong */
    printf("empty list\n");
  }
}

/**
 * @brief      Delete all element from a list.
 *
 * @param      L     The list to delete node from.
 */
void delAll(list_t *L) {
  int i;
  for (i = countElement(*L); i >= 1; i--) {
    delLast(L);
  }
}

/**
 * @brief      Determines if the number given is in range.
 *
 * @param[in]  num   The number.
 * @param[in]  lo    The lower range limit.
 * @param[in]  hi    The higher range limit.
 *
 * @return     True if in range, False otherwise.
 */
int isInRange(int num, int lo, int hi) {
  if (num >= lo && num <= hi) return 1; /* If given number is in range, TRUE */
  return 0; /* Otherwise, FALSE */
}

/**
 * @brief      Walker function to get element index before specified element.
 *
 * @param[in]  node_after  The specified element.
 * @param[in]  L           The list to search index from.
 *
 * @return     The desired index, NULL_VALUE otherwise.
 */
int walker(int node_after, list_t L) {
  int node_nth = NULL_VALUE;
  if (L.first != NULL_VALUE) { /* Only proceed when the list isn't empty */
    node_nth = L.first;
    while (L.node[node_nth].next != node_after) { /* Iterate till match */
      node_nth = L.node[node_nth].next;
    }
  }
  return node_nth;
}

/**
 * @brief      Filter list element with content in specified range.
 *
 * @param      L     The list to filter from.
 * @param[in]  lo    The lower range limit.
 * @param[in]  hi    The higher range limit.
 */
void filterInRange(list_t *L, int lo, int hi) {
  if ((*L).first != NULL_VALUE) { /* Only proceed when the list isn't empty */
    int node_nth = (*L).first;
    int node_before = (*L).first;
    while (node_nth != NULL_VALUE) { /* Iterate while node is present */
      if (isInRange((*L).node[node_nth].content.numeric, lo, hi)) {
        if (node_nth == (*L).first) { /* If matched node is first element */
          delFirst(L); /* Delete first element */
          node_nth = (*L).first; /* Set iterator to the start */
        } else { /* Otherwise */
          node_before = walker(node_nth, *L);
          delAfter(node_before, L); /* Delete this element */
          node_nth = node_before; /* Set iterator to it's element before */
        }
      } else { /* If element not in specified range */
        node_nth = (*L).node[node_nth].next; /* Go to the next element */
      }
    }
  }
}
